import sys
import os
import json
import requests
import time
import func

########################### GLOBAL PARAMETERS ##########################

# server = "http://review_release-5-11-x.opensee.ninja:8080"
# server = "http://review_master.opensee.ninja:8080"
server = "http://review_1355-diff-api-controller.opensee.ninja:8080"
# server="http://review_feature-ingestion-by-block.opensee.ninja:8080"
user = "admin"
password = "Welcome?"

# parameters
module = "module_alexandre"
table= "Facttable"
dict = "Dictionary"
ziptable="Ziptable"

# module = "testing_Arrays"
# table= "testing_Arrays_trade_FactTable"
# dict ="testing_Arrays_csa_Dictionary"
# ziptable="testing_Arrays_riskfactors_ZipTable"

########################### STEPS ##########################
steps={
"create_dm_with_autocube":{'type':'autocube','file_name':'autocube_input.json'},

# initial 1
"open_initial_1":{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'initial','comment':'initial_1'},
"ingest_initial_1":{'type':'ingest','transaction':'open_initial_1','file_name':'initial_1.csv'},
"close_initial_1":{'type':'closeTransaction','transaction':'open_initial_1','destinationBranch':'initial','body':{"closingMode": {"Merge": {"force":False}}}},

# adj_1
'open_adj_1':{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'initial','comment':'adj_1'},
"ingest_adj_1":{'type':'ingest','transaction':'open_adj_1','file_name':'adj_1.csv','blocks':'StaticKey1/StaticKey2'},
"close_adj_1":{'type':'closeTransaction','transaction':'open_adj_1','destinationBranch':'adj_1','body':{"closingMode": {"Merge": {"force":True}}}},

# adj_2
'open_adj_2':{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'initial','comment':'adj_2'},
"ingest_adj_2":{'type':'ingest','transaction':'open_adj_2','file_name':'adj_2.csv','blocks':'StaticKey1/StaticKey2'},
"close_adj_2":{'type':'closeTransaction','transaction':'open_adj_2','destinationBranch':'adj_2','body':{"closingMode": {"Merge": {"force":True}}}},

# checkpoint 1
"query_adj_1":{'type':'queryCheckpoint','file_name':'query_adj_1.json'},
"query_adj_2":{'type':'queryCheckpoint','file_name':'query_adj_2.json'},

"diff_1":{'type':'queryDiffCheckpoint','file_name':'diff_adj_1_vs_adj_2.json'},
"diff_2":{'type':'queryDiffCheckpoint','file_name':'diff_adj_1_vs_adj_2_bis.json'}

}


########################### PATHS ##########################
# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
log_full_path=f"{script_folder}/logs.log"

# clear log file
with open(log_full_path, "w") as log_file:
  pass

checkpoints_trigger=True
result={}

########################### LOGIN TO SERVER ##########################

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...',end="\n")
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
# kwcnx = dict(headers={'Authorization': token})

headers={'Authorization': token}


########################### STEPS ##########################
dm_trigger=True

for step in steps:
    type=steps[step]['type']

    # autocube
    if type=="autocube":

        headers['Content-Type']='json'
        file_name=steps[step]['file_name']

        url=f"{server}/autocube/{module}"

        file_full_path=f"{script_folder}/source/{file_name}"
        autocube_input_dict=func.load_json(file_full_path)

        try:
            #Deletes module if it aleady exists
            autocube_input_dict['mode']='Delete'

            resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
            time.sleep(1)
        finally:
            #creates module
            autocube_input_dict['mode']='Create'

            resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
            time.sleep(1)

            if resp.status_code!=200:
                dm_trigger=False

            # Set insertMethod to Linear
            url=f"{server}/module/{module}/tables/{table}/ingestion_settings"
            resp=requests.request(method='get',url=url, headers=headers)
            body=resp.json()
            body["insertMethod"]="Linear"

            resp=requests.request(method='post',url=url,json=body, headers=headers)

        if resp.status_code not in [200,201,204]:
            result[step]={"type":type,"status":"KO","url":url,"body":body,"response":resp.json()}
            print("datamodel error : could not create datamodel; please check logs or debug to investigate")
            func.print_logs(result,log_full_path)
            sys.exit()

    # rdm
    elif type=="rdm":
        headers['Content-Type']='json'
        file_name=steps[step]['file_name']

        url=f"{server}/datamodel"

        file_full_path=f"{script_folder}/source/{file_name}"
        body=func.load_json(file_full_path)

        resp=requests.request(method='post',url=url,json=body, headers=headers)
        time.sleep(1)

        if resp.status_code not in [200,201,204]:
            result[step]={"type":type,"status":"KO","url":url,"body":body,"response":resp.json()}
            print("datamodel error : could not create datamodel; please check logs or debug to investigate")
            func.print_logs(result,log_full_path)
            sys.exit()

    # open transaction
    elif type=='openTransaction':

        as_of=steps[step]['as_of']
        sourceBranch=steps[step]['sourceBranch']
        comment=steps[step]['comment']

        #open transaction
        url=f"{server}/tables/{table}/transaction/{as_of}/from/{sourceBranch}/{comment}"

        resp=requests.request(method='post',url=url, headers=headers)

        steps[step]['transaction_id']=resp.text

        time.sleep(1)
    
    #ingest fact table
    elif type=='ingest':
        headers['Content-Type']='text/csv'
        transaction_id=steps[steps[step]['transaction']]['transaction_id']
        as_of=steps[steps[step]['transaction']]['as_of']
        file_name=steps[step]['file_name']

        if "blockId" in steps[step]:
            blockId=steps[step]['blockId']
            url = f"{server}/tables/{table}/commits/{transaction_id}/{as_of}/{blockId}"
        elif "blocks" in steps[step]:
            blocks=steps[step]['blocks']
            url = f"{server}/beta/transactions/{transaction_id}/blocks/{blocks}"
        else:
            url=f"{server}/tables/{table}/commits/{transaction_id}/{as_of}"

        file_full_path=f"{script_folder}/source/data/{file_name}"

        with open(file_full_path, 'rb') as fp:
            resp=requests.request(method='post',url=url,data=fp, headers=headers)

        time.sleep(1)

    #soft delete
    elif type=='softDelete':
        transaction_id=steps[steps[step]['transaction']]['transaction_id']
        as_of=steps[steps[step]['transaction']]['as_of']
        json_full_path = f"{script_folder}/source/data/{'softdelete_body.json'}"
        url = f"{server}/tables/{table}/commits/{transaction_id}/{as_of}/delete"
        body = func.load_json(json_full_path)

        resp = requests.request(method='delete', url=url,json=body, headers=headers)

        time.sleep(1)

    #ingest dictionary table
    elif type=="dictIngest":
        headers['Content-Type']='text/csv'
        as_of=steps[step]['as_of']
        sourceBranch=steps[step]['sourceBranch']
        destinationBranch=steps[step]['destinationBranch']
        comment=steps[step]['comment']
        file_name=steps[step]['file_name']

        url=f"{server}/dictionaries/{dict}/{as_of}/from/{sourceBranch}/to/{destinationBranch}/{comment}"

        file_full_path=f"{script_folder}/source/data/{file_name}"

        with open(file_full_path, 'rb') as fp:
            resp=requests.request(method='post',url=url,data=fp, headers=headers)
        
        time.sleep(1)

    #ingest zip table
    elif type=="zipIngest":
        headers['Content-Type']='text/csv'
        as_of=steps[step]['as_of']
        file_name=steps[step]['file_name']

        url=f"{server}/tables/{ziptable}/{as_of}"
        file_full_path=f"{script_folder}/source/data/{file_name}"

        with open(file_full_path, 'rb') as fp:
            resp=requests.request(method='post',url=url,data=fp, headers=headers)
        
        time.sleep(1)

    #close transaction
    elif type=='closeTransaction':

        #close transaction
        transaction_id=steps[steps[step]['transaction']]['transaction_id']
        as_of=steps[steps[step]['transaction']]['as_of']
        destinationBranch=steps[step]['destinationBranch']
        body=steps[step]['body']

        destinationBranch=steps[step]['destinationBranch']
        url=f"{server}/tables/{table}/closeTransaction/{transaction_id}/{as_of}/to/{destinationBranch}"
        
        resp=requests.request(method='post',url=url,json=body, headers=headers)

        time.sleep(1)
    
    #merge
    elif type=='merge':
        headers['Content-Type']='json'

        as_of=steps[step]['as_of']
        sourceBranch=steps[step]['sourceBranch']
        destinationBranch=steps[step]['destinationBranch']
        body=steps[step]['body']

        url= f'{server}/tables/{table}/merge/{as_of}/from/{sourceBranch}/into/{destinationBranch}'

        resp=requests.request(method='post',url=url,json=body, headers=headers)

        time.sleep(1)

    #queryCheckpoint
    elif type=="queryCheckpoint":
        file_name=steps[step]['file_name']
        input_file_full_path=f"{script_folder}/source/checkpoints/{file_name}"
        data=func.load_json(input_file_full_path)["body"]

        curent_output=func.query_api(server,token,module,data)
        expected_output=func.load_json(input_file_full_path)["expected_response"]

    #calculatorRequestCheckpoint
    elif type=="calculatorRequestCheckpoint":
        file_name=steps[step]['file_name']
        input_file_full_path=f"{script_folder}/source/checkpoints/{file_name}"
        data=func.load_json(input_file_full_path)["body"]

        curent_output=func.executecalculator_api(server,token,module,data)
        expected_output=func.load_json(input_file_full_path)["expected_response"]

    #queryDiffCheckpoint
    elif type=="queryDiffCheckpoint":
        file_name=steps[step]['file_name']
        input_file_full_path=f"{script_folder}/source/checkpoints/{file_name}"
        data=func.load_json(input_file_full_path)["body"]

        curent_output=func.query_api(server,token,module,data,"diff")
        expected_output=func.load_json(input_file_full_path)["expected_response"]

    #Unknown steps
    else:
        result[step]={"type":type,"status":"KO","comment":"type unknown"}
        print(f"{step} ({type}): KO. type is unknown",end="\n")

    #checkpoints checks
    if "Checkpoint" not in type:
        if resp.status_code in [200,201,204]:
            print(f"{step} ({type}): OK",end="\n")
        else:
            result[step]={"type":type,"status":"KO","url":url,"body":body,"response":resp.json()}
            print(f"{step} ({type}): KO",end="\n")

############# CHECKS ##############
    if "Checkpoint" in type :
    # Compare response with expected output
        try:
            trigger=func.query_validator(curent_output,expected_output)
            if trigger:
                print(f"{step} ({type}): OK",end="\n")
            else:
                checkpoints_trigger=False
                result[step]={"type":type,"status":"KO","module":module,"query_body":data,"query_response":curent_output,"expected_response":expected_output}
                print(f"{step} ({type}): KO",end="\n")
        except:
            trigger=False
            checkpoints_trigger=False
            print(f"{step} ({type}):KO. Please debug for more details",end="\n")

# print results
func.print_logs(result,log_full_path)

if checkpoints_trigger:
    print(f'all tests succeeded. log file available here: {log_full_path}')
else:
    print(f'Some tests failed. log file available here:{log_full_path}')