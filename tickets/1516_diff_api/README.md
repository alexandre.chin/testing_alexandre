## Purpose

This template enables to create Python scripts for automatizing APIs calls, which can be used for the following purposes :
- bug reproducing
- bug fix testing
- non-regression testing
- new feature testing
- acceptance criterias specifications

### Checkpoints

This template enables to perform 2 differents sort of tests:

1. API technical test, which are validated based on the response code. These tests are automatically performed on every API call performed through this script, and do not need to be specified by the user.

To be added soon: response pattern validation

2. API Functional tests, refered as checkpoints in this document, which are validated by comparing an API call response against its expected outresponseput. For this type of tests, the user must define the API to be tested, its body, and the expected response.

Please refer to the operational process below for more details.

## Operational Process

1. Clone/pull this GIT repository

2. Copy this whole folder, paste it in the folder `tickets` and rename it with the correspoinding ticket number

3. Define the global parameters (server, module etc.) in the file main.py

4. Define the steps to be performed in the file main.py and create the required input files in the folder data

Please refer to the Steps syntax chapter for more details on how to fill the main.py file

5. Create checkpoints
- define checkpoints to be performed in the steps in main.py
- for each checkpoint, define in the folder `checkpoints` the body to be sent and the expected result for validating the checkpoint

## Steps syntax

This chapter provide the list of template of supported steps, and their associated syntax to be used in the main.py file.

**Important:** users only have to modify the "route_parameters" and "body" fields 

### Ping and Login
#### ping
```json
unique_step_name:{"type":"ping","method":"get","route_parameters":{"server":server},"body":None}
```

#### login
```json
unique_step_name:{"type":"login","method":"post","route_parameters":{"server":server},"body":{"identifier": user,"password": password}}
```

### Usergroups and Policies
#### get a userGroup Policies
```json
unique_step_name:{"type":"getPolicies","method":"get","route_parameters":{"server":server,"userGroup":"Admin"},"body":None}
```
#### add a policy to a userGroup
```json
unique_step_name:{"type":"addPolicy","method":"put","route_parameters":{"server":server,"userGroup":"Admin"},"body":{"id": "9de78f13-bc16-4bbb-abcd-8b5db16c4ffc", "actions": ["canCreateValidationRequest"], "resource": "validationrequest:WF_template"}}
```

### Datamodel
#### Delete DM with autocube
```json
unique_step_name:{"type":"autocube","method":"post","route_parameters":{"server":server,"module":module},"body":f"{script_folder}/source/dm/autocube_delete.json"}
```
#### Create DM with autocube
```json
unique_step_name:{"type":"autocube","method":"post","route_parameters":{"server":server,"module":module},"body":f"{script_folder}/source/dm/autocube_input.json"}
```
### Ingestion settings

#### Get DM ingestion settings
```json
unique_step_name:{"type":"ingestionSettings","method":"get","route_parameters":{"server":server,"module":module,"table":table},"body":{}}
```

#### Set DM insertMethod to Linear
```json
unique_step_name:{"type":"ingestionSettings","method":"post","route_parameters":{"server":server,"module":module,"table":table},"body":{},"overload":[{"step":"test_get_ingestionSettings","body":{"insertMethod":"Linear"}}]},
```

### Ingestions in a versioned facttable
#### Opening a transaction
```json
unique_step_name:{"type":"openTransaction","method":"post","route_parameters":{"server":server,"table":table,"asof":"2021-05-07","sourceBranch":"initial","comment":"initial"},"body":None}
```

#### Ingesting
```json
unique_step_name:{"type":"ingest","transaction":"openTransaction_step_name","method":"post","route_parameters":{"server":server,"table":table,"asof":"2021-05-07"},"body":f"{script_folder}/source/data/initial_1.csv"}
```

#### Ingesting by blockId
```json
unique_step_name:{"type":"ingest","transaction":"openTransaction_step_name","method":"post","route_parameters":{"server":server,"table":table,"asof":"2021-05-07","blockId":"StaticKey1/trade_1"},"body":f"{script_folder}/source/data/initial_1.csv"}
```

#### Ingesting by blocks
```json
unique_step_name:{"type":"ingest","transaction":"openTransaction_step_name","method":"post","route_parameters":{"server":server,"table":table,"asof":"2021-05-07","blocks":"StaticKey1/StaticKey2"},"body":f"{script_folder}/source/data/initial_1.csv"}
```

#### soft Delete
```json
unique_step_name:{"type":"softDelete","transaction":"openTransaction_step_name","method":"delete","route_parameters":{"server":server,"table":table,"asof":"2021-05-07"},"body":f"{script_folder}/source/json_bodies/softdelete.json"}
```

#### Closing a transaction
```json
unique_step_name:{"type":"closeTransaction","method":"post","transaction":"openTransaction_step_name","route_parameters":{"server":server,"table":table,"destinationBranch":"initial"},"body":{"closingMode": {"Merge": {"force":True}}}}
```

### Merging and rebasing branches

#### Merging
```json
unique_step_name:{"type":"merge","method":"post","route_parameters":{"server":server,"table":table,"asof":"2021-05-07","sourceBranch":"initial","destinationBranch":"official"},"body":{"comment": "merging initial into official", "conflictManagement": "KeepSourceBranch", "concurrencyManagement":"None","moveBranch": "Destination"}}
```

#### Rebasing
```json
unique_step_name:{"type":"rebase","method":"post","route_parameters":{"server":server,"table":table,"asof":"2021-05-07","sourceBranch":"initial","destinationBranch":"official"},"body":{"comment": "merging initial into official", "conflictManagement": "KeepSourceBranch", "concurrencyManagement":"None","moveBranch": "Destination"}}
```

### Checkpoints

A checkpoint enables to compare, at any point within the script, an API response against its expected response.

#### Performing a query
```json
unique_step_name:{"type":"queryCheckpoint","method":"post","route_parameters":{"server":server,"module":module},"body":f"{script_folder}/source/checkpoints/query_official_2.json"}
```
#### Performing a details query
```json
unique_step_name:{"type":"detailsCheckpoint","method":"post","route_parameters":{"server":server,"module":module},"body":f"{script_folder}/source/checkpoints/query_official_2.json"}
```
#### Performing a diff query
```json
unique_step_name:{"type":"diffCheckpoint","method":"post","route_parameters":{"server":server,"module":module},"body":f"{script_folder}/source/checkpoints/query_official_2.json"}
```

#### performing a calculator request

Coming soon


## Logs
The file full_logs.log contains:
- all details regarding the environment on which the script has been run
- all details on all tests performed

The file error_logs.log contains:
- all details regarding the environment on which the script has been run
- all details for each test which has failed


