import sys
import os
import re
import json
import requests
import time
import random

def load_json(json_full_path):
    '''
    reads a json file as a dictionary
    '''
    with open(json_full_path, "r") as f:
        data = json.load(f)
    return data

def print_logs(obj,log_full_path):

    '''
    obj=object to print (dictionnary,list etc.)
    log_full_path= full path to log file
    '''
    with open(log_full_path, "a") as log_file:
        log_file.write(json.dumps(obj,sort_keys=False, indent=4))

def ping_api(server):

    result={}
    result['ping']={}

    # testing connection & server version
    url=f'{server}/ping'
    resp=requests.request(method='get',url= url)

    print(f"Rafal version = {resp.json()['version']}")
    print(f"Rafal DB version = {resp.json()['dbVersion']}")

    response=resp.json()

    result['ping'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    return result

def login_api(server,user,password):

    result={}
    result['login']={}

    # testing connection & server version
    url=f'{server}/login'
    resp=requests.request(method='post',url= url,json={"identifier": user,"password": password},)

    # response
    response=resp.json()

    result['login'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':{"identifier": user,"password": password}, 'response':response}

    return result

