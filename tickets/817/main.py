import sys
import os
import json
import requests
import time
from source import func

result={}

########################### GLOBAL PARAMETERS ##########################

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
log_full_path=f"{script_folder}/logs.log"

# global parameters
server = "http://review_feature-rebase-merge.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# parameters
module = "IngestionWrapperTest"
table= "IngestionWrapperTest"
dict ="dictionaryIdTest"


# queries parameters
ingestion_parameters=[
#test 1
{
'request_parameters_fact':{'file_name':'ingestion_fact_test_1.csv','as_of':'2021-05-07','sourceBranch':'official','destinationBranch':'official','comment':'ingestion_fact_test_1'}
},

{
'request_parameters_fact':{'file_name':'ingestion_fact_test_2.csv','as_of':'2021-05-07','sourceBranch':'official','destinationBranch':'official','comment':'ingestion_fact_test_2'}
},

{
'request_parameters_fact':{'file_name':'ingestion_fact_test_3.csv','as_of':'2021-05-07','sourceBranch':'official','destinationBranch':'private','comment':'ingestion_fact_test_3'}
},

{
'request_parameters_fact':{'file_name':'ingestion_fact_test_4.csv','as_of':'2021-05-07','sourceBranch':'private','destinationBranch':'private','comment':'ingestion_fact_test_4'}
},

{
'request_parameters_fact':{'file_name':'ingestion_fact_test_5.csv','as_of':'2021-05-07','sourceBranch':'official','destinationBranch':'official','comment':'ingestion_fact_test_5'}
}
]

# clear log file
with open(log_full_path, "w") as log_file:
  pass

########################### LOGIN TO SERVER ##########################

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
# kwcnx = dict(headers={'Authorization': token})

headers={'Authorization': token}

########################### CREATING MODULE WITH AUTOCUBE ##########################

headers['Content-Type']='json'

url=f"{server}/autocube/{module}"

json_full_path=f"{script_folder}/source/autocube_input.json"
autocube_input_dict=func.load_json(json_full_path)

try:
  #Deletes module if it aleady exists
  autocube_input_dict['mode']='Delete'

  resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
  time.sleep(2)
finally:
  #creates module
  autocube_input_dict['mode']='Create'

  resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
  time.sleep(2)

  print(f"module {module} created")

########################### DATA INGESTION ##########################
headers['Content-Type']='text/csv'

for request_parameters in ingestion_parameters:

    # ingest fact table
    if 'request_parameters_fact' in request_parameters.keys():

        as_of=request_parameters['request_parameters_fact']['as_of']
        sourceBranch=request_parameters['request_parameters_fact']['sourceBranch']
        destinationBranch=request_parameters['request_parameters_fact']['destinationBranch']
        comment=request_parameters['request_parameters_fact']['comment']
        file_name=request_parameters['request_parameters_fact']['file_name']

        #open transaction
        url=f"{server}/tables/{table}/transaction/{as_of}/from/{sourceBranch}/{comment}"

        resp=requests.request(method='post',url=url, headers=headers)

        transaction_id=resp.text
        print(f"Opening transaction: {transaction_id}")
        time.sleep(2)

        #ingest fact table
        url=f"{server}/tables/{table}/commits/{transaction_id}/{as_of}"

        file_full_path=f"{script_folder}/source/data/{file_name}"

        with open(file_full_path, 'rb') as fp:
            resp=requests.request(method='post',url=url,data=fp, headers=headers)
            time.sleep(2)

        print(f"Ingesting facts: {resp}")
        if resp.status_code not in (200,204):
            print(f"error: {resp.text}")
        else:
            pass

        #close transaction
        url=f"{server}/tables/{table}/closeTransaction/{transaction_id}/{as_of}/to/{destinationBranch}"

        resp=requests.request(method='post',url=url, headers=headers)
        print(f"Closing transaction: {resp.text}\n")

        time.sleep(2)

    # ingest dict
    elif 'request_parameters_dict' in request_parameters.keys():
        as_of=request_parameters['request_parameters_dict']['as_of']
        sourceBranch=request_parameters['request_parameters_dict']['sourceBranch']
        destinationBranch=request_parameters['request_parameters_dict']['destinationBranch']
        comment=request_parameters['request_parameters_dict']['comment']
        file_name=request_parameters['request_parameters_dict']['file_name']

        url=f"{server}/dictionaries/{dict}/{as_of}/from/{sourceBranch}/to/{destinationBranch}/{comment}"

        file_full_path=f"{script_folder}/source/data/{file_name}"

        with open(file_full_path, 'rb') as fp:
            resp=requests.request(method='post',url=url,data=fp, headers=headers)
            time.sleep(2)

        print(f"Ingesting dict: {resp}")
        if resp.status_code not in (200,204):
            print(f"error: {resp.text}\n")
        else:
            print(f"\n")

########################### MERGE OR REBASE ##########################
headers['Content-Type']='json'
comment='merging private branch with official'
url= f'{server}/tables/{table}/rebase/{as_of}/from/private/to/official'
body={"comment": "testing", "conflictManagement": "Abort"}

resp=requests.request(method='post',url=url,json=body, headers=headers)

print(f"Merging: {resp}")
if resp.status_code not in (200,204):
    print(f"error: {resp.text}")
else:
    pass

### CHECK BRANCHES COMMIT HIERARCHY ###
# Offical branch
url= f'{server}/tables/{table}/branches/official/{as_of}'
resp=requests.request(method='get',url=url, headers=headers)

official_branch_hierarchy=resp.json()

# Private branch
url= f'{server}/tables/{table}/branches/private/{as_of}'
resp=requests.request(method='get',url=url, headers=headers)

private_branch_hierarchy=resp.json()

print("test completed")