import sys
import os
import json
import requests
import time
import asyncio

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
log_full_path=f"{script_folder}/logs.log"

sys.path.insert(1, f'{parent_folder}/toolbox/source')
import func

result={}

########################### GLOBAL PARAMETERS ##########################

# global parameters
server = "http://review_improvment-adapt-transaction-close.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# parameters
module = "module_alexandre"
table= "Facttable"
dict ="Dictionary"


# queries parameters
ingestion_parameters=[
#test 1
{
'request_parameters_fact':{'file_name':'ingestion_fact_test_1.csv','as_of':'2021-05-07','sourceBranch':'official','destinationBranch':'official','comment':'ingestion_fact_test_1',"closingMode":None}
},

{
'request_parameters_fact':{'file_name':'ingestion_fact_test_2.csv','as_of':'2021-05-07','sourceBranch':'official','destinationBranch':'private','comment':'ingestion_fact_test_2',"closingMode":None}
},

{
'request_parameters_fact':{'file_name':'ingestion_fact_test_3.csv','as_of':'2021-05-07','sourceBranch':'official','destinationBranch':'official','comment':'ingestion_fact_test_3',"closingMode":None}
},

{
'request_parameters_fact':{'file_name':'ingestion_fact_test_4.csv','as_of':'2021-05-07','sourceBranch':'private','destinationBranch':'private','comment':'ingestion_fact_test_4',"closingMode":None}
},

{
'request_parameters_fact':{'file_name':'ingestion_fact_test_5.csv','as_of':'2021-05-07','sourceBranch':'private','destinationBranch':'official','comment':'ingestion_fact_test_5',"closingMode":"Merge"}
}
]


# clear log file
with open(log_full_path, "w") as log_file:
  pass

########################### LOGIN TO SERVER ##########################

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
# kwcnx = dict(headers={'Authorization': token})

headers={'Authorization': token}

########################### CREATING MODULE WITH AUTOCUBE ##########################

headers['Content-Type']='json'

url=f"{server}/autocube/{module}"

json_full_path=f"{script_folder}/source/autocube_input.json"
autocube_input_dict=func.load_json(json_full_path)

try:
  #Deletes module if it aleady exists
  autocube_input_dict['mode']='Delete'

  resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
  time.sleep(2)
finally:
  #creates module
  autocube_input_dict['mode']='Create'

  resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
  time.sleep(2)

  print(f"module {module} created")

########################### DATA INGESTION ##########################
headers['Content-Type']='text/csv'

for request_parameters in ingestion_parameters:

    # ingest fact table
    if 'request_parameters_fact' in request_parameters.keys():

        as_of=request_parameters['request_parameters_fact']['as_of']
        sourceBranch=request_parameters['request_parameters_fact']['sourceBranch']
        destinationBranch=request_parameters['request_parameters_fact']['destinationBranch']
        comment=request_parameters['request_parameters_fact']['comment']
        file_name=request_parameters['request_parameters_fact']['file_name']
        closingMode=request_parameters['request_parameters_fact']['closingMode']

        #open transaction
        url=f"{server}/tables/{table}/transaction/{as_of}/from/{sourceBranch}/{comment}"

        resp=requests.request(method='post',url=url, headers=headers)

        transaction_id=resp.text
        print(f"Opening transaction: {transaction_id}")
        time.sleep(2)

        #ingest fact table
        url=f"{server}/tables/{table}/commits/{transaction_id}/{as_of}"

        file_full_path=f"{script_folder}/source/data/{file_name}"

        with open(file_full_path, 'rb') as fp:
            resp=requests.request(method='post',url=url,data=fp, headers=headers)
            time.sleep(2)

        print(f"Ingesting facts: {resp}")
        if resp.status_code not in (200,204):
            print(f"error: {resp.text}")
        else:
            pass

        #close transaction
        url=f"{server}/tables/{table}/closeTransaction/{transaction_id}/{as_of}/to/{destinationBranch}"
        if closingMode=="Merge":
            body={"closingMode":{"Merge":{"force":False}}}
            resp=requests.request(method='post',url=url, json=body, headers=headers)
        else:
            resp=requests.request(method='post',url=url, headers=headers)
        print(f"Closing transaction: {resp.text}\n")

        time.sleep(2)

print("test completed")