import os
import time
import func
import pandas as pd
import json

########################### GLOBAL PARAMETERS ##########################

# server = "http://streamid-perf-0.opensee.ninja:9999"
server = "http://review_master.opensee.ninja:8080"
# server = "http://review_1606-expunge-record.opensee.ninja:8080"
# server = "http://devtest.opensee.ninja:8080"
# server = "http://review_feature-dict-ingestion-changes.opensee.ninja:8080"
# server="http://review_1540-fix-ingestion-wrapper.opensee.ninja:8080"

user = "guillaume"
password = "Welcome?"

module="ES3"
table="ES3_FumStock"
dict1="ES3_PriceableErrorMultiContext"
dict2="ES3_AnalyticalStructure"

script_folder=os.path.dirname(os.path.realpath(__file__))

result={}
token=None

steps={
############# PING AND LOGIN #############
#ping
'ping':{'type':'ping','method':'get','route_parameters':{'server':server},'body':{}},
#login
'login_admin':{'type':'login','method':'post','route_parameters':{'server':server},'body':{"identifier": user,"password": password}},

############# DM #############
"init_module":{'type':'initModule','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/dm/init_obj_ES.json'},

"addPolicy":{'type':'addPolicy','method':'put','route_parameters':{'server':server,'userGroup':'Admin1606'},'body':{'actions': ['editPivot','runPivot'],'resource': 'pivot:ES3/*'}},
"addMandatoryFilter":{'type':'addMandatoryFilter','method':'post','route_parameters':{'server':server,'userGroup':'Admin1606'},'body':{"ES3": {"@and": []}}},
"addUser":{'type':'addUser','method':'put','route_parameters':{'server':server,'userGroup':'Admin1606','user':user},'body':None},

############# Expunge all #############
"expunge_ES3_FumStock_all":{'type':'expunge','method':'delete','route_parameters':{'server':server,'table':table},'body':f'{script_folder}/source/json_bodies/expunge_facttable.json'},
"expunge_ES3_PriceableErrorMultiContext_all":{'type':'expunge','method':'delete','route_parameters':{'server':server,'table':dict1},'body':f'{script_folder}/source/json_bodies/expunge_dict1_all.json'},
"expunge_ES3_AnalyticalStructure_all":{'type':'expunge','method':'delete','route_parameters':{'server':server,'table':dict2},'body':f'{script_folder}/source/json_bodies/expunge_dict2_all.json'},

# delete branch
"delete_branch":{'type':'deleteBranch','method':'delete','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','branch':'initial'},'body':None},

############# INGESTION #############
# ingest dict
# "table_ingest_ES3_PriceableErrorMultiContext":{'type':'zipIngest','method':'post','route_parameters':{'server':server,'table':dict1},'body':f'{script_folder}/source/data/PriceableErrorMultiContext.csv'},
"dict_ingest_ES3_PriceableErrorMultiContext_1":{'type':'dictIngest','method':'post','route_parameters':{'server':server,'dict':dict1,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/PriceableErrorMultiContext.csv'},
# "dict_ingest_ES3_PriceableErrorMultiContext_2":{'type':'dictIngest','method':'post','route_parameters':{'server':server,'dict':dict1,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/PriceableErrorMultiContext.csv'},

# "dict_ingest_ES3_AnalyticalStructure":{'type':'dictIngest','method':'post','route_parameters':{'server':server,'dict':dict2,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/AnalyticalStructure.csv'},

# ingest initial 1
"open_initial_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'initial_1'},'body':None},
"ingest_initial_1":{'type':'ingest','transaction':'open_initial_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/FumStock0.csv'},
"close_initial_1":{'type':'closeTransaction','method':'post','transaction':'open_initial_1','route_parameters':{'server':server,'table':table,'destinationBranch':'initial'},'body':{"closingMode": {"Merge": {"force":True}}}},

# ingest initial 2 by blockId
"open_initial_2":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'initial_2'},'body':None},
"ingest_initial_2_by_blockId":{'type':'ingest','transaction':'open_initial_2','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07',"blockId":"CODPTF/CODPTF4"},'body':f'{script_folder}/source/data/FumStock.csv'},
"close_initial_2":{'type':'closeTransaction','method':'post','transaction':'open_initial_2','route_parameters':{'server':server,'table':table,'destinationBranch':'initial'},'body':{"closingMode": {"Merge": {"force":True}}}},

# wrapper
"dict_ingest_ES3_AnalyticalStructure_wrapper_1":{'type':'wrapper','method':'post','route_parameters':{'server':server,'dict':dict2,'asof':'2021-05-07','sourceBranch':'initial','destinationBranch':'initial','comment':'dict wrapper'},'body':f'{script_folder}/source/data/AnalyticalStructure.csv'},
# "dict_ingest_ES3_AnalyticalStructure_wrapper_2":{'type':'wrapper','method':'post','route_parameters':{'server':server,'dict':dict2,'asof':'2021-05-07','sourceBranch':'initial','destinationBranch':'initial','comment':'dict wrapper'},'body':f'{script_folder}/source/data/AnalyticalStructure.csv'},

# # query initial checkpoint
"query_initial_before_expunge1":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge2":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge3":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge4":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge5":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge6":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge7":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge8":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge9":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge10":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge11":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge12":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge13":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge14":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},
# "query_initial_before_expunge15":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_before_expunge.json'},


# expunge dict
"expunge_ES3_PriceableErrorMultiContext":{'type':'expunge','method':'delete','route_parameters':{'server':server,'table':dict1},'body':f'{script_folder}/source/json_bodies/expunge_dict1.json'},
"expunge_ES3_AnalyticalStructure":{'type':'expunge','method':'delete','route_parameters':{'server':server,'table':dict2},'body':f'{script_folder}/source/json_bodies/expunge_dict2.json'},

# query initial checkpoint
"query_initial_after_expunge":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_after_expunge.json'},

# reinsert in dict
"dict_ingest_ES3_AnalyticalStructure_wrapper_update_1":{'type':'wrapper','method':'post','route_parameters':{'server':server,'dict':dict2,'asof':'2021-05-07','sourceBranch':'initial','destinationBranch':'initial','comment':'dict wrapper'},'body':f'{script_folder}/source/data/AnalyticalStructure_updated.csv'},
# "dict_ingest_ES3_AnalyticalStructure_wrapper_update_2":{'type':'wrapper','method':'post','route_parameters':{'server':server,'dict':dict2,'asof':'2021-05-07','sourceBranch':'initial','destinationBranch':'initial','comment':'dict wrapper'},'body':f'{script_folder}/source/data/AnalyticalStructure_updated.csv'},

# query initial checkpoint
"query_initial_after_expunge_and_update":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_after_expunge_and_update.json'},

}

start = time.time()
print(f"Tests on going...")

all_tests_ok=True
script_folder=os.path.dirname(os.path.realpath(__file__))

full_log_path=f"{script_folder}/full_logs.log"
error_log_path=f"{script_folder}/error_logs.log"

for step in steps:
    # time.sleep(2)
    if step in "expunge_ES3_FumStock_all":
        print("xxx")
        # time.sleep(30)
    if steps[step]['type'] in ['ingest','softDelete','closeTransaction']:
        #add asof and transaction_id to route parameters
        steps[step]['route_parameters']['transaction_id']=result[steps[step]['transaction']]['response']['response']
        steps[step]['route_parameters']['asof']=result[steps[step]['transaction']]['route_parameters']['asof']
        time.sleep(1)

    # overload
    result=func.overload(steps[step],result)

    # api call
    temp=func.api_call(token,**steps[step])

    result[step]=func.store_api_call(step,temp)

    if steps[step]['type']=='login':
        token=result[step]['response']['response']['token']

###### Print logs #######
# clear log file
with open(full_log_path, "w") as log_file:
    pass
with open(error_log_path, "w") as log_file:
    pass

func.print_logs(result,full_log_path)

result_errors={}

for step in result:
    if result[step]['response']['status_code'] in [200,201,203,204]:
        if result[step]['type'] in ['ping','login']:
            result_errors[step]=result[step]
        elif 'Checkpoint' in result[step]['type']:
            if result[step]['checkpoint_status']=='KO':
                print(step)
                print("current response", result[step]['response']['response'])
                print("expected response", result[step]['expected_response'])
                result_errors[step]=result[step]
                all_tests_ok=False
    else:
        result_errors[step]=result[step]
        all_tests_ok=False

print(f"Tests completed in {time.time()-start:.2f} sec. /n")

if all_tests_ok:
    print(f'all tests succeeded. full logs file available here: {full_log_path}')
else:
    func.print_logs(result_errors,error_log_path)
    print(f'Some tests failed. full logs and error logs file available here:{script_folder}')
