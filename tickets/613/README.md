# Testing on Ingestion API - wrapper for dictionary updates Linear

## Summary


## Issue description
[Ticket 613: Ingestion API - wrapper for dictionary updates Linear](https://gitlab.com/ica1/rafal/-/issues/613)

## Testing Notebook
[Testing Notebook](testing.ipynb)

## Testing Results

| Test   | Test description                              | Test description commet                                        | Test results | Test results comment                                                           |
|--------|-----------------------------------------------|----------------------------------------------------------|--------------|------------------------------------------------------------------------|
| Test 1 | Raw ingestion                                 | Fact table raw ingestion followed by dict table raw ingestion  | OK           |  |
| Test 2 | Inserting a new line in a dict                | 2 new lines ingested in a dictionary                     | OK           ||
| Test 3 | Updating a line attributes in a dict          | Attribute updated in a dictionary's existing line        | OK           ||
| Test 4 | Ingestion on another as of          | Fact table ingestion followed by dict table ingestion        | OK           ||



# Testing environment
For testing purposes, a dedicated module has been created as follow :

`Server`: http://review_613-ingestion-api-wrapper-for-dictionary-updates.opensee.ninja:8080

`Module`: IngestionWrapperTest

`Fact table`: IngestionWrapperTest

`Dictionary`: dictionaryIdTest

`Pivot`: IngestionWrapperTest

`Branch`: official

`FAT UI`: 5.2.101

`Date`: 2021-05-07

`Ingestion mode`: Linear

## Module definiton
We used the following json to create our data model via autocube endpoint :

```json
{
    "mode": "Create",
    "inputData": [
        {
            "tableName": "IngestionWrapperTest",
            "asOf": "2021-05-07",
            "data": {
                "dataCSV": "\"asOf\",\"Cpty\",\"portfolio\",\"trade\",\"fact\"\r\n\"2021-04-27\",\"Cpty0\",\"portfolio0\",626941,121742.24758764052\r\n"
            },
            "format": "CSV"
        },
        {
            "tableName": "dictionaryIdTest",
            "asOf": "2021-05-07",
            "data": {
                "dataCSV": "\"Cpty\",\"portfolio\",\"decoration1\",\"decoration2\"\r\n\"Cpty7\",\"portfolio7\",\"decoration13\",\"decoration21\"\r\n"
            },
            "format": "CSV"
        }
    ],
    "dataModelSpecs": [
        {
            "factTable": "IngestionWrapperTest"
        },
        {
            "table": "IngestionWrapperTest",
            "dateColumn": "asOf"
        },
        {
            "table": "IngestionWrapperTest",
            "unicityKey": [
                "asOf",
                "Cpty",
                "portfolio",
                "trade"
            ]
        },
        {
            "table": "IngestionWrapperTest",
            "fact": "fact",
            "dataType": "Float64"
        },
        {
            "key1": {
                "table": "IngestionWrapperTest",
                "key": [
                    "Cpty",
                    "portfolio"
                ]
            },
            "key2": {
                "table": "dictionaryIdTest",
                "key": [
                    "Cpty",
                    "portfolio"
                ]
            },
            "keep": true
        }
    ]
}
```

Please note that the ingestion mode had to be set manually to `Linear` in the MongoDB table "TableSetting".

# Testing
The jupyter notebook used for testing can be found here: [Testing notebook](testing.ipynb)

The API calls used for ingesting are the following:
- data ingestion in dictionary table: `Server`/dictionaries/`Dictionary`/`Date`/from/`Branch`/`Comment`
- data ingestion in fact table: `Server`/tables/`Fact table`/commits/`transaction_id`/`Date`/`BlockId`/`Comment`

## Test 1 : Raw ingestion
The purpose of this test is to ensure the new dictionary ingestion endpoint is correctly updating the join keys in the fact table when ingesting new lines in a dictionary.

 To do so, we first ingest data in an empty fact table, and then ingest data in an empty dictionnary.

### Inputs
 We used the following csv files :

Dict table (Ctpy, Portfolio, Dimension1, Dimension2):
```csv
Cpty_1,Portfolio_1,titi,toto
Cpty_1,Portfolio_2,tata,tete
```

Fact table (Date,Ctpy, Portfolio, TradeId, Fact):
```csv
2021-05-07, Cpty_1, Portfolio_1, 1, 10
2021-05-07, Cpty_1, Portfolio_2, 2, 20
2021-05-07, Cpty_2, Portfolio_1, 1, 100
```

### Expected outputs

**Dict table:**

| DictionaryIdTest     | Dimension1 | Dimension2 |
|----------------------|------------|------------|
| 8105404497971028638  | titi       | toto       |
| 10881427750000039909 | tata       | tete       |

**Fact table:**

|     dictionaryIdTest | Date       | Cpty   | portfolio   | trade | fact  | commit              | factor | nb_aggregated |
|---------------------:|------------|--------|-------------|-------|-------|---------------------|--------|---------------|
| 0                   | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0  | 7720387547474471136  | 1  | 1 |
| 0                   | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0  | 7720387547474471136  | 1  | 1 |
| 0                   | 2021-05-07 | Cpty_2 | Portfolio_1 | 1 | 100.0 | 7720387547474471136  | 1  | 1 |
| 0                   | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0  | -1280712294886622717 | -1 | 1 |
| 0                   | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0  | -1280712294886622717  | -1  | 1 |
| 8105404497971028638 | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0  | -1280712294886622717 | 1  | 1 |
| 10881427750000039909 | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0  | -1280712294886622717  | 1  | 1 |


### Test outputs

| Table     | Test status | Test comment |
|----------------------|------|----------|
| Dict table | OK |      |
| Fact table | OK |      |


## Test 2 : Inserting a new line in a dict
This 2nd test aims at ensuring that the join key is updated in the fact table when ingesting a new line in a dictionnary.

### Inputs
 We used the following csv files :

Dict table (Ctpy, Portfolio, Dimension1, Dimension2):
```csv
Cpty_2,Portfolio_1,lili,lala
Cpty_2,Portfolio_2,roro,ruru
```

### Expected outputs

**Dict table:**

| DictionaryIdTest     | Dimension1 | Dimension2 |
|----------------------|------------|------------|
| 8105404497971028638  | titi | toto     |
| 10881427750000039909 | tata | tete     |
| 5595640088560178569  | roro | ruru     |
| 5414007437216441249  | lili | lala     |

**Fact table:**

|     dictionaryIdTest | Date       | Cpty   | portfolio   | trade | fact  | commit              | factor | nb_aggregated |
|---------------------:|------------|--------|-------------|-------|-------|---------------------|--------|---------------|
| 0                   | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0  | 7720387547474471136  | 1  | 1 |
| 0                   | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0  | 7720387547474471136  | 1  | 1 |
| 0                   | 2021-05-07 | Cpty_2 | Portfolio_1 | 1 | 100.0 | 7720387547474471136  | 1  | 1 |
| 0                   | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0  | -1280712294886622717 | -1 | 1 |
| 0                   | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0  | -1280712294886622717  | -1  | 1 |
| 8105404497971028638 | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0  | -1280712294886622717 | 1  | 1 |
| 10881427750000039909 | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0  | -1280712294886622717  | 1  | 1 |
| 0                   | 2021-05-07 | Cpty_2 | Portfolio_1 | 1 | 100.0 | 12345678912345667890  | -1  | 1 |
| 5414007437216441249  | 2021-05-07 | Cpty_2 | Portfolio_1 | 1 | 100.0 | 12345678912345667890  | 1  | 1 |

### Test outputs

| Table     | Test status | Test comment |
|----------------------|------|----------|
| Dict table | OK |      |
| Fact table | OK |     |

## Test 3 : Updating a line attributes in a dict
This 3rd test aims at ensuring that the join key is updated in the fact table when updating the attributes of an existing line in a dictionnary.

### Inputs
 We used the following csv files :

Dict table (Ctpy, Portfolio, Dimension1, Dimension2):

```csv
Cpty_1,Portfolio_1,titi,toto_bis
```

### Expected outputs

**Dict table:**

| DictionaryIdTest     | Dimension1 | Dimension2 |
|----------------------|------|----------|
| 10881427750000039909 | tata | tete     |
| 5595640088560178569  | roro | ruru     |
| 5414007437216441249  | lili | lala     |
| 8807667858931452908  | titi | toto_bis |
| 8105404497971028638  | titi | toto     |

**Fact table:**

|     dictionaryIdTest | Date       | Cpty   | portfolio   | trade | fact  | commit              | factor | nb_aggregated |
|---------------------:|------------|--------|-------------|-------|-------|---------------------|--------|---------------|
| 0                    | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0   | -4586137105679250849 | 1  | 1 |
| 0                    | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0   | -4586137105679250849 | 1  | 1 |
| 0                    | 2021-05-07 | Cpty_2 | Portfolio_1 | 1 | 100.0  | -4586137105679250849 | 1  | 1 |
| 0                    | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0   | -3454820485972244119 | -1 | 1 |
| 0                    | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0   | -3454820485972244119 | -1 | 1 |
| 10881427750000039909 | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0   | -3454820485972244119 | 1  | 1 |
| 8105404497971028638  | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0   | -3454820485972244119 | 1  | 1 |
| 0                    | 2021-05-07 | Cpty_2 | Portfolio_1 | 1 | 100.0  | -8215071081405814864 | -1 | 1 |
| 5414007437216441249  | 2021-05-07 | Cpty_2 | Portfolio_1 | 1 | 100.0  | -8215071081405814864 | 1  | 1 |
| 8105404497971028638  | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0   | 1990734522085858252  | -1 | 1 |
| 8807667858931452908  | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0   | 1990734522085858252  | 1  | 1 |

### Test ouputs

| Table     | Test status | Test comment |
|----------------------|------|----------|
| Dict table | OK |     |
| Fact table | OK |     |

## Test 4 : Inserting data on another as of date

### Inputs
 We used the following csv files :

Dict table (Ctpy, Portfolio, Dimension1, Dimension2):

```csv
Cpty_1,Portfolio_1,titi_new,toto_new
```

Fact table (Date,Ctpy, Portfolio, TradeId, Fact):
```csv
2021-05-08, Cpty_1, Portfolio_1, 1, 1000
2021-05-08, Cpty_1, Portfolio_2, 2, 2000
```

### Expected outputs

**Dict table:**

| DictionaryIdTest     | Dimension1 | Dimension2 |
|----------------------|------|----------|
| 10881427750000039909 | tata | tete     |
| 5595640088560178569  | roro | ruru     |
| 5414007437216441249  | lili | lala     |
| 8807667858931452908  | titi | toto_bis |
| 8105404497971028638  | titi | toto     |
| 15222806988248366912  | titi_new | toto_new     |

**Fact table:**

|     dictionaryIdTest | Date       | Cpty   | portfolio   | trade | fact  | commit              | factor | nb_aggregated |
|---------------------:|------------|--------|-------------|-------|-------|---------------------|--------|---------------|
| 0                    | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0   | -4586137105679250849 | 1  | 1 |
| 0                    | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0   | -4586137105679250849 | 1  | 1 |
| 0                    | 2021-05-07 | Cpty_2 | Portfolio_1 | 1 | 100.0  | -4586137105679250849 | 1  | 1 |
| 0                    | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0   | -3454820485972244119 | -1 | 1 |
| 0                    | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0   | -3454820485972244119 | -1 | 1 |
| 10881427750000039909 | 2021-05-07 | Cpty_1 | Portfolio_2 | 2 | 20.0   | -3454820485972244119 | 1  | 1 |
| 8105404497971028638  | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0   | -3454820485972244119 | 1  | 1 |
| 0                    | 2021-05-07 | Cpty_2 | Portfolio_1 | 1 | 100.0  | -8215071081405814864 | -1 | 1 |
| 5414007437216441249  | 2021-05-07 | Cpty_2 | Portfolio_1 | 1 | 100.0  | -8215071081405814864 | 1  | 1 |
| 8105404497971028638  | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0   | 1990734522085858252  | -1 | 1 |
| 8807667858931452908  | 2021-05-07 | Cpty_1 | Portfolio_1 | 1 | 10.0   | 1990734522085858252  | 1  | 1 |
| 0                    | 2021-05-08 | Cpty_1 | Portfolio_1 | 1 | 1000.0 | 5696398273804166862  | 1  | 1 |
| 0                    | 2021-05-08 | Cpty_1 | Portfolio_2 | 2 | 2000.0 | 5696398273804166862  | 1  | 1 |
| 0                    | 2021-05-08 | Cpty_1 | Portfolio_1 | 1 | 1000.0 | -3155091376108698115 | -1 | 1 |
| 15222806988248366912 | 2021-05-08 | Cpty_1 | Portfolio_1 | 1 | 1000.0 | -3155091376108698115 | 1  | 1 |


### Test ouputs

| Table     | Test status | Test comment |
|----------------------|------|----------|
| Dict table | OK |     |
| Fact table | OK |    |
