# Testing on ingestion issue reported by HSBC

## Summary
- [ ] issue reproduced
- [ ] issue identified
- [ ] issue fixed

## Issue description
[Ticket 279: Ingestion issue for HSBC](https://gitlab.com/ica1/rafal/-/issues/729)

## Testing Notebook
[Testing Notebook](testing.ipynb)

## Testing Results

| Test          | BlockId         | issue reported | issue reproduced | issue identified | issue fixed |
|---------------|-----------------|----------------|------------------|------------------|-------------|
| 1st ingestion | Date/2021-04-01| No             | NA               |                  |             |
| 2nd ingestion | PortfolioId/1| Yes            | No               |                  |             |
| 3rd ingestion | PortfolioId/1| Yes            | No               |                  |             |


**&rightarrow; Unable to reproduce the reported issue**


# Testing environment
For reproducing the issue reported by HSBC, a dedicated module has been created as follow :

Environment: `Master`

Module: `Test_HSBC_Alexandre`

Fact table: `Test_HSBC_Alexandre`

Dictionnary: NA

Pivot: `Test_HSBC_Alexandre`

Branch: `Test_HSBC_Alexandre_V4`

FAT UI: `5.2.101`

Ingestion mode: `Linear`

## Module definiton
```json
{
    "_id" : ObjectId("60913c36679a7d5dc67dd56e"),
    "module" : "Test_HSBC_Alexandre",
    "rawDataModel" : {
        "zips" : [],
        "joins" : [],
        "tables" : [ 
            {
                "tableType" : "CanonicalFactTable",
                "database" : "default",
                "name" : "Test_HSBC_Alexandre",
                "unicityKey" : [ 
                    "Date", 
                    "PortfolioId", 
                    "TradeId"
                ],
                "prependDate" : false,
                "orderingKey" : null,
                "shardingKey" : null,
                "primaryKey" : null,
                "schema" : [ 
                    {
                        "name" : "Date",
                        "dataType" : "Date"
                    }, 
                    {
                        "name" : "PortfolioId",
                        "dataType" : "Int8"
                    }, 
                    {
                        "name" : "TradeId",
                        "dataType" : "Int8"
                    }, 
                    {
                        "name" : "Fact",
                        "dataType" : "Float64"
                    }
                ],
                "isVersioned" : true,
                "partitioningKey" : null
            }
        ],
        "arrayDims" : [],
        "facts" : [ 
            {
                "table" : "Test_HSBC_Alexandre",
                "fact" : "Fact",
                "dataType" : "Float64"
            }
        ]
    },
    "creates" : [ 
        "CREATE TABLE IF NOT EXISTS default.Test_HSBC_Alexandre ON CLUSTER '{cluster}' (Date Date, PortfolioId Int8, TradeId Int8, Fact Float64, commit Int64, factor Int8, nb_aggregated UInt64) ENGINE = Distributed('{cluster}', default, shard_Test_HSBC_Alexandre, rand())", 
        "CREATE TABLE IF NOT EXISTS default.shard_Test_HSBC_Alexandre ON CLUSTER '{cluster}' (Date Date, PortfolioId Int8, TradeId Int8, Fact Float64, commit Int64, factor Int8, nb_aggregated UInt64) ENGINE = ReplicatedMergeTree('{zoo_prefix}/tables/{shard}/shard_Test_HSBC_Alexandre', '{host}') PARTITION BY tuple()  ORDER BY tuple()"
    ],
    "drops" : [ 
        "DROP TABLE IF EXISTS default.Test_HSBC_Alexandre ON CLUSTER '{cluster}'", 
        "DROP TABLE IF EXISTS default.shard_Test_HSBC_Alexandre ON CLUSTER '{cluster}'"
    ]
}
```

Please note that the unicity key is defined by `Date`, `PortfolioId` and `TradeId`

## TableSetting definition

```json
{
    "_id" : ObjectId("60913c36679a7d5dc67dd561"),
    "protobufSchema" : null,
    "facts" : [ 
        "Fact"
    ],
    "insertQuery" : "INSERT INTO default.Test_HSBC_Alexandre SELECT ~~as_of~~ Date, PortfolioId, TradeId, Fact, ~~commit~~, 1, 1 FROM input('Date Date, PortfolioId Int8, TradeId Int8, Fact Float64')",
    "dictionaries" : null,
    "facettes" : [],
    "insertMethod" : "Linear",
    "table" : "Test_HSBC_Alexandre"
}
```


## Pivot definition
```json
{
    "_id" : ObjectId("60913c36679a7d5dc67dd563"),
    "module" : "Test_HSBC_Alexandre",
    "name" : "Test_HSBC_Alexandre",
    "facts" : [ 
        "Fact"
    ],
    "postaggregations" : {},
    "defaultMetric" : null,
    "postaggregkey" : [],
    "dimensions" : [ 
        "Date", 
        "PortfolioId", 
        "TradeId"
    ],
    "metrics" : {
        "sum_Fact" : {
            "function" : "sum(Fact)",
            "facets" : [],
            "currency" : null
        },
        "count_Fact" : {
            "function" : "count(Fact)",
            "facets" : [],
            "currency" : null
        }
    },
    "where" : {
        "@and" : []
    },
    "rows" : [],
    "cols" : [],
    "table" : "Test_HSBC_Alexandre"
}
```

# 1st ingestion
The purpose of this 1st ingestion is to load all the deals as of `2021-04-01`, using the following csv file (Date, PortfolioId, TradeId, Fact):

```csv
2021-04-01,1,1,10
2021-04-01,1,2,20
2021-04-01,2,1,50
2021-04-01,2,2,80

```

## API call

### Configuration
```json
{
  "rafaleHost": "review_master.opensee.ninja",
  "rafalePort": "8080",
  "user": "alexandre",
  "password": "********",
  "token": "********",
  "module": "Test_HSBC_Alexandre",
  "table": "Test_HSBC_Alexandre",
  "as_of": "2021-04-01",
  "branch": "Test_HSBC_Alexandre_V4",
  "commit_hash": "**************",
  "block_id": "Date/2021-04-01"
}
```

### Endpoint
```
{{ _.rafaleHost }}:{{ _.rafalePort }}/tables/{{ _.table }}/commits/{{ _.commit_hash }}/{{ _.as_of }}
```
Please note that the `blockid` used in the API call is equal to `Date/2021-04-01`


## Output
### Fact table
| Date       | PortfolioId | TradeId | Fact | commit               | factor | nb_aggregated |
|------------|-------------|---------|------|----------------------|--------|---------------|
| 2021-04-01 | 1           | 1       | 10.0 | -7060435086791665957 | 1      | 1             |
| 2021-04-01 | 1           | 2       | 20.0 | -7060435086791665957 | 1      | 1             |
| 2021-04-01 | 2           | 1       | 50.0 | -7060435086791665957 | 1      | 1             |
| 2021-04-01 | 2           | 2       | 80.0 | -7060435086791665957 | 1      | 1             |


### FAT UI
![1st ingestion](src/img/FAT_UI_5-2-101-1st_ingestion.png)


## Testing report
| Test            | issue reported | issue reproduced | issue identified | issue fixed | Status |
|-----------------|----------------|------------------|------------------|-------------|-------------|
| 1st ingestion | No              | NA             | NA     | NA     | OK   

# 2nd ingestion
In this 2nd ingestion, we want to update all the deals as of `2021-04-01` for which the `PortfolioId=1`, using the following csv file (Date, PortfolioId, TradeId, Fact):


```csv
2021-04-01,1,2,100
2021-04-01,1,3,150
```

## API call

### Configuration
```json
{
  "rafaleHost": "review_master.opensee.ninja",
  "rafalePort": "8080",
  "user": "alexandre",
  "password": "********",
  "token": "********",
  "module": "Test_HSBC_Alexandre",
  "table": "Test_HSBC_Alexandre",
  "as_of": "2021-04-01",
  "branch": "Test_HSBC_Alexandre_V4",
  "commit_hash": "**************",,
  "block_id": "PortfolioId/1"
}
```

### Endpoint
```
{{ _.rafaleHost }}:{{ _.rafalePort }}/tables/{{ _.table }}/commits/{{ _.commit_hash }}/{{ _.as_of }}/{{ _.block_id }}
```
Please note that the `blockid` used in the API call is equal to `PortfolioId/1`

## Output
### Fact table
| Date       | PortfolioId | TradeId | Fact  | commit               | factor | nb_aggregated |
|------------|-------------|---------|-------|----------------------|--------|---------------|
| 2021-04-01 | 1           | 1       | 10.0  | -7060435086791665957 | 1      | 1             |
| 2021-04-01 | 1           | 2       | 20.0  | -7060435086791665957 | 1      | 1             |
| 2021-04-01 | 2           | 1       | 50.0  | -7060435086791665957 | 1      | 1             |
| 2021-04-01 | 2           | 2       | 80.0  | -7060435086791665957 | 1      | 1             |
| 2021-04-01 | 1           | 1       | 10.0  | -6283472006519261996 | -1     | 1             |
| 2021-04-01 | 1           | 2       | 20.0  | -6283472006519261996 | -1     | 1             |
| 2021-04-01 | 1           | 2       | 100.0 | -6283472006519261996 | 1      | 1             |
| 2021-04-01 | 1           | 3       | 150.0 | -6283472006519261996 | 1      | 1             |
   

### FAT UI
![2nd ingestion](src/img/FAT_UI_5-2-101-2nd_ingestion.png)

## Testing report
| Test            | issue reported | issue reproduced | issue identified | issue fixed | Status |
|-----------------|----------------|------------------|------------------|-------------|-------------|
| 2nd ingestion | Yes              | No             | NA     | NA     | OK   

# 3rd ingestion
In this 3rd ingestion, we want to update again all the deals as of `2021-04-01` for which the `PortfolioId=1`, using the following csv file (Date, PortfolioId, TradeId, Fact):

```csv
2021-04-01,1,1,100
2021-04-01,1,2,150
2021-04-01,1,3,300
```

## API call

### Configuration
```json
{
  "rafaleHost": "review_master.opensee.ninja",
  "rafalePort": "8080",
  "user": "alexandre",
  "password": "********",
  "token": "********",
  "module": "Test_HSBC_Alexandre",
  "table": "Test_HSBC_Alexandre",
  "as_of": "2021-04-01",
  "branch": "Test_HSBC_Alexandre_V4",
  "commit_hash": "**************",
  "block_id": "PortfolioId/1"
}
```

### Endpoint
```
{{ _.rafaleHost }}:{{ _.rafalePort }}/tables/{{ _.table }}/commits/{{ _.commit_hash }}/{{ _.as_of }}/{{ _.block_id }}
```
Please note that the `blockid` used in the API call is equal to `PortfolioId/1`

## Output
### Fact table
| Date       | PortfolioId | TradeId | Fact  | commit               | factor | nb_aggregated |
|------------|-------------|---------|-------|----------------------|--------|---------------|
| 2021-04-01 | 1           | 1       | 10.0  | -7060435086791665957 | 1      | 1             |
| 2021-04-01 | 1           | 2       | 20.0  | -7060435086791665957 | 1      | 1             |
| 2021-04-01 | 2           | 1       | 50.0  | -7060435086791665957 | 1      | 1             |
| 2021-04-01 | 2           | 2       | 80.0  | -7060435086791665957 | 1      | 1             |
| 2021-04-01 | 1           | 1       | 10.0  | -6283472006519261996 | -1     | 1             |
| 2021-04-01 | 1           | 2       | 20.0  | -6283472006519261996 | -1     | 1             |
| 2021-04-01 | 1           | 2       | 100.0 | -6283472006519261996 | 1      | 1             |
| 2021-04-01 | 1           | 3       | 150.0 | -6283472006519261996 | 1      | 1             |
| 2021-04-01 | 1           | 2       | 100.0 | 6689630818540908722  | -1     | 1             |
| 2021-04-01 | 1           | 3       | 150.0 | 6689630818540908722  | -1     | 1             |
| 2021-04-01 | 1           | 1       | 100.0 | 6689630818540908722  | 1      | 1             |
| 2021-04-01 | 1           | 2       | 150.0 | 6689630818540908722  | 1      | 1             |
| 2021-04-01 | 1           | 3       | 300.0 | 6689630818540908722  | 1      | 1             |

### FAT UI
![3rd ingestion](src/img/FAT_UI_5-2-101-3rd_ingestion.png)

## Testing report
| Test            | issue reported | issue reproduced | issue identified | issue fixed | Status |
|-----------------|----------------|------------------|------------------|-------------|-------------|
| 3rd ingestion | Yes              | No             | NA     | NA     | OK   