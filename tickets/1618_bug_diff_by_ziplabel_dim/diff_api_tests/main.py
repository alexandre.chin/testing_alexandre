import os
import time
import func
import pandas as pd
import json

########################### GLOBAL PARAMETERS ##########################

# server = "http://review_release-5-13-x.opensee.ninja:8080"
server = "http://review_master.opensee.ninja:8080"
#server = "http://devtest-rhel.opensee.ninja:8080"
# server = "http://devtest.opensee.ninja:8080"
# server="http://review_1540-fix-ingestion-wrapper.opensee.ninja:8080"

user = "admin"
password = "Welcome?"

module = "test_Arrays_diff_Caroline"
table = "test_Arrays_diff_trade_FactTable"
dict = "test_Arrays_diff_csa_Dictionary"
ziptable = "test_Arrays_diff_riskfactors_Ziptable"

script_folder = os.path.dirname(os.path.realpath(__file__))

result = {}
token = None

steps = {
    ############# PING AND LOGIN #############
    # ping
    'ping': {'type': 'ping', 'method': 'get', 'route_parameters': {'server': server}, 'body': {}},
    # login
    'login_admin': {'type': 'login', 'method': 'post', 'route_parameters': {'server': server}, 'body': {"identifier": user, "password": password}},

    ############# DM #############
    # "delete_dm":{'type':'deleteDM','method':'delete','route_parameters':{'server':server,'module':module},'body':{"hardDelete": True}},
    # "validate_dm":{'type':'validateDM','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/dm/rdm_testing_arrays.json'},
    # "create_dm":{'type':'createDM','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/dm/rdm_testing_arrays.json'},

    # #ingestion settings
    # "get_ingestionSettings_0":{'type':'ingestionSettings','method':'get','route_parameters':{'server':server,'module':module,'table':table},'body':{}},
    # "post_ingestionSettings":{'type':'ingestionSettings','method':'post','route_parameters':{'server':server,'module':module,'table':table},'body':f'{script_folder}/source/json_bodies/testing_Arrays_trade_FactTable_ingestion_settings.json'},
    # "get_ingestionSettings":{'type':'ingestionSettings','method':'get','route_parameters':{'server':server,'module':module,'table':table},'body':{}},

    # ############# INGESTION #############

    # # ingest dict
    # "dict_ingest":{'type':'dictIngest','method':'post','route_parameters':{'server':server,'dict':dict,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/csa_Dictionary.csv'},
    # "zip_ingest":{'type':'zipIngest','method':'post','route_parameters':{'server':server,'table':ziptable},'body':f'{script_folder}/source/data/riskfactors_ZipTable.csv'},

    # # # ingest initial 1
    # "open_initial_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'initial_1'},'body':None},
    # "ingest_initial_1":{'type':'ingest','transaction':'open_initial_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/trade_FactTable_initial_1.csv'},
    # "close_initial_1":{'type':'closeTransaction','method':'post','transaction':'open_initial_1','route_parameters':{'server':server,'table':table,'destinationBranch':'initial'},'body':{"closingMode": {"Merge": {"force":True}}}},

    # # # create official branch
    # "open_official_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'create official branch'},'body':None},
    # "close_official_1":{'type':'closeTransaction','method':'post','transaction':'open_official_1','route_parameters':{'server':server,'table':table,'destinationBranch':'official'},'body':{"closingMode": {"Merge": {"force":True}}}},

    # # ingest initial_2
    # "open_initial_2":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'initial_2'},'body':None},
    # "ingest_initial_2":{'type':'ingest','transaction':'open_initial_2','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/trade_FactTable_initial_2.csv'},

    # # ingest adj_1
    # "open_adj_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'official','comment':'adj_1'},'body':None},
    # "ingest_adj_1":{'type':'ingest','transaction':'open_adj_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','blockId':'tradeId/trade1/book/book1'},'body':f'{script_folder}/source/data/trade_FactTable_adj_1.csv'},

    # # close initial_2
    # "close_initial_2":{'type':'closeTransaction','method':'post','transaction':'open_initial_2','route_parameters':{'server':server,'table':table,'destinationBranch':'initial'},'body':{"closingMode": {"Merge": {"force":False}}}},
    # "merge_initial_2_in_official":{'type':'merge','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','destinationBranch':'official'},'body':{"comment": "merging initial_2 into official", "conflictManagement": "KeepSourceBranch", "concurrencyManagement":"None","moveBranch": "Destination"}},

    # # close adj_1
    # "close_adj_1":{'type':'closeTransaction','method':'post','transaction':'open_adj_1','route_parameters':{'server':server,'table':table,'destinationBranch':'official'},'body':{"closingMode": {"Merge": {"force":False}}}},

    # # ingest initial_3
    # "open_initial_3":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'initial_3'},'body':None},
    # "ingest_initial_3":{'type':'ingest','transaction':'open_initial_3','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/trade_FactTable_initial_3.csv'},

    # # ingest adj_2
    # "open_adj_2":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'official','comment':'adj_2'},'body':None},
    # "ingest_adj_2":{'type':'ingest','transaction':'open_adj_2','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07',"blockId":"tradeId/book"},'body':f'{script_folder}/source/data/trade_FactTable_adj_2.csv'},

    # # close initial_3
    # "close_initial_3":{'type':'closeTransaction','method':'post','transaction':'open_initial_3','route_parameters':{'server':server,'table':table,'destinationBranch':'initial'},'body':{"closingMode": {"Merge": {"force":False}}}},
    # "merge_initial_3_in_official":{'type':'rebase','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','destinationBranch':'official'},'body':{"comment": "merging initial_3 into official", "conflictManagement": "KeepSourceBranch", "concurrencyManagement":"None","moveBranch": "Destination"}},

    # # close adj_2
    # "close_adj_2":{'type':'closeTransaction','method':'post','transaction':'open_adj_2','route_parameters':{'server':server,'table':table,'destinationBranch':'official'},'body':{"closingMode": {"Merge": {"force":False}}}},

    # # CALLS WORKING OK
    # #QUERY ON METRICS WITH SAME CLAUSE
    "query_1ziplabel_1dim_official": {'type': 'queryCheckpoint', 'method': 'post', 'route_parameters': {'server': server, 'module': module}, 'body': f'{script_folder}/source/checkpoints/query_1ziplabel_1dim_official.json'},
    "query_1ziplabel_1dim_initial": {'type': 'queryCheckpoint', 'method': 'post', 'route_parameters': {'server': server, 'module': module}, 'body': f'{script_folder}/source/checkpoints/query_1ziplabel_1dim_initial.json'},

    # # CALLS WITH BUGS
    # DIFF ON 2 METRICS ON ARRAYFACTS, BY CLAUSE COMBINING 1 ZIPLABEL 1 DECO/DIM
    "diff_1ziplabel_1dim": {'type': 'diffCheckpoint', 'method': 'post', 'route_parameters': {'server': server, 'module': module}, 'body': f'{script_folder}/source/checkpoints/diff_off_vs_init_1ziplabel_1dim.json'},
    "diff_1ziplabel_1deco": {'type': 'diffCheckpoint', 'method': 'post', 'route_parameters': {'server': server, 'module': module}, 'body': f'{script_folder}/source/checkpoints/diff_off_vs_init_1ziplabel_1deco.json'},

}

start = time.time()
print(f"Tests on going...")

all_tests_ok = True
script_folder = os.path.dirname(os.path.realpath(__file__))

full_log_path = f"{script_folder}/full_logs.log"
error_log_path = f"{script_folder}/error_logs.log"

# clear log file
with open(full_log_path, "w") as log_file:
    pass
with open(error_log_path, "w") as log_file:
    pass

for step in steps:
    # time.sleep(2)
    if step == "dict_wrapper_test":
        print("xxx")
    if steps[step]['type'] in ['ingest', 'softDelete', 'closeTransaction']:
        # add asof and transaction_id to route parameters
        steps[step]['route_parameters']['transaction_id'] = result[steps[step]
                                                                   ['transaction']]['response']['response']
        steps[step]['route_parameters']['asof'] = result[steps[step]
                                                         ['transaction']]['route_parameters']['asof']
        time.sleep(1)

    # overload
    result = func.overload(steps[step], result)

    # api call
    temp = func.api_call(token, **steps[step])
    result[step] = func.store_api_call(step, temp)

    # for k, v in  result[step].items():
    #     print("coucou", k,":", v)

    if steps[step]['type'] == 'login':
        token = result[step]['response']['response']['token']

###### Print logs #######
func.print_logs(result, full_log_path)

result_errors = {}

for step in result:
    if result[step]['response']['status_code'] in [200, 201, 203, 204]:
        if result[step]['type'] in ['ping', 'login']:
            result_errors[step] = result[step]
        elif 'Checkpoint' in result[step]['type']:
            if result[step]['checkpoint_status'] != 'OK':
                result_errors[step] = result[step]
                all_tests_ok = False
    else:
        result_errors[step] = result[step]
        all_tests_ok = False

print(f"Tests completed in {time.time()-start:.2f} sec. /n")

if all_tests_ok:
    print(
        f'all tests succeeded. full logs file available here: {full_log_path}')
else:
    func.print_logs(result_errors, error_log_path)
    print(
        f'Some tests failed. full logs and error logs file available here:{script_folder}')
