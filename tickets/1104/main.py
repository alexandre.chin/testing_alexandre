import sys
import os
import json
import requests
import time

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
log_full_path=f"{script_folder}/logs.log"

sys.path.insert(1, f'{parent_folder}/toolbox/source')
import func

result={}

########################### GLOBAL PARAMETERS ##########################

# global parameters
server = "http://review_feature-branch-instead-commit-details.opensee.ninja:8080"
# server = "http://dev.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# queries parameters
query_parameters=[
  {'query_number':1},
]

# clear log file
with open(log_full_path, "w") as log_file:
  pass

########################### LOGIN TO SERVER ##########################
start = time.time()
# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
kwcnx = dict(headers={'Authorization': token})

########################### TESTING DETAILS ##########################
print('testing query...')
module='HistoVaR'
json_full_path=f"{script_folder}/source/body.json"
data=func.load_json(json_full_path)

# POST module/HistoVaR/details

try:
  result.update(func.details_api(server,token,module,data,"1"))
except:
  result["details"]={'status':'KO: please debug for investigation'}

print(f'Time: {time.time()-start:.2f} sec') 

 
# print results
print('generating log file...')
func.print_logs(result,log_full_path)

