from re import S
import sys
import os
import json
import requests
import time
import asyncio

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
log_full_path=f"{script_folder}/logs.log"

sys.path.insert(1, f'{parent_folder}/toolbox/source')
import func

result={}

########################### GLOBAL PARAMETERS ##########################

# global parameters
server = "http://review_master.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# parameters
module = "module_alexandre"
table= "Facttable"
dict ="Dictionary"


# queries parameters
steps=[
# initial 1
{'open_initial_1':{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'initial','comment':'initial_1'}},
{"ingest_initial_1":{'type':'ingest','transaction':'open_initial_1','file_name':'initial_1.csv'}},
{"close_initial_1":{'type':'closeTransaction','transaction':'open_initial_1','destinationBranch':'initial'},'closingMode':{"closingMode": {"Rebase": {"force":True}}}},
{"merge_initial_in_official":{'type':'merge','as_of':'2021-05-07','sourceBranch':'initial','destinationBranch':'official','body':{"comment": "merging initial into official", "conflictManagement": "KeepDestinationBranch"}}},

# initial_2
{'open_initial_2':{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'initial','comment':'initial_2'}},
{"ingest_initial_2":{'type':'ingest','transaction':'open_initial_2','file_name':'initial_2.csv'}},

# adj_1
{'open_adj_1':{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'official','comment':'adj_1'}},
{"ingest_adj_1":{'type':'ingest','transaction':'open_adj_1','file_name':'adj_1.csv'}},

# close initial_2
{"close_initial_2":{'type':'closeTransaction','transaction':'open_initial_2','destinationBranch':'initial'},'closingMode':{"closingMode": {"Rebase": {"force":True}}}},
{"merge_initial_in_official":{'type':'merge','as_of':'2021-05-07','sourceBranch':'initial','destinationBranch':'official','body':{"comment": "merging initial into official", "conflictManagement": "KeepDestinationBranch"}}},

# close adj_1
{"close_adj_1":{'type':'closeTransaction','transaction':'open_adj_1','destinationBranch':'official'},'closingMode':{"closingMode": {"Merge": {"force":False}}}},

# initial_3
{'open_initial_3':{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'initial','comment':'initial_3'}},
{"ingest_initial_3":{'type':'ingest','transaction':'open_initial_3','file_name':'initial_3.csv'}},

# adj_2
{'open_adj_2':{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'official','comment':'adj_2'}},
{"ingest_adj_2":{'type':'ingest','transaction':'open_adj_2','file_name':'adj_2.csv'}},

# close initial_3
{"close_initial_3":{'type':'closeTransaction','transaction':'open_initial_3','destinationBranch':'initial'},'closingMode':{"closingMode": {"Rebase": {"force":True}}}},
{"merge_initial_in_official":{'type':'merge','as_of':'2021-05-07','sourceBranch':'initial','destinationBranch':'official','body':{"comment": "merging initial into official", "conflictManagement": "KeepDestinationBranch"}}},

# close adj_2
{"close_adj_2":{'type':'closeTransaction','transaction':'open_adj_2','destinationBranch':'official'},'closingMode':{"closingMode": {"Merge": {"force":False}}}},

]


# clear log file
with open(log_full_path, "w") as log_file:
  pass

########################### LOGIN TO SERVER ##########################

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
# kwcnx = dict(headers={'Authorization': token})

headers={'Authorization': token}

########################### CREATING MODULE WITH AUTOCUBE ##########################

headers['Content-Type']='json'

url=f"{server}/autocube/{module}"

json_full_path=f"{script_folder}/source/autocube_input.json"
autocube_input_dict=func.load_json(json_full_path)

try:
  #Deletes module if it aleady exists
  autocube_input_dict['mode']='Delete'

  resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
  time.sleep(2)
finally:
  #creates module
  autocube_input_dict['mode']='Create'

  resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
  time.sleep(2)

  print(f"module {module} created")

########################### DATA INGESTION ##########################
headers['Content-Type']='text/csv'

for step in steps:

    # ingest fact table
    if step['type']=='openTransaction':

        as_of=step['as_of']
        sourceBranch=step['sourceBranch']

        comment=step['comment']
        file_name=step['file_name']
        closingMode=step['closingMode']

        #open transaction
        url=f"{server}/tables/{table}/transaction/{as_of}/from/{sourceBranch}/{comment}"

        resp=requests.request(method='post',url=url, headers=headers)

        step['transaction_id']=resp.text
        print(f"Opening transaction: {step['transaction_id']}")
        time.sleep(2)

    if step['type']=='ingest':
        #ingest fact table
        transaction_id=steps[step['transaction']]['transaction_id']
        as_of=steps[step['transaction']]['as_of']
        file_name=step['file_name']

        url=f"{server}/tables/{table}/commits/{transaction_id}/{as_of}"

        file_full_path=f"{script_folder}/source/data/{file_name}"

        with open(file_full_path, 'rb') as fp:
            resp=requests.request(method='post',url=url,data=fp, headers=headers)

        print(f"Ingesting facts: {resp}")
        if resp.status_code not in (200,204):
            print(f"error: {resp.text}")
        else:
            pass

        time.sleep(1)

    if step['type']=='closeTransaction':
        #close transaction

        transaction_id=steps[step['transaction']]['transaction_id']
        as_of=steps[step['transaction']]['as_of']
        destinationBranch=step['destinationBranch']
        body=step['body']

        destinationBranch=step['destinationBranch']
        url=f"{server}/tables/{table}/closeTransaction/{transaction_id}/{as_of}/to/{destinationBranch}"
        
        resp=requests.request(method='post',url=url,data=body, headers=headers)
        print(f"Closing transaction: {resp.text}\n")

        time.sleep(1)
    
    if step['type']=='merge':
        headers['Content-Type']='json'

        as_of=step['as_of']
        sourceBranch=step['sourceBranch']
        destinationBranch=step['destinationBranch']
        body=step['body']

        url= f'{server}/tables/{table}/merge/{as_of}/from/{sourceBranch}/into/{destinationBranch}'
        body={"comment": "testing", "conflictManagement": "Abort"}

        resp=requests.request(method='post',url=url,json=body, headers=headers)

        print(f"Merging: {resp}")
        if resp.status_code not in (200,204):
            print(f"error: {resp.text}")
        else:
            pass

        time.sleep(1)

print("test completed")