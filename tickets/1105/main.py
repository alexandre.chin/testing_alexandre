import sys
import os
import json
import requests
import time

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
log_full_path=f"{script_folder}/logs.log"

sys.path.insert(1, f'{parent_folder}/toolbox/source')
import func

result={}

########################### GLOBAL PARAMETERS ##########################

# global parameters
server = "http://review_1160-linked-array-zip-join.opensee.ninja:8080"
# server = "http://dev.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# queries parameters
query_parameters=[
  {'query_number':1},
]

# clear log file
with open(log_full_path, "w") as log_file:
  pass

########################### LOGIN TO SERVER ##########################
start = time.time()
# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
headers = {'Authorization': token}

########################### TESTING CALCULATE ##########################
print('testing calculate...')
module='HistoVaR'
json_full_path=f"{script_folder}/source/body.json"
data=func.load_json(json_full_path)


url=f"{server}/module/{module}/calculate"

resp=requests.request(method='post',url=url,json=data, headers=headers)

# response
try:
    response=resp.json()

except:
    response=resp.text

time.sleep(1)

result['calculate']={}

result['calculate'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

print(f'Time: {time.time()-start:.2f} sec') 

 
# print results
print('generating log file...')
func.print_logs(result,log_full_path)

