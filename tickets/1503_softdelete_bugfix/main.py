import os
import time
import func
import pandas as pd
import json

########################### GLOBAL PARAMETERS ##########################

server = "http://review_1503-backport-5-12.opensee.ninja:8080"
user = "admin"
password = "Welcome?"

module = "module_alexandre"
table= "Facttable"
dict ="Dictionary"
ziptable="Ziptable"

script_folder=os.path.dirname(os.path.realpath(__file__))

result={}
token=None

steps={
############# PING AND LOGIN #############
#ping
'ping':{'type':'ping','method':'get','route_parameters':{'server':server},'body':{}},
#login
'login_admin':{'type':'login','method':'post','route_parameters':{'server':server},'body':{"identifier": user,"password": password}},

############# DM #############
#autocube
"delete_dm":{'type':'autocube','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/dm/autocube_delete.json'},
"create_dm":{'type':'autocube','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/dm/autocube_input.json'},

#ingestion settings
"get_ingestionSettings":{'type':'ingestionSettings','method':'get','route_parameters':{'server':server,'module':module,'table':table},'body':{}},
"post_ingestionSettings":{'type':'ingestionSettings','method':'post','route_parameters':{'server':server,'module':module,'table':table},'body':{},'overload':[{'step':'get_ingestionSettings','body':{'insertMethod':'Linear'}}]},

############# INGESTION #############
#ingest official_1
"open_official_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'official_1'},'body':None},
"ingest_official_1":{'type':'ingest','transaction':'open_official_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/initial_1.csv'},
"close_official_1":{'type':'closeTransaction','method':'post','transaction':'open_official_1','route_parameters':{'server':server,'table':table,'destinationBranch':'official'},'body':{"closingMode": {"Merge": {"force":True}}}},

#ingest official_2
"open_official_2":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'official','comment':'official_2'},'body':None},
"ingest_official_2":{'type':'ingest','transaction':'open_official_2','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/initial_2.csv'},
"close_official_2":{'type':'closeTransaction','method':'post','transaction':'open_official_2','route_parameters':{'server':server,'table':table,'destinationBranch':'official'},'body':{"closingMode": {"Merge": {"force":True}}}},

# ingest adj_1
"open_adj_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'official','comment':'adj_1'},'body':None},
"ingest_adj_1":{'type':'ingest','transaction':'open_adj_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','blockId':'StaticKey1/trade_1/StaticKey2/Cpty_1'},'body':f'{script_folder}/source/data/adj_1.csv'},
"close_adj_1":{'type':'closeTransaction','method':'post','transaction':'open_adj_1','route_parameters':{'server':server,'table':table,'destinationBranch':'official'},'body':{"closingMode": {"Merge": {"force":True}}}},

# ingest adj_2
"open_adj_2":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'official','comment':'adj_2'},'body':None},
"ingest_adj_2":{'type':'ingest','transaction':'open_adj_2','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07',"blockId":"StaticKey1/trade_1/StaticKey2/Cpty_2"},'body':f'{script_folder}/source/data/adj_2.csv'},
"close_adj_2":{'type':'closeTransaction','method':'post','transaction':'open_adj_2','route_parameters':{'server':server,'table':table,'destinationBranch':'official'},'body':{"closingMode": {"Merge": {"force":True}}}},

# softDelete
"open_softdelete":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'official','comment':'softdelete'},'body':None},
"softDelete":{'type':'softDelete','transaction':'open_softdelete','method':'delete','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/json_bodies/softdelete_body.json'},
"close_softdelete":{'type':'closeTransaction','method':'post','transaction':'open_softdelete','route_parameters':{'server':server,'table':table,'destinationBranch':'official'},'body':{"closingMode": {"Merge": {"force":False}}}},

# query official checkpoint
"query_official_3":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_official_3.json'},

# details official checkpoint
"details_3":{'type':'detailsCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/details_official_3.json'},

}

start = time.time()
print(f"Tests on going...")

all_tests_ok=True
script_folder=os.path.dirname(os.path.realpath(__file__))

full_log_path=f"{script_folder}/full_logs.log"
error_log_path=f"{script_folder}/error_logs.log"

# clear log file
with open(full_log_path, "w") as log_file:
    pass
with open(error_log_path, "w") as log_file:
    pass

for step in steps:
    # time.sleep(1)
    if steps[step]['type'] in ['ingest','softDelete','closeTransaction']:
        #add asof and transaction_id to route parameters
        steps[step]['route_parameters']['transaction_id']=result[steps[step]['transaction']]['response']['response']
        steps[step]['route_parameters']['asof']=result[steps[step]['transaction']]['route_parameters']['asof']
        time.sleep(1)

    # overload
    result=func.overload(steps[step],result)

    # api call
    temp=func.api_call(token,**steps[step])
    result[step]=func.store_api_call(step,temp)

    if steps[step]['type']=='login':
        token=result[step]['response']['response']['token']

###### Print logs #######
func.print_logs(result,full_log_path)

result_errors={}

for step in result:
    if result[step]['response']['status_code'] in [200,201,203,204]:
        if result[step]['type'] in ['ping','login']:
            result_errors[step]=result[step]
        elif 'Checkpoint' in result[step]['type']:
            if result[step]['checkpoint_status']!='OK':
                result_errors[step]=result[step]
                all_tests_ok=False
    else:
        result_errors[step]=result[step]
        all_tests_ok=False

func.print_logs(result_errors,error_log_path)

print(f"Tests completed in {time.time()-start:.2f} sec. /n")

if all_tests_ok:
    print(f'all tests succeeded. full logs file available here: {full_log_path}')
else:
    print(f'Some tests failed. full logs adn error logs file available here:{script_folder}')
