
import sys
import os
import json
import requests
import time
import asyncio

# path
parent_folder = os.getcwd()
script_folder = os.path.dirname(os.path.realpath(__file__))
log_full_path = f"{script_folder}/logs.log"

sys.path.insert(1, f'{parent_folder}/toolbox/source')
import func
result = {}

########################### GLOBAL PARAMETERS ##########################

# global parameters
server = "http://review_feature-rebase-merge-4.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# parameters
module = "test_954"
table = "Facttable"
dict = "Dictionary"


# queries parameters
ingestion_parameters = [
    # test 1
    {
        'request_parameters_fact': {'file_name': 'ingestion_fact_test_1.csv', 'as_of': '2020-06-01', 'sourceBranch': 'official', 'destinationBranch': 'official', 'comment': 'ingestion_fact_test_1'}
    },

    {
        'request_parameters_fact': {'file_name': 'ingestion_fact_test_2.csv', 'as_of': '2020-06-01', 'sourceBranch': 'official', 'destinationBranch': 'official', 'comment': 'ingestion_fact_test_2'}
    },

    {
        'request_parameters_fact': {'file_name': 'ingestion_fact_test_3.csv', 'as_of': '2020-06-01', 'sourceBranch': 'official', 'destinationBranch': 'private_1', 'comment': 'ingestion_fact_test_3'}
    },

    {
        'request_parameters_fact': {'file_name': 'ingestion_fact_test_4.csv', 'as_of': '2020-06-01', 'sourceBranch': 'official', 'destinationBranch': 'private_2', 'comment': 'ingestion_fact_test_4'}
    },

    {
        'request_parameters_fact': {'file_name': 'ingestion_fact_test_5.csv', 'as_of': '2020-06-01', 'sourceBranch': 'official', 'destinationBranch': 'official', 'comment': 'ingestion_fact_test_5'}
    }
]

# merge parameters
merge_parameters = [
    # merge 1
    {'as_of': '2020-06-01', 'sourceBranch': 'private_1', 'destinationBranch': 'official', 'body': {"comment": "merging private_1 into official",
                                                                                                   "conflictManagement": "KeepSourceBranch", "concurrencyManagement": "Abort"}, 'merge_number': 1},
    # merge 2
    {'as_of': '2020-06-01', 'sourceBranch': 'private_1', 'destinationBranch': 'official', 'body': {"comment": "merging private_1bis into official",
                                                                                                   "conflictManagement": "KeepSourceBranch", "concurrencyManagement": "Abort"}, 'merge_number': 2}
]

# clear log file
with open(log_full_path, "w") as log_file:
    pass

########################### LOGIN TO SERVER ##########################
start = time.time()

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server, user, password))

# store token
token = result['login'][f'200 POST {server}/login']['response']['token']
# kwcnx = dict(headers={'Authorization': token})

headers = {'Authorization': token}

########################### CREATING MODULE WITH AUTOCUBE ##########################
print('creating datamodel...')

headers['Content-Type'] = 'json'

url = f"{server}/autocube/{module}"

json_full_path = f"{script_folder}/source/autocube_input.json"
autocube_input_dict = func.load_json(json_full_path)

try:
    # Deletes module if it aleady exists
    autocube_input_dict['mode'] = 'Delete'

    resp = requests.request(method='post', url=url,
                            json=autocube_input_dict, headers=headers)
    time.sleep(2)
finally:
    # creates module
    autocube_input_dict['mode'] = 'Create'

    resp = requests.request(method='post', url=url,
                            json=autocube_input_dict, headers=headers)
    time.sleep(2)

    print(f"module {module} created")

########################### DATA INGESTION ##########################
headers['Content-Type'] = 'text/csv'

for request_parameters in ingestion_parameters:

    # ingest fact table
    if 'request_parameters_fact' in request_parameters.keys():

        as_of = request_parameters['request_parameters_fact']['as_of']
        sourceBranch = request_parameters['request_parameters_fact']['sourceBranch']
        destinationBranch = request_parameters['request_parameters_fact']['destinationBranch']
        comment = request_parameters['request_parameters_fact']['comment']
        file_name = request_parameters['request_parameters_fact']['file_name']

        # open transaction
        url = f"{server}/tables/{table}/transaction/{as_of}/from/{sourceBranch}/{comment}"

        resp = requests.request(method='post', url=url, headers=headers)

        transaction_id = resp.text
        print(f"Opening transaction: {comment}")
        time.sleep(2)

        # ingest fact table
        url = f"{server}/tables/{table}/commits/{transaction_id}/{as_of}"

        file_full_path = f"{script_folder}/source/data/{file_name}"

        with open(file_full_path, 'rb') as fp:
            resp = requests.request(
                method='post', url=url, data=fp, headers=headers)
            time.sleep(2)

        print(f"Ingesting facts: {resp}")
        if resp.status_code not in (200, 204):
            print(f"error: {resp.text}")
        else:
            pass

        # close transaction
        url = f"{server}/tables/{table}/closeTransaction/{transaction_id}/{as_of}/to/{destinationBranch}"

        resp = requests.request(method='post', url=url, headers=headers)
        print(f"Closing transaction: {resp.text}\n")

        time.sleep(2)

    # ingest dict
    elif 'request_parameters_dict' in request_parameters.keys():
        as_of = request_parameters['request_parameters_dict']['as_of']
        sourceBranch = request_parameters['request_parameters_dict']['sourceBranch']
        destinationBranch = request_parameters['request_parameters_dict']['destinationBranch']
        comment = request_parameters['request_parameters_dict']['comment']
        file_name = request_parameters['request_parameters_dict']['file_name']

        url = f"{server}/dictionaries/{dict}/{as_of}/from/{sourceBranch}/to/{destinationBranch}/{comment}"

        file_full_path = f"{script_folder}/source/data/{file_name}"

        with open(file_full_path, 'rb') as fp:
            resp = requests.request(
                method='post', url=url, data=fp, headers=headers)
            time.sleep(2)

        print(f"Ingesting dict: {resp}")
        if resp.status_code not in (200, 204):
            print(f"error: {resp.text}\n")
        else:
            print(f"\n")

########################### MERGE OR REBASE ##########################


async def merge_api_async(merge_parameters, timer):
    time.sleep(timer)
    server = merge_parameters['server']
    token = merge_parameters['token']
    table = merge_parameters['table']
    as_of = merge_parameters['as_of']
    sourceBranch = merge_parameters['sourceBranch']
    destinationBranch = merge_parameters['destinationBranch']
    body = merge_parameters['body']
    merge_number = merge_parameters['merge_number']
    print(f'starting merge {merge_number}')
    print(f'Time: {time.time()-start:.2f} sec')

    result = await func.merge_api(server, token, table, as_of, sourceBranch, destinationBranch, body, merge_number)
    print(f'merge {merge_number} completed')
    print(f'Time: {time.time()-start:.2f} sec')
    return result

for element in merge_parameters:
    element['server'] = server
    element['token'] = token
    element['table'] = table


loop = asyncio.get_event_loop()
tasks = [loop.create_task(merge_api_async(merge_parameters[i], 4))
        for i in range(0, len(merge_parameters))]
loop.run_until_complete(asyncio.wait(tasks))
loop.close()

# print results
for task in tasks:
    result.update(task._result)

print('generating log file...')
func.print_logs(result, log_full_path)

print(f"test completed in {time.time()-start:.2f} sec")
