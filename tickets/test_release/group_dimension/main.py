import os
import time
import func
import pandas as pd
import json

########################### GLOBAL PARAMETERS ##########################

# server = "http://review_release-5-13-x.opensee.ninja:8080"
server = "http://review_master.opensee.ninja:8080"
# server = "http://devtest-rhel.opensee.ninja:8080"
# server = "http://devtest.opensee.ninja:8080"
#server ="http://devtest-rhel.opensee.ninja:8080"

user = "admin"
password = "Welcome?"

module="testing_Arrays"
table="testing_Arrays_trade_FactTable"
dict="testing_Arrays_csa_Dictionary"
ziptable="testing_Arrays_riskfactors_Ziptable"
pivot="testing_Arrays_trade_FactTable"


script_folder=os.path.dirname(os.path.realpath(__file__))

dimension_group= { "enumerable": True, "folder": "Main","type": "Default","anonymize": False,"description": "","alias": ""}

result={}
token=None

steps={
############# PING AND LOGIN #############
#ping
'ping':{'type':'ping','method':'get','route_parameters':{'server':server},'body':{}},
#login
'login_admin':{'type':'login','method':'post','route_parameters':{'server':server},'body':{"identifier": user,"password": password}},

############# USERGROUPS AND POLICIES #############
# #getPolicies
# "getPolicies":{'type':'getPolicies','method':'get','route_parameters':{'server':server,'userGroup':'Admin'},'body':None},
# #addPolicy
# "addPolicy":{'type':'addPolicy','method':'put','route_parameters':{'server':server,'userGroup':'Admin'},'body':{'id': '9de78f13-bc16-4bbb-abcd-8b5db16c4ffc', 'actions': ['canCreateValidationRequest'], "resource": "validationrequest:WF_template"}},

############# DM #############
#autocube
# "delete_dm":{'type':'autocube','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/dm/autocube_delete.json'},
# "create_dm":{'type':'autocube','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/dm/autocube_input.json'},

"delete_dm":{'type':'deleteDM','method':'delete','route_parameters':{'server':server,'module':module},'body':{"hardDelete": True}},
"validate_dm":{'type':'validateDM','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/dm/rdm_testing_arrays.json'},
"create_dm":{'type':'createDM','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/dm/rdm_testing_arrays.json'},

#ingestion settings
"get_ingestionSettings_0":{'type':'ingestionSettings','method':'get','route_parameters':{'server':server,'module':module,'table':table},'body':{}},
"post_ingestionSettings":{'type':'ingestionSettings','method':'post','route_parameters':{'server':server,'module':module,'table':table},'body':f'{script_folder}/source/json_bodies/testing_Arrays_trade_FactTable_ingestion_settings.json'},
"get_ingestionSettings":{'type':'ingestionSettings','method':'get','route_parameters':{'server':server,'module':module,'table':table},'body':{}},


############# GROUPS CREATION #############

"group_1":{'type':'CreateGroupDimension','method':'post','route_parameters':{'server':server,'module':module,'group_name':'group_1'},'body':f'{script_folder}/source/json_bodies/group_1.json'},
"get_module_def":{'type':'ModuleDefinition','method':'get','route_parameters':{'server':server,'module':module},'body':{}},
"post_module_def":{'type':'ModuleDefinition','method':'post','route_parameters':{'server':server,'module':module}, 'body':None,'overload':[{'step':'get_module_def','body':{},'new_dimension':{'group_1':dimension_group}}]},
#"post_module_def":{'type':'ModuleDefinition','method':'post','route_parameters':{'server':server,'module':module}, 'body':None,'overload':[{'step':'get_module_def','route_parameters':{'test':'dimensions'},}]},
"get_pivot_def":{'type':'PivotDefinition','method':'get','route_parameters':{'server':server,'module':module,'pivot':pivot},'body':{}},
"post_pivot_def":{'type':'PivotDefinition','method':'post','route_parameters':{'server':server,'module':module,'pivot':pivot}, 'body':None,'overload':[{'step':'get_pivot_def','body':{},'new_dimension_pivot':['group_1']}]},


############# INGESTION #############

# ingest initial 1
"open_initial_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'initial_1'},'body':None},
"ingest_initial_1":{'type':'ingest','transaction':'open_initial_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/Facttable_1.csv'},
"close_initial_1":{'type':'closeTransaction','method':'post','transaction':'open_initial_1','route_parameters':{'server':server,'table':table,'destinationBranch':'initial'},'body':{"closingMode": {"Merge": {"force":True}}}},


# # query initial checkpoint
"query_initial_1":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_1.json'},

## modification of group_1
"group_1_bis":{'type':'CreateGroupDimension','method':'post','route_parameters':{'server':server,'module':module,'group_name':'group_1'},'body':f'{script_folder}/source/json_bodies/group_1_bis.json'},

# # query initial checkpoint

"query_initial_1_bis":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_1_bis.json'},


## delete group_1
"group_1_deletion":{'type':'CreateGroupDimension','method':'delete','route_parameters':{'server':server,'module':module,'group_name':'group_1'},'body':{"hardDelete": True}},


"query_initial_1_ter":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_1_ter.json'},


}

start = time.time()
print(f"Tests on going...")

all_tests_ok=True
script_folder=os.path.dirname(os.path.realpath(__file__))

full_log_path=f"{script_folder}/full_logs.log"
error_log_path=f"{script_folder}/error_logs.log"

# clear log file
with open(full_log_path, "w") as log_file:
    pass
with open(error_log_path, "w") as log_file:
    pass

for step in steps:
    # time.sleep(2)
    if step=="post_module_def":
        print("xxx")
    if steps[step]['type'] in ['ingest','softDelete','closeTransaction']:
        #add asof and transaction_id to route parameters
        steps[step]['route_parameters']['transaction_id']=result[steps[step]['transaction']]['response']['response']
        steps[step]['route_parameters']['asof']=result[steps[step]['transaction']]['route_parameters']['asof']
        time.sleep(1)

    # overload
    result=func.overload(steps[step],result)

    # api call
    temp=func.api_call(token,**steps[step])
    result[step]=func.store_api_call(step,temp)

    if steps[step]['type']=='login':
        token=result[step]['response']['response']['token']

###### Print logs #######
func.print_logs(result,full_log_path)

result_errors={}

for step in result:
    if step=='get_datamodel_def':
        print('salut')
    if result[step]['response']['status_code'] in [200,201,203,204]:
        if result[step]['type'] in ['ping','login']:
            result_errors[step]=result[step]
        elif 'Checkpoint' in result[step]['type']:
            if result[step]['checkpoint_status']!='OK':
                result_errors[step]=result[step]
                all_tests_ok=False
    else:
        result_errors[step]=result[step]
        all_tests_ok=False

print(f"Tests completed in {time.time()-start:.2f} sec. /n")

if all_tests_ok:
    print(f'all tests succeeded. full logs file available here: {full_log_path}')
else:
    func.print_logs(result_errors,error_log_path)
    print(f'Some tests failed. full logs and error logs file available here:{script_folder}')
