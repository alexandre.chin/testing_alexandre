import sys
import os
import json
import requests
import time

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
log_full_path=f"{script_folder}/logs.log"

sys.path.insert(1, f'{parent_folder}/toolbox/source')
import func

checkpoints_trigger=True
result={}

########################### GLOBAL PARAMETERS ##########################

# global parameters
# server = "http://review_release-5-11-x.opensee.ninja:8080"
server="http://review_feature-ingestion-by-block.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# parameters
module = "module_alexandre"
table= "Facttable"
dict ="Dictionary"

# queries parameters
steps={
# initial 1
"open_initial_1":{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'initial','comment':'initial_1'},
"ingest_initial_1":{'type':'ingest','transaction':'open_initial_1','file_name':'initial_1.csv'},
"close_initial_1":{'type':'closeTransaction','transaction':'open_initial_1','destinationBranch':'initial','body':{"closingMode": {"Rebase": {"force":False}}}},

"open_official_1":{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'official','comment':'official_1'},
"close_official_1":{'type':'closeTransaction','transaction':'open_official_1','destinationBranch':'official','body':{"closingMode": {"Rebase": {"force":False}}}},

"merge_initial_in_official_1":{'type':'merge','as_of':'2021-05-07','sourceBranch':'initial','destinationBranch':'official','body':{"comment": "merging initial into official", "conflictManagement": "KeepDestinationBranch", "concurrencyManagement":"None","moveBranch": "Destination"}},

# initial_2
'open_initial_2':{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'initial','comment':'initial_2'},
"ingest_initial_2":{'type':'ingest','transaction':'open_initial_2','file_name':'initial_2.csv'},

# adj_1
'open_adj_1':{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'official','comment':'adj_1'},
"ingest_adj_1":{'type':'ingest','transaction':'open_adj_1','file_name':'adj_1.csv','blockId':'StaticKey1/trade_1'},

# close initial_2
"close_initial_2":{'type':'closeTransaction','transaction':'open_initial_2','destinationBranch':'initial','body':{"closingMode": {"Rebase": {"force":False}}}},
"merge_initial_in_official_2":{'type':'merge','as_of':'2021-05-07','sourceBranch':'initial','destinationBranch':'official','body':{"comment": "merging initial into official", "conflictManagement": "KeepDestinationBranch", "concurrencyManagement":"None","moveBranch": "Destination"}},

# close adj_1
"close_adj_1":{'type':'closeTransaction','transaction':'open_adj_1','destinationBranch':'official','body':{"closingMode": {"Rebase": {"force":False}}}},

# initial_3
'open_initial_3':{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'initial','comment':'initial_3'},
"ingest_initial_3":{'type':'ingest','transaction':'open_initial_3','file_name':'initial_3.csv'},

# adj_2
'open_adj_2':{'type':'openTransaction','as_of':'2021-05-07','sourceBranch':'official','comment':'adj_2'},
"ingest_adj_2":{'type':'ingest','transaction':'open_adj_2','file_name':'adj_2.csv',"blocks":"StaticKey1/StaticKey2"},

# close initial_3
"close_initial_3":{'type':'closeTransaction','transaction':'open_initial_3','destinationBranch':'initial','body':{"closingMode": {"Merge": {"force":True}}}},
"merge_initial_in_official_3":{'type':'merge','as_of':'2021-05-07','sourceBranch':'initial','destinationBranch':'official','body':{"comment": "merging initial into official", "conflictManagement": "KeepDestinationBranch", "concurrencyManagement":"None","moveBranch": "Destination"}},

# close adj_2
"close_adj_2":{'type':'closeTransaction','transaction':'open_adj_2','destinationBranch':'official','body':{"closingMode": {"Merge": {"force":True}}}}
}

# clear log file
with open(log_full_path, "w") as log_file:
  pass

########################### LOGIN TO SERVER ##########################

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...',end="\n")
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
# kwcnx = dict(headers={'Authorization': token})

headers={'Authorization': token}

########################### CREATING MODULE WITH AUTOCUBE ##########################

headers['Content-Type']='json'

url=f"{server}/autocube/{module}"

json_full_path=f"{script_folder}/source/autocube_input.json"
autocube_input_dict=func.load_json(json_full_path)

dm_trigger=True

try:
    #Deletes module if it aleady exists
    autocube_input_dict['mode']='Delete'

    resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
    time.sleep(1)
finally:
    #creates module
    autocube_input_dict['mode']='Create'

    resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
    time.sleep(1)

    if resp.status_code!=200:
        dm_trigger=False

    # Set insertMethod to Linear
    url=f"{server}/module/{module}/tables/{table}/ingestion_settings"
    resp=requests.request(method='get',url=url, headers=headers)
    body=resp.json()
    body["insertMethod"]="Linear"

    resp=requests.request(method='post',url=url,json=body, headers=headers)
    if resp.status_code!=200:
        dm_trigger=False

    if dm_trigger==True:
        print(f"module {module} created",end="\n")
    else:
        result={"datamodel error":"could not create datamodel; please debug to investigate"}
        print(result)
        func.print_logs(result,log_full_path)
        sys.exit()

########################### DATA INGESTION ##########################

for step in steps:
    type=steps[step]['type']

    # open transaction
    if type=='openTransaction':

        as_of=steps[step]['as_of']
        sourceBranch=steps[step]['sourceBranch']
        comment=steps[step]['comment']

        #open transaction
        url=f"{server}/tables/{table}/transaction/{as_of}/from/{sourceBranch}/{comment}"

        resp=requests.request(method='post',url=url, headers=headers)

        steps[step]['transaction_id']=resp.text

        time.sleep(1)

    if type=='ingest':
        headers['Content-Type']='text/csv'
        #ingest fact table
        transaction_id=steps[steps[step]['transaction']]['transaction_id']
        as_of=steps[steps[step]['transaction']]['as_of']
        file_name=steps[step]['file_name']

        if "blockId" in steps[step]:
            blockId=steps[step]['blockId']
            url = f"{server}/tables/{table}/commits/{transaction_id}/{as_of}/{blockId}"
        elif "blocks" in steps[step]:
            blocks=steps[step]['blocks']
            url = f"{server}/beta/transactions/{transaction_id}/blocks/{blocks}"
        else:
            url=f"{server}/tables/{table}/commits/{transaction_id}/{as_of}"

        file_full_path=f"{script_folder}/source/data/{file_name}"

        with open(file_full_path, 'rb') as fp:
            resp=requests.request(method='post',url=url,data=fp, headers=headers)

        time.sleep(1)

    if type=='closeTransaction':

        #close transaction
        transaction_id=steps[steps[step]['transaction']]['transaction_id']
        as_of=steps[steps[step]['transaction']]['as_of']
        destinationBranch=steps[step]['destinationBranch']
        body=steps[step]['body']

        destinationBranch=steps[step]['destinationBranch']
        url=f"{server}/tables/{table}/closeTransaction/{transaction_id}/{as_of}/to/{destinationBranch}"
        
        resp=requests.request(method='post',url=url,json=body, headers=headers)

        time.sleep(1)
    
    if type=='merge':
        headers['Content-Type']='json'

        as_of=steps[step]['as_of']
        sourceBranch=steps[step]['sourceBranch']
        destinationBranch=steps[step]['destinationBranch']
        body=steps[step]['body']

        url= f'{server}/tables/{table}/merge/{as_of}/from/{sourceBranch}/into/{destinationBranch}'

        resp=requests.request(method='post',url=url,json=body, headers=headers)

        time.sleep(1)

    if type=="queryCheckpoint":
        # Perform API call
        file_name=steps[step]['file_name']
        input_file_full_path=f"{script_folder}/source/checkpoints/{file_name}"
        data=func.load_json(input_file_full_path)["body"]

        query_output=func.query_api(server,token,module,data)
        expected_response=func.load_json(input_file_full_path)["expected_response"]

        # Compare response with expected output
        try:
            trigger=func.query_validator(query_output,expected_response)
            if trigger:
                print(f"{step} ({type}): OK",end="\n")
            else:
                checkpoints_trigger=False
                result[step]={"type":"queryCheckpoint","status":"KO","module":module,"query_body":data,"query_response":query_output,"expected_response":expected_response}
                print(f"{step} ({type}): KO",end="\n")
        except:
            trigger=False
            checkpoints_trigger=False
            print(f"{step} ({type}):KO. Please debug for more details",end="\n")

    if type not in ["queryCheckpoint"]:
        if resp.status_code in [200,201,204]:
            print(f"{step} ({type}): OK",end="\n")
        else:
            result[step]={"type":type,"status":"KO","url":url,"body":body,"response":resp.json()}
            print(f"{step} ({type}): KO",end="\n")

# print results
func.print_logs(result,log_full_path)

if checkpoints_trigger:
    print(f'all tests succeeded. log file available here: {log_full_path}')
else:
    print(f'Some tests failed. log file available here:{log_full_path}')