## Purpose

This template enables to create Python scripts for automatizing APIs calls, which can be used for the following purposes :
- bug reproducing
- bug fix testing
- non-regression testing
- new feature testing
- acceptance criterias specifications

### Checkpoints

A checkpoint enables to test at any point within the script if the current data in the database is what is expected. This template enables to perform 2 differents sort of tests:

1. API technical test, which are validated based on the response code. These tests are automatically performed on every API call performed through this script, and do not need to be specified by the user.

2. API Functional tests, which are validated by comparing the output of a query or calculatorRequest API call against the expected output. For this type of tests, the user must define the API to be tested and the expected output.

Please refer to the operational process chapter for more details.

## Operational Process

1. Clone/pull this GIT repository

2. Copy this whole folder, paste it in the folder `tickets` and rename it with the correspoinding ticket number

3. Define the global parameters (server, module etc.) in the file main.py

4. Define the steps to be performed (or get rid off the ones you dont need) in the file main.py:
- creating a datamodel through autocube
- ingesting in a dictionary (coming soon)
- ingesting in a ziptable (coming soon)
- opening a transaction
- ingesting in a versioned facttable by blockId or by blocks
- soft deleting records in a versioned facttable
- closing a transaction and performing a merge or a rebase at transaction close
- performing a merge or a rebase
- performing a query
- performing a diff (coming soon)
- performing a calculator request

5. Create the required input files in the folder data
- define autocube's body (json file)
- define data to be ingested (csv file)

6. Create checkpoints
- define checkpoints to be performed in the steps in main.py
- for each checkpoint, define in the folder `checkpoints` the body to be sent and the expected result for validating the checkpoint

## Logs
The file logs.log contains:
- all details regarding the environment on which the script has been run
- all details for each test which has failed
