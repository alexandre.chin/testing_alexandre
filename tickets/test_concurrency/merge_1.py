import sys
import os
import json
import requests
import time
from datetime import datetime

server="http://review_master.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"


# ping server stacking 
resp=requests.request(method='get',url= f"{server}/ping")

# print(f"Rafal version = {resp.json()['version']}")
# print(f"Rafal DB version = {resp.json()['dbVersion']}")

# print (resp)

# getting token
token=requests.post(f"{server}/login",json={"identifier":user,"password":password}).json()[ 'token']

#print(f'token: {token}')
headers={}
headers={'Authorization': token}
headers['Content-Type']='json'

date ='2021-05-07'
sourceBranch='branch_1'
destinationBranch='branch_2'

json_body= '''{"comment": "merging branch_1 into branch_2", 
"conflictManagement": "KeepDestinationBranch",
"concurrencyManagement":"Recursive",
"moveBranch": "Destination"}'''



#{
#"conflictsManagement" : "keepSourceBranch",
#"concurrencyManagement" : "Recursive",
#"moveBranch": "Destination",
#"comment": "comment"
#}




print (f"Begin of merge of branch 1 into branch 2 : {datetime.now()}")
body=json.loads(json_body, strict=False)
start =time.time()
url=f"{server}/tables/testing_Arrays_trade_FactTable/merge/{date}/from/{sourceBranch}/into/{destinationBranch}"
resp=requests.request(method='post',url=url,json=body, headers=headers)
end=time.time()
print (f"End of merge of branch 1 into branch 2 : {datetime.now()}")
try:
    response=resp.json()
except:
    response=resp.text

result={}
result[f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':body, 'response':response}

print(f"Result body of merge request : {result}")
#print(end-start)


