import os
import time
import func
import pandas as pd
import json

########################### GLOBAL PARAMETERS ##########################

# server = "http://review_release-5-13-x.opensee.ninja:8080"
server = "http://review_master.opensee.ninja:8080"
# server = "http://devtest-rhel.opensee.ninja:8080"
# server = "http://devtest.opensee.ninja:8080"
# server = "http://review_feature-dict-ingestion-changes.opensee.ninja:8080"
#server="http://review_1659-1-15-wrong-behavior-to-many-line-facet.opensee.ninja:8080"

user = "admin"
password = "Welcome?"

module="testing_Arrays"
table="testing_Arrays_trade_FactTable"
dict="testing_Arrays_csa_Dictionary"
ziptable="testing_Arrays_riskfactors_ZipTable"

script_folder=os.path.dirname(os.path.realpath(__file__))

result={}
token=None

# Test description 
# ingestion on two different branchs branch_1 and branch_2 1 million records (in order to have a merge with a lot of conflicts -> takes time )
# launching in paralell a merge and a adjustments 



steps={
############# PING AND LOGIN #############
#ping
'ping':{'type':'ping','method':'get','route_parameters':{'server':server},'body':{}},
#login
'login_admin':{'type':'login','method':'post','route_parameters':{'server':server},'body':{"identifier": user,"password": password}},

############# USERGROUPS AND POLICIES #############
# #getPolicies
# "getPolicies":{'type':'getPolicies','method':'get','route_parameters':{'server':server,'userGroup':'Admin'},'body':None},
# #addPolicy
# "addPolicy":{'type':'addPolicy','method':'put','route_parameters':{'server':server,'userGroup':'Admin'},'body':{'id': '9de78f13-bc16-4bbb-abcd-8b5db16c4ffc', 'actions': ['canCreateValidationRequest'], "resource": "validationrequest:WF_template"}},

############# DM #############

"delete_dm":{'type':'deleteDM','method':'delete','route_parameters':{'server':server,'module':module},'body':{"hardDelete": True}},
"validate_dm":{'type':'validateDM','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/dm/rdm_testing_arrays_facet.json'},
"create_dm":{'type':'createDM','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/dm/rdm_testing_arrays_facet.json'},

#ingestion settings
"get_ingestionSettings_0":{'type':'ingestionSettings','method':'get','route_parameters':{'server':server,'module':module,'table':table},'body':{}},
"post_ingestionSettings":{'type':'ingestionSettings','method':'post','route_parameters':{'server':server,'module':module,'table':table},'body':f'{script_folder}/source/json_bodies/testing_Arrays_trade_FactTable_ingestion_settings.json'},
"get_ingestionSettings":{'type':'ingestionSettings','method':'get','route_parameters':{'server':server,'module':module,'table':table},'body':{}},

############# INGESTION #############

#ingest initial 1
"open_initial_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'branch_1','comment':'initial_1'},'body':None},
"ingest_initial_1":{'type':'ingest','transaction':'open_initial_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/branch_1.csv'},
"close_initial_1":{'type':'closeTransaction','method':'post','transaction':'open_initial_1','route_parameters':{'server':server,'table':table,'destinationBranch':'branch_1'},'body':{"closingMode": {"Merge": {"force":True}}}},

#ingest initial_2
"open_initial_2":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'branch_2','comment':'initial_2'},'body':None},
"ingest_initial_2":{'type':'ingest','transaction':'open_initial_2','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/branch_2.csv'},
"close_initial_2":{'type':'closeTransaction','method':'post','transaction':'open_initial_2','route_parameters':{'server':server,'table':table,'destinationBranch':'branch_2'},'body':{"closingMode": {"Merge": {"force":False}}}},


#concurency test -> this step launch in parallell merge_1.py and adj_branch_2.py  
"parallelism":{'type':"parallelism"},

# query checkpoint
"query_branch_2":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial.json'},
# ############# MERGE/REBASE #############

# #createWf
# "createWfTemplate":{'type':'createWfTemplate','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/json_bodies/wf_template.json'},
# #triggerWf
# "triggerWf":{'type':'triggerWf','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/json_bodies/triggerWf.json'},
# #login
# 'login_guillaume':{'type':'login','method':'post','route_parameters':{'server':server},'body':{"identifier": "guillaume","password": password}},
# #approveWfState
# "approveWfState":{'type':'changeWfStateStatus','method':'post','route_parameters':{'server':server,'state':'state1','action':'accept'},'body':'validated by me','overload':[{'step':'triggerWf','route_parameters':{'wfId':'id'}},{'step':'wfStatus','route_parameters':{'step':'current.name'}}]},
# #mergeWF
# "mergeWf_adj_1_in_official":{'type':'mergeWf','method':'post','route_parameters':{'server':server},'body':None,'overload':[{'step':'triggerWf','route_parameters':{'wfId':'id'}}]},
# #wfStatus
# "wfStatus":{'type':'wfStatus','method':'get','route_parameters':{'server':server,'wfId':'xxx'},'body':None,'overload':[{'step':'triggerWf','route_parameters':{'wfId':'id'}}]},

# ############ CHECKPOINTS ###########

# #login
# "login_admin_bis":{'type':'login','method':'post','route_parameters':{'server':server},'body':{"identifier": user,"password": password}},
# #query checkpoint
# "query_official_1":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_official_1.json'},
# #merge
# "merge_adj_2_in_official":{'type':'merge','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'adj_2','destinationBranch':'official'},'body':{"comment": "merging adj_2 into official", "conflictManagement": "KeepSourceBranch", "concurrencyManagement":"None","moveBranch": "Destination"}},

# # query checkpoint
# "query_official_2":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_official_2.json'},

}

start = time.time()
print(f"Tests on going...")

all_tests_ok=True
script_folder=os.path.dirname(os.path.realpath(__file__))

full_log_path=f"{script_folder}/full_logs.log"
error_log_path=f"{script_folder}/error_logs.log"

# clear log file
with open(full_log_path, "w") as log_file:
    pass
with open(error_log_path, "w") as log_file:
    pass

for step in steps:
    # time.sleep(2)
    if step=="close_initial_1":
        print("xxx")
    if steps[step]['type'] in ['ingest','softDelete','closeTransaction']:
        #add asof and transaction_id to route parameters
        steps[step]['route_parameters']['transaction_id']=result[steps[step]['transaction']]['response']['response']
        steps[step]['route_parameters']['asof']=result[steps[step]['transaction']]['route_parameters']['asof']
        time.sleep(1)
    if step=='parallelism':
        print('Launching the merge and adjustements in concurrency')
        exec(open("testing_alexandre/tickets/testing_concurrency_ter/test_threading.py").read())
        time.sleep(20) 
        continue  
    
    # overload
    result=func.overload(steps[step],result)

    # api call
    temp=func.api_call(token,**steps[step])
    result[step]=func.store_api_call(step,temp)

    if steps[step]['type']=='login':
        token=result[step]['response']['response']['token']

###### Print logs #######
func.print_logs(result,full_log_path)

result_errors={}

for step in result:
    if result[step]['response']['status_code'] in [200,201,203,204]:
        if result[step]['type'] in ['ping','login']:
            result_errors[step]=result[step]
        elif 'Checkpoint' in result[step]['type']:
            if result[step]['checkpoint_status']!='OK':
                result_errors[step]=result[step]
                all_tests_ok=False
    else:
        result_errors[step]=result[step]
        all_tests_ok=False

print(f"Tests completed in {time.time()-start:.2f} sec. /n")

if all_tests_ok:
    print(f'all tests succeeded. full logs file available here: {full_log_path}')
else:
    func.print_logs(result_errors,error_log_path)
    print(f'Some tests failed. full logs and error logs file available here:{script_folder}')
