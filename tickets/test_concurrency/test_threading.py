import threading 
import time

def merge_1():
    #time.sleep(1)
    exec(open("testing_alexandre/tickets/testing_concurrency_ter/merge_1.py").read())

def private_2():
    time.sleep(1)
    exec(open("testing_alexandre/tickets/testing_concurrency_ter/adj_branch_2.py").read())       

if __name__ == "__main__": 
    
    t1 = threading.Thread(target=merge_1) 
    t2 = threading.Thread(target=private_2)
    t2.start()     
    t1.start()     

