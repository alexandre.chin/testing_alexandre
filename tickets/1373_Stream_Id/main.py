import os
import time
import func
import pandas as pd
import json

########################### GLOBAL PARAMETERS ##########################
#server = "http://review_feature-expunge-stream-id-rollback-close.opensee.ninja:8080"
#server= "http://devtest-rhel.opensee.ninja:8080"
#server="http://review_expungeincaseoferror-facetandstreamid.opensee.ninja:8080"
#server="http://ui-test-dev.opensee.ninja:8080"
#server="http://review_1405-expunge-in-case-of-error-v2.opensee.ninja:8080"
#server = "http://review_bugfix-review-double-merge-algorithm.opensee.ninja:8080"
server = "http://review_master.opensee.ninja:8080"
#server="http://review_1659-1-15-wrong-behavior-to-many-line-facet.opensee.ninja:8080"
# server = "http://devtest.opensee.ninja:8080"
#server="http://review_release-5-12-x.opensee.ninja"
user = "admin"
password = "Welcome?"

module = "module_alexandre"
table= "Facttable"
dict ="Dictionary"
ziptable="Ziptable"

script_folder=os.path.dirname(os.path.realpath(__file__))

result={}
token=None

############# Test description  #############
# open transaction 
# ingestion 1 -> trade 1  -> OK 
# ingestion 2 -> trade 2  -> OK
# ingestion 3 -> trade 3  -> OK
# ingestion 4 -> 2 million lines with corrupted data (last line) -> KO
# Block size is 1 approximativley 1 million 
# When ingestion 4 fails (at the end of 2 million lines) there is a block already ingested on CHK -> needs to be expunge
# At Close transaction expunge of the inserted line of ingestion 4 with stream id -> u can check on CHK 
# Query the branch -> only the 3 trades are here 


steps={
############# PING AND LOGIN #############
#ping
'ping':{'type':'ping','method':'get','route_parameters':{'server':server},'body':{}},
#login
'login_admin':{'type':'login','method':'post','route_parameters':{'server':server},'body':{"identifier": user,"password": password}},

############# DM #############
#autocube
"delete_dm":{'type':'autocube','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/dm/autocube_delete.json'},
"create_dm":{'type':'autocube','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/dm/autocube_input.json'},

#ingestion settings
"get_ingestionSettings":{'type':'ingestionSettings','method':'get','route_parameters':{'server':server,'module':module,'table':table},'body':{}},
"post_ingestionSettings":{'type':'ingestionSettings','method':'post','route_parameters':{'server':server,'module':module,'table':table},'body':{},'overload':[{'step':'get_ingestionSettings','body':{'insertMethod':'Linear'}}]},

############# INGESTION #############
#open
"open_initial_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-08','sourceBranch':'initial','comment':'initial_1'},'body':None},

#ingest_initial_1
"ingest_initial_1":{'type':'ingest','transaction':'open_initial_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-08'},'body':f'{script_folder}/source/data/initial_1.csv'},

#ingest_initial_2
"ingest_initial_2":{'type':'ingest','transaction':'open_initial_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-08'},'body':f'{script_folder}/source/data/initial_2.csv'},

#ingest_initial_3
"ingest_initial_3":{'type':'ingest','transaction':'open_initial_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-08'},'body':f'{script_folder}/source/data/initial_3.csv'},

#ingest_initial_4
"ingest_initial_4":{'type':'ingest','transaction':'open_initial_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-08'},'body':f'{script_folder}/source/data/initial_4.csv'},

#Close
"close_initial_1":{'type':'closeTransaction','method':'post','transaction':'open_initial_1','route_parameters':{'server':server,'table':table,'destinationBranch':'initial'},'body':{"closingMode": {"Merge": {"force":True}}}},


# query initial checkpoint
"query_initial_3":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_initial_3.json'},


############# WF #############
# #createWf
# "createWfTemplate":{'type':'createWfTemplate','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/json_bodies/wf_template.json'},
# #getWfsTemplates
# "getWfsTemplates":{'type':'getWfsTemplates','method':'get','route_parameters':{'server':server},'body':None},
# #triggerWf
# "triggerWf":{'type':'triggerWf','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/json_bodies/triggerWf.json'},
# #wfStatus
# "wfStatus":{'type':'wfStatus','method':'get','route_parameters':{'server':server,'wfId':'xxx'},'body':None},
# #wfsStatus
# "wfsStatus":{'type':'wfsStatus','method':'get','route_parameters':{'server':server},'body':None},
# #approveWfState
# "approveWfState":{'type':'changeWfStateStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','state_name':'yyy','action':'accpet'},'body':f'validated by me'},
# #rejectWfState
# "rejectWfState":{'type':'changeWfStateStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','state_name':'yyy','action':'reject'},'body':f'validated by me'},
# #retyWfState
# "retyWfState":{'type':'changeWfStateStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','state_name':'yyy','action':'retry'},'body':f'validated by me'},
# #rejectWfState
# "overridetWfState":{'type':'changeWfStateStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','state_name':'yyy','action':'override'},'body':f'validated by me'},
# #closetWfStatus
# "closetWfStatus":{'type':'changeWfStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','action':'close'},'body':None},
# #closetWfStatus
# "reopentWfStatus":{'type':'changeWfStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','action':'reopen'},'body':None},
# #mergeWF
# "mergeWF":{'type':'mergeWF','method':'post','route_parameters':{'server':server, 'wfId':'xxx'},'body':None},

############# MERGE/REBASE #############

# #createWf
# "createWfTemplate":{'type':'createWfTemplate','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/json_bodies/wf_template.json'},
# #triggerWf
# "triggerWf":{'type':'triggerWf','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/json_bodies/triggerWf.json'},
# #login
# 'login_guillaume':{'type':'login','method':'post','route_parameters':{'server':server},'body':{"identifier": "guillaume","password": password}},
# #approveWfState
# "approveWfState":{'type':'changeWfStateStatus','method':'post','route_parameters':{'server':server,'state':'state1','action':'accept'},'body':'validated by me','overload':[{'step':'triggerWf','route_parameters':{'wfId':'id'}},{'step':'wfStatus','route_parameters':{'step':'current.name'}}]},
# #mergeWF
# "mergeWf_adj_1_in_official":{'type':'mergeWf','method':'post','route_parameters':{'server':server},'body':None,'overload':[{'step':'triggerWf','route_parameters':{'wfId':'id'}}]},
# #wfStatus
# "wfStatus":{'type':'wfStatus','method':'get','route_parameters':{'server':server,'wfId':'xxx'},'body':None,'overload':[{'step':'triggerWf','route_parameters':{'wfId':'id'}}]},

# ############ CHECKPOINTS ###########

# #login
# "login_admin_bis":{'type':'login','method':'post','route_parameters':{'server':server},'body':{"identifier": user,"password": password}},
# #query checkpoint
# "query_official_1":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_official_1.json'},
# #merge
# "merge_adj_2_in_official":{'type':'merge','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'adj_2','destinationBranch':'official'},'body':{"comment": "merging adj_2 into official", "conflictManagement": "KeepSourceBranch", "concurrencyManagement":"None","moveBranch": "Destination"}},

# # query checkpoint
# "query_official_2":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_official_2.json'},

# "details_1":{'type':'detailsCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_official_2.json'},

# #dict wrapper
# "dict_wrapper":{'type':'dictIngest','method':'post','route_parameters':{'server':server,'dict':dict,'asof':'2021-05-07','sourceBranch':'official','destinationBranch':'official','comment':'dict wrapper'},'body':f'{script_folder}/source/data/dict_wrapper.csv'},

}

start = time.time()
print(f"Tests on going...")

all_tests_ok=True
script_folder=os.path.dirname(os.path.realpath(__file__))

full_log_path=f"{script_folder}/full_logs.log"
error_log_path=f"{script_folder}/error_logs.log"

# clear log file
with open(full_log_path, "w") as log_file:
    pass
with open(error_log_path, "w") as log_file:
    pass

for step in steps:
    #time.sleep(1)
    if step=="close_initial_1":
        print("test")
    if steps[step]['type'] in ['ingest','closeTransaction']:
        #add asof and transaction_id to route parameters
        steps[step]['route_parameters']['transaction_id']=result[steps[step]['transaction']]['response']['response']
        steps[step]['route_parameters']['asof']=result[steps[step]['transaction']]['route_parameters']['asof']
        time.sleep(1)

    # overload
    result=func.overload(steps[step],result)

    # api call
    temp=func.api_call(token,**steps[step])
    result[step]=func.store_api_call(step,temp)

    if steps[step]['type']=='login':
        token=result[step]['response']['response']['token']

###### Print logs #######
func.print_logs(result,full_log_path)

result_errors={}

for step in result:
    if result[step]['response']['status_code'] in [200,201,203,204]:
        if result[step]['type'] in ['ping','login']:
            result_errors[step]=result[step]
        elif 'Checkpoint' in result[step]['type']:
            if result[step]['checkpoint_status']!='OK':
                result_errors[step]=result[step]
                all_tests_ok=False
    else:
        result_errors[step]=result[step]
        all_tests_ok=False

func.print_logs(result_errors,error_log_path)

print(f"Tests completed in {time.time()-start:.2f} sec. /n")

if all_tests_ok:
    print(f'all tests succeeded. full logs file available here: {full_log_path}')
else:
    print(f'Some tests failed. full logs adn error logs file available here:{script_folder}')
