import sys
import os
import json
import pandas as pd
import numpy as np
import requests
import time

number_of_records=2000000

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))

df = pd.DataFrame({
    "asOf":"2021-05-07",
    "StaticKey1":['trade_'+str(i+4) for i in range(0,number_of_records)],
    "StaticKey2":['Cpty_'+str(i+4) for i in range(0,number_of_records)],
    "Dim1":['book_'+str(i+4)for i in range(0,number_of_records)],
    "Dim2":['EUR'for i in range(0,number_of_records)],    
    "fact"  : np.random.normal(1000.0, 1000.0, size=number_of_records)
                     })

df.to_csv(f"{script_folder}/initial_4.csv",index=False,header=False)