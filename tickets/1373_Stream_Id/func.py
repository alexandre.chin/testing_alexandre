import sys
import os
import re
import json
import requests
import asyncio
import time
import random
from functools import reduce
import pandas as pd

def load_json(json_full_path):
    '''
    reads a json file as a dictionary
    '''
    with open(json_full_path, "r") as f:
        data = json.load(f)
    return data

def load_csv(csv_full_path):
    '''
    reads a csv file as a dictionary
    '''
    with open(csv_full_path, 'rb') as data:
        pass
    return data

def convert_to_dict(obj):
    # format response
    if type(obj) is str:
        if "\n" in obj:
            # add comma after each json string and convert them to list
            cleaned_str = obj.replace("\n", ",\n").split(",\n")

            # remove empty items from the list
            result = [s for s in cleaned_str if s]

            # convert to json object
            result=json.loads(json.dumps(result))
            result=[json.loads(element) for element in result]
        else:
            result=obj
    
    elif type(obj) is dict:
        result=obj

    return result

def deep_get(dictionary, keys, default=None):
    return reduce(lambda d, key: d.get(key, default) if isinstance(d, dict) else default, keys.split("."), dictionary)


def print_logs(obj,log_full_path):

    '''
    obj=object to print (dictionnary,list etc.)
    log_full_path= full path to log file
    '''
    with open(log_full_path, "a") as log_file:
        log_file.write(json.dumps(obj,sort_keys=False, indent=4))

def query_validator(query_output,expected_output):
    """
    Checks if a query performed outputs the expected result
    """

    trigger=True

    if len(expected_output)==len(query_output):
        if len(expected_output):
            for record in expected_output:
                if record in query_output:
                    pass
                else:
                    trigger=False
                    break
        else:
            if query_output!=expected_output:
                trigger=False

    else:
        trigger=False
    
    return trigger

def overload(source_dict, target_dict):
    if 'overload' in source_dict:
        for item in source_dict['overload']:
            if 'body' in item:
                source_dict['body']=target_dict[item['step']]['response']['response']
                for key in item['body']:
                    source_dict['body'][key]=item['body'][key]
            elif 'route_parameters' in item:
                for key in item['route_parameters']:
                    source_dict['route_parameters'][key]=deep_get(target_dict[item['step']]['response']['response'],item['route_parameters'][key])
    return target_dict

class api_call:
    def __init__(self,token,**kwargs):
        self.token=token
        self.type=kwargs['type']
        self.method=kwargs['method']
        self.route_parameters=kwargs['route_parameters']
        self.body=kwargs['body']

    def headers(self):
        if self.token:
            if self.body_format()=='json':
                result={'Authorization': self.token,'Content-Type':'application/json'}
            elif self.body_format() is None:
                result={'Authorization': self.token,'Content-Type':'application/json'}
            elif self.body_format()=='str':
                result={'Authorization': self.token}
            else:
                result={'Authorization': self.token,'Content-Type':'text/csv'}
        else:
            result=None
        return result

    def body_format(self):
        if type(self.body) is dict:
            if self.body:
                result='json'
            else:
                result=None
        elif type(self.body) is str:
            if '.json' in self.body:
               result='json'
            elif '.csv' in self.body:
                result='csv'
            else:
                result='str'
        else:
            result=None
        return result

    def route(self):        
        if self.type=='ping':
            result=f"{self.route_parameters['server']}/ping"
        elif self.type=='login':
            result=f"{self.route_parameters['server']}/login"
        ############# USERGROUPS AND POLICIES #############
        elif self.type=='getPolicies':
            result=f"{self.route_parameters['server']}/group/{self.route_parameters['userGroup']}"
        elif self.type=='addPolicy':
            result=f"{self.route_parameters['server']}/group/{self.route_parameters['userGroup']}/policy"

        ############# DM #############
        elif self.type=='autocube':
            result=f"{self.route_parameters['server']}/autocube/{self.route_parameters['module']}"
        elif self.type=='ingestionSettings':
            result=f"{self.route_parameters['server']}/module/{self.route_parameters['module']}/tables/{self.route_parameters['table']}/ingestion_settings"

        ############# Ingestion #############
        elif self.type=='openTransaction':
            result=f"{self.route_parameters['server']}/tables/{self.route_parameters['table']}/transaction/{self.route_parameters['asof']}/from/{self.route_parameters['sourceBranch']}/{self.route_parameters['comment']}"
        elif self.type=='ingest':
            if "blockId" in self.route_parameters:
                blockId=self.route_parameters['blockId']
                result=f"{self.route_parameters['server']}/tables/{self.route_parameters['table']}/commits/{self.route_parameters['transaction_id']}/{self.route_parameters['asof']}/{blockId}"
            elif "blocks" in self.route_parameters:
                blocks=self.route_parameters['blocks']
                result=f"{self.route_parameters['server']}/beta/transactions/{self.route_parameters['transaction_id']}/blocks/{blocks}"
            else:
                result=f"{self.route_parameters['server']}/tables/{self.route_parameters['table']}/commits/{self.route_parameters['transaction_id']}/{self.route_parameters['asof']}"
        elif self.type=='softDelete':
            result = f"{server}/tables/{self.route_parameters['table']}/commits/{self.route_parameters['transaction_id']}/{self.route_parameters['asof']}/delete"
        elif self.type=='dictIngest':
            result=f"{self.route_parameters['server']}/dictionaries/{self.route_parameters['dict']}/{self.route_parameters['asof']}/from/{self.route_parameters['sourceBranch']}/to/{self.route_parameters['destinationBranch']}/{self.route_parameters['comment']}"        
        elif self.type=='softDelete':
            result=f"{self.route_parameters['server']}/tables/{self.route_parameters['table']}/commits/{self.route_parameters['transaction_id']}/{self.route_parameters['asof']}/delete"
        elif self.type=='closeTransaction':
             result=f"{self.route_parameters['server']}/tables/{self.route_parameters['table']}/closeTransaction/{self.route_parameters['transaction_id']}/{self.route_parameters['asof']}/to/{self.route_parameters['destinationBranch']}"
        elif self.type=='merge':
            result= f"{self.route_parameters['server']}/tables/{self.route_parameters['table']}/merge/{self.route_parameters['asof']}/from/{self.route_parameters['sourceBranch']}/into/{self.route_parameters['destinationBranch']}"
        elif self.type=='rebase':
            result= f"{self.route_parameters['server']}/tables/{self.route_parameters['table']}/rebase/{self.route_parameters['asof']}/from/{self.route_parameters['sourceBranch']}/onto/{self.route_parameters['destinationBranch']}"
        
        ############# WF #############
        elif self.type=='createWfTemplate':
            result= f"{self.route_parameters['server']}/validation/flow"
        elif self.type=='triggerWf':
            result= f"{self.route_parameters['server']}/validation/request"
        elif self.type=='wfStatus':
            result= f"{self.route_parameters['server']}/validation/request/{self.route_parameters['wfId']}"
        elif self.type=='changeWfStateStatus':
            result= f"{self.route_parameters['server']}/validation/request/{self.route_parameters['wfId']}/state/{self.route_parameters['state']}/{self.route_parameters['action']}"
        elif self.type=='mergeWf':
            result= f"{self.route_parameters['server']}/validation/merge/{self.route_parameters['wfId']}"

        ############# Checks points #############
        elif self.type=='detailsCheckpoint':
            result= f"{self.route_parameters['server']}/module/{self.route_parameters['module']}/details"        
        elif self.type=='queryCheckpoint':
            result= f"{self.route_parameters['server']}/module/{self.route_parameters['module']}/query"
        elif self.type=='diffCheckpoint':
            result= f"{self.route_parameters['server']}/beta/module/{self.route_parameters['module']}/diff"
        elif self.type=='calculatorRequestCheckpoint':
            result= f"{self.route_parameters['server']}/module/{self.route_parameters['module']}/calculate"
        
        else:
            result='type unknown'

        return result

    def decoded_body(self):        
        if self.body is None:
            result=None
        elif type(self.body) is dict:
            if self.body:
                if "Checkpoint" in self.type:
                    result=self.body['body']
                else:
                    result=self.body
            else:
                result=None
        elif type(self.body) is str:
            if self.body_format()=='json':
                result=load_json(self.body)
                if 'body' in result:
                    result=result['body']
            elif self.body_format()=='csv':
                result=self.body
            elif self.body_format()=='str':
                result=self.body
            
        return result

    def expected_response(self):
        result={}
        if "Checkpoint" in self.type:
            if self.body_format()=='json':
                result=load_json(self.body)
                if 'expected_response' in result:
                    result=result['expected_response']

        return result

    def call(self):
        if self.body_format() is None:
            resp=requests.request(method=self.method,url=self.route(),headers=self.headers())

        elif self.body_format()=='json':
            if self.type=='login':
                resp=requests.request(method=self.method,url= self.route(),json=self.decoded_body(),)
            else:
                resp=requests.request(method=self.method,url= self.route(),json=self.decoded_body(),headers=self.headers())
        elif self.body_format()=='csv':
            with open(self.body, 'rb') as data:
                resp=requests.request(method=self.method,url= self.route(),data=data,headers=self.headers())
        elif self.body_format()=='str':
            resp=requests.request(method=self.method,url= self.route(),json=self.decoded_body(),headers=self.headers())
        try:
            response=resp.json()
        except:
            try:
                response=convert_to_dict(resp.text)
            except:
                response=resp.text
        
        result={'status_code':resp.status_code,'response':response}
        return result

def store_api_call(step_name,api_call_obj):    
    result={}
    result['type']=api_call_obj.type
    result['method']=api_call_obj.method
    result['route_parameters']=api_call_obj.route_parameters
    result['route']=api_call_obj.route()

    #body
    if api_call_obj.body_format()=='json':
        if "Checkpoint" not in result['type']:
            result['body']=api_call_obj.body
        else:
            result['body']=api_call_obj.decoded_body()
            result['expected_response']=api_call_obj.expected_response()
    elif api_call_obj.body_format()=='csv':
        result['body']=api_call_obj.body

    result['response']=api_call_obj.call()

    if "Checkpoint" not in result['type']:
        if result['response']['status_code'] in [200,201,204]:
            print(f"{step_name} ({result['type']}): OK",end="\n")
        else:
            print(f"{step_name} ({result['type']}): KO",end="\n")
    else:
# Compare response with expected output
        try:
            trigger=query_validator(result['response']['response'],result['expected_response'])
            if trigger:
                result['checkpoint_status']="OK"
                print(f"{step_name} ({result['type']}): OK",end="\n")
            else:
                result['checkpoint_status']="KO"
                print(f"{step_name} ({result['type']}): KO",end="\n")
        except:
            trigger=False
            result['checkpoint_status']="KO"
            print(f"{step_name} ({result['type']}): KO. Please debug for more details",end="\n")

    

    return result

script_folder=os.path.dirname(os.path.realpath(__file__))

server="http://review_master.opensee.ninja:8080"
user = "admin"
password = "Welcome?"
module = "module_alexandre"
table= "Facttable"

result={}

steps={
#ping
'ping':{'type':'ping','method':'get','route_parameters':{'server':server},'body':{}},
#login
'login':{'type':'login','method':'post','route_parameters':{'server':server},'body':{"identifier": user,"password": password}},

############# DM #############
#autocube
"test_autocube":{'type':'autocube','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/dm/autocube_input.json'},
#ingestion settings
"test_get_ingestionSettings":{'type':'ingestionSettings','method':'get','route_parameters':{'server':server,'module':module,'table':table},'body':{}},
"test_post_ingestionSettings":{'type':'ingestionSettings','method':'post','route_parameters':{'server':server,'module':module,'table':table},'body':{},'overload':[{'step':'test_get_ingestionSettings','body':{'insertMethod':'Linear'}}]},

############# INGESTION #############
#openTransaction
"openTransaction_initial":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'initial'},'body':None},
#ingest by blocks
"ingest_initial":{'type':'ingest','transaction':'openTransaction_initial','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/initial_1.csv'},
#closeTransaction
"closeTransaction_initial":{'type':'closeTransaction','method':'post','transaction':'openTransaction_initial','route_parameters':{'server':server,'table':table,'destinationBranch':'initial'},'body':{"closingMode": {"Merge": {"force":False}}}},


#openTransaction
"openTransaction_official":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'initial'},'body':None},
#closeTransaction
"closeTransaction_official":{'type':'closeTransaction','method':'post','transaction':'openTransaction_official','route_parameters':{'server':server,'table':table,'destinationBranch':'official'},'body':{"closingMode": {"Merge": {"force":False}}}},

#merge
"merge_initial_in_official":{'type':'merge','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','destinationBranch':'official'},'body':{"comment": "merging initial into official", "conflictManagement": "KeepSourceBranch", "concurrencyManagement":"None","moveBranch": "Destination"}},


#openTransaction
"openTransaction_adj_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'official','comment':'adj_1'},'body':None},
#ingest by blocks
"ingest_adj_1":{'type':'ingest','transaction':'openTransaction_adj_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07',"blocks":"StaticKey1/StaticKey2"},'body':f'{script_folder}/source/data/adj_1.csv'},
#closeTransaction
"closeTransaction_adj_1":{'type':'closeTransaction','method':'post','transaction':'openTransaction_adj_1','route_parameters':{'server':server,'table':table,'destinationBranch':'adj_1'},'body':{"closingMode": {"Merge": {"force":False}}}},


#openTransaction
"openTransaction_adj_2":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'official','comment':'adj_2'},'body':None},
#ingest by blocks
"ingest_adj_2":{'type':'ingest','transaction':'openTransaction_adj_2','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07',"blocks":"StaticKey1/StaticKey2"},'body':f'{script_folder}/source/data/adj_2.csv'},
#closeTransaction
"closeTransaction_adj_2":{'type':'closeTransaction','method':'post','transaction':'openTransaction_adj_2','route_parameters':{'server':server,'table':table,'destinationBranch':'adj_2'},'body':{"closingMode": {"Merge": {"force":False}}}},

############# WF #############
# #createWf
# "test_createWf":{'type':'createWf','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/json_bodies/wf_template.json'},
# #getWfsTemplates
# "test_getWfsTemplates":{'type':'getWfsTemplates','method':'get','route_parameters':{'server':server},'body':None},
# #triggerWf
# "test_triggerWf":{'type':'triggerWf','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/json_bodies/triggerWf.json'},
# #wfStatus
# "test_wfsStatus":{'type':'wfStatus','method':'get','route_parameters':{'server':server,'wfId':'xxx'},'body':None},
# #wfsStatus
# "test_wfsStatus":{'type':'wfsStatus','method':'get','route_parameters':{'server':server},'body':None},
# #approveWfState
# "test_approveWfState":{'type':'changeWfStateStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','state_name':'yyy','action':'approve'},'body':f'validated by me'},
# #rejectWfState
# "test_rejectWfState":{'type':'changeWfStateStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','state_name':'yyy','action':'reject'},'body':f'validated by me'},
# #retyWfState
# "test_retyWfState":{'type':'changeWfStateStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','state_name':'yyy','action':'retry'},'body':f'validated by me'},
# #rejectWfState
# "test_overridetWfState":{'type':'changeWfStateStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','state_name':'yyy','action':'overrride'},'body':f'validated by me'},
# #closetWfStatus
# "test_closetWfStatus":{'type':'changeWfStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','action':'close'},'body':None},
# #closetWfStatus
# "test_reopentWfStatus":{'type':'changeWfStatus','method':'post','route_parameters':{'server':server, 'wfId':'xxx','action':'reopen'},'body':None},
# #mergeWF
# "test_mergeWF":{'type':'mergeWF','method':'post','route_parameters':{'server':server, 'wfId':'xxx'},'body':None},

############# MERGE/REBASE #############
#merge
"merge_adj_1_in_official":{'type':'merge','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'adj_1','destinationBranch':'official'},'body':{"comment": "merging adj_1 into official", "conflictManagement": "KeepSourceBranch", "concurrencyManagement":"None","moveBranch": "Destination"}},
# query checkpoint
"query_official_1":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_official_1.json'},

#rebase
"rebase_adj_2_in_official":{'type':'rebase','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'adj_2','destinationBranch':'official'},'body':{"comment": "rebasing adj_2 into official", "conflictManagement": "KeepSourceBranch", "concurrencyManagement":"None","moveBranch": "Destination"}},
# query checkpoint
"query_official_2":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_official_2.json'},

}

if __name__== '__main__':
    start = time.time()
    print(f"Tests on going...")

    all_tests_ok=True
    script_folder=os.path.dirname(os.path.realpath(__file__))

    full_log_path=f"{script_folder}/full_logs.log"
    error_log_path=f"{script_folder}/error_logs.log"

    # clear log file
    with open(full_log_path, "w") as log_file:
        pass

    with open(error_log_path, "w") as log_file:
        pass

    result={}
    token=None

    for step in steps:
        if steps[step]['type'] in ['ingest','closeTransaction']:
            #add asof and transaction_id to route parameters
            steps[step]['route_parameters']['transaction_id']=result[steps[step]['transaction']]['response']['response']
            steps[step]['route_parameters']['asof']=result[steps[step]['transaction']]['route_parameters']['asof']
            time.sleep(1)

        # overload
        result=overload(steps[step],result)

        temp=api_call(token,**steps[step])
        result[step]=store_api_call(step,temp)

        if step=='login':
            token=result[step]['response']['response']['token']

    ###### Print logs #######
    print_logs(result,full_log_path)

    result_errors={}

    for step in result:
        if result[step]['response']['status_code'] in [200,201,203,204]:
            if result[step]['type'] in ['ping','login']:
                result_errors[step]=result[step]
            elif 'Checkpoint' in result[step]['type']:
                if result[step]['checkpoint_status']!='OK':
                    result_errors[step]=result[step]
                    all_tests_ok=False
        else:
            result_errors[step]=result[step]
            all_tests_ok=False

    print_logs(result_errors,error_log_path)

    print(f"Tests completed in {time.time()-start:.2f} sec. /n")

    if all_tests_ok:
        print(f'all tests succeeded. full logs file available here: {full_log_path}')
    else:
        print(f'Some tests failed. full logs adn error logs file available here:{script_folder}')
    
