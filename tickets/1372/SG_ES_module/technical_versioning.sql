INSERT INTO TABLE TechnicalTableSetting VALUES ('ES4_FumStock','versioned','true',now(),today());
INSERT INTO TABLE TechnicalTableSetting VALUES ('ES4_FumStock','unicityKey','contextlabel,riskmetricname,dealId,tradeId,pairingkey,instrument,riskClass,riskFactor,pricingCurrency,regulatoryWindow,calculatorname,riskFactorSet,liquidityHorizon,codeApp',now(),today());

INSERT INTO TABLE TechnicalTableSetting VALUES ('ES4_FumStockFacet','versioned','true',now(),today());
INSERT INTO TABLE TechnicalTableSetting VALUES ('ES4_FumStockFacet','unicityKey','ES4_AnalyticalStructure, ES4_ProductDescription, scenarioVectorId, contextlabel, riskmetricname,  riskClass, regulatoryWindow, pricingCurrency, isreducedset, riskFactorSet, liquidityHorizon, versionid',now(),today());
