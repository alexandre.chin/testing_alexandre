DROP DICTIONARY IF EXISTS ES4_ProductDescription ON CLUSTER '{cluster_oneshard}';
DROP DICTIONARY IF EXISTS ES4_ProductDescriptionJoin ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_ProductDescriptionReceiver ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_ProductDescriptionInsert ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_ProductDescriptionJoinInsert ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.shard_ES4_ProductDescriptionData ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.shard_ES4_ProductDescriptionJoin ON CLUSTER '{cluster_oneshard}';
DROP DICTIONARY IF EXISTS ES4_AnalyticalStructure ON CLUSTER '{cluster_oneshard}';
DROP DICTIONARY IF EXISTS ES4_AnalyticalStructureJoin ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_AnalyticalStructureReceiver ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_AnalyticalStructureInsert ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_AnalyticalStructureJoinInsert ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.shard_ES4_AnalyticalStructureData ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.shard_ES4_AnalyticalStructureJoin ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_FumStock ON CLUSTER '{cluster}';
DROP TABLE IF EXISTS default.shard_ES4_FumStock ON CLUSTER '{cluster}';
DROP TABLE IF EXISTS default.shard_state_ES4_FumStockFacet ON CLUSTER '{cluster}';
DROP TABLE IF EXISTS default.state_ES4_FumStockFacet ON CLUSTER '{cluster}';
DROP TABLE IF EXISTS default.mv_ES4_FumStockFacet ON CLUSTER '{cluster}';
DROP TABLE IF EXISTS default.shard_view_finalize_ES4_FumStockFacet ON CLUSTER '{cluster}';
DROP TABLE IF EXISTS default.ES4_FumStockFacet ON CLUSTER '{cluster}';
DROP TABLE IF EXISTS default.ES4_FumStockInsert ON CLUSTER '{cluster}';
DROP TABLE IF EXISTS default.ES4_FumStockFactInsert ON CLUSTER '{cluster}';
DROP TABLE IF EXISTS default.shard_ES4_FumStockFactLabel ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.dist_ES4_FumStockFactLabel ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_FumStockFactLabelInsert ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_FumStockFactLabel ON CLUSTER '{cluster_oneshard}';
DROP DICTIONARY IF EXISTS ES4_PriceableErrorMultiContext ON CLUSTER '{cluster_oneshard}';
DROP DICTIONARY IF EXISTS ES4_PriceableErrorMultiContextJoin ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_PriceableErrorMultiContextReceiver ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_PriceableErrorMultiContextInsert ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.ES4_PriceableErrorMultiContextJoinInsert ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.shard_ES4_PriceableErrorMultiContextData ON CLUSTER '{cluster_oneshard}';
DROP TABLE IF EXISTS default.shard_ES4_PriceableErrorMultiContextJoin ON CLUSTER '{cluster_oneshard}';
