## How to reproduce issue #1372
A. Upload module by following
 1. Launch drop_tables.sql script.
 2. Launch create_tables.sql script.
 3. Launch technical_versioning.sql script.
 4. Post definition, pivot and table settings.
 5. Update mandatory filters to add the new module.

B. Ingest data
 6. Open transaction to insert data into the fact table.
 7. Post api call by choosing the file FumStock.csv in order to ingest data.
 8. Close the transaction.

### Issue while performing 
The last step on the env 5.12 :
* ENV : http://review_1362-xmla-calculator-date-format.opensee.ninja:8080/
* Rafal version = v5.12.0-psql-ssl-14-gf7a0377 
* Rafal DB version = v21.6.9.7-stable ) 

### We got 
The error message :

{ "notFinished": { "Failed": \[ 2308642070934714960 \] }, "id": "46ed9686-fe65-49df-ab36-49b051fb7bb7", "rafalError": "StreamsNotFinished", "message": "transaction cdbc1529-0174-3bd4-bf92-4f3ce499ccaa has not all streams finished, at least there is Failed: 2308642070934714960" }

### Expected
 The close transaction should be possible.