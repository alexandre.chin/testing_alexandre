import sys
import os
import json
import requests
import time
from source import func

result={}

########################### GLOBAL PARAMETERS ##########################

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
log_full_path=f"{script_folder}/logs.log"

# global parameters
server = "http://review_856-close-transaction-bug.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# parameters
module = "IngestionWrapperTest"
table= "IngestionWrapperTest"
dict ="dictionaryIdTest"
as_of= "2021-05-07"


# queries parameters
ingestion_parameters=[
#test 1
{
'request_parameters_fact':{'file_name':'ingestion_fact_test_1.csv','sourceBranch':'official','destinationBranch':'official','comment':'ingestion_fact_test_1'}
},

{
'request_parameters_fact':{'file_name':'ingestion_fact_test_2.csv','sourceBranch':'official','destinationBranch':'private','comment':'ingestion_fact_test_2'}
},

{
'request_parameters_fact':{'file_name':'ingestion_fact_test_3.csv','sourceBranch':'private','destinationBranch':'official','comment':'ingestion_fact_test_3'}
}
]

# clear log file
with open(log_full_path, "w") as log_file:
  pass

########################### LOGIN TO SERVER ##########################

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
# kwcnx = dict(headers={'Authorization': token})
kwcnx={}
kwcnx={'headers':{'Authorization': token}}

########################### CREATING MODULE WITH AUTOCUBE ##########################

kwcnx['headers']['Content-Type']='json'

url=f"{server}/autocube/{module}"

json_full_path=f"{script_folder}/source/autocube_input.json"
autocube_input_dict=func.load_json(json_full_path)

try:
  #Deletes module if it aleady exists
  autocube_input_dict['mode']='Delete'

  resp=requests.request(method='post',url=url,json=autocube_input_dict, **kwcnx)
  time.sleep(2)
finally:
  #creates module
  autocube_input_dict['mode']='Create'

  resp=requests.request(method='post',url=url,json=autocube_input_dict, **kwcnx)
  time.sleep(2)

  print(f"module {module} created")

########################### DATA INGESTION ##########################
kwcnx['headers']['Content-Type']='text/csv'
for request_parameters in ingestion_parameters:
    sourceBranch=request_parameters['request_parameters_fact']['sourceBranch']
    destinationBranch=request_parameters['request_parameters_fact']['destinationBranch']
    comment=request_parameters['request_parameters_fact']['comment']
    file_name=request_parameters['request_parameters_fact']['file_name']

    # ingest fact table
    if 'request_parameters_fact' in request_parameters.keys():

        #open transaction
        url=f"{server}/tables/{table}/transaction/{as_of}/from/{sourceBranch}/{comment}"

        resp=requests.request(method='post',url=url, **kwcnx)

        transaction_id=resp.text
        print(f"Opening transaction: {transaction_id}")
        time.sleep(2)

        #ingest fact table
        url=f"{server}/tables/{table}/commits/{transaction_id}/{as_of}"

        file_full_path=f"{script_folder}/source/data/{file_name}"

        with open(file_full_path, 'rb') as fp:
            resp=requests.request(method='post',url=url,data=fp, **kwcnx)
            time.sleep(2)

        print(f"Ingesting facts: {resp}")
        if resp.status_code not in (200,204):
            print(f"error: {resp.text}")
        else:
            pass

        #close transaction
        url=f"{server}/tables/{table}/closeTransaction/{transaction_id}/{as_of}/to/{destinationBranch}"
        body={"closingMode" : { "Merge" : { "force" : True}}}

        resp=requests.request(method='post',url=url,json=body, **kwcnx)
        print(f"Closing transaction: {resp.text}\n")

        time.sleep(2)


### CHECK BRANCHES COMMIT HIERARCHY ###
# Offical branch
url= f'{server}/tables/{table}/branches/official/{as_of}'
resp=requests.request(method='get',url=url, **kwcnx)

official_branch_hierarchy=resp.json()

# Private branch
url= f'{server}/tables/{table}/branches/private/{as_of}'
resp=requests.request(method='get',url=url, **kwcnx)

private_branch_hierarchy=resp.json()

print("test completed")