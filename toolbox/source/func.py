import sys
import os
import re
import json
import requests
import asyncio
import time
import random

def load_json(json_full_path):
    '''
    reads a json file as a dictionary
    '''
    with open(json_full_path, "r") as f:
        data = json.load(f)
    return data

def print_logs(obj,log_full_path):

    '''
    obj=object to print (dictionnary,list etc.)
    log_full_path= full path to log file
    '''
    with open(log_full_path, "a") as log_file:
        log_file.write(json.dumps(obj,sort_keys=False, indent=4))

def ping_api(server):

    result={}
    result['ping']={}

    # testing connection & server version
    url=f'{server}/ping'
    resp=requests.request(method='get',url= url)

    print(f"Rafal version = {resp.json()['version']}")
    print(f"Rafal DB version = {resp.json()['dbVersion']}")

    response=resp.json()

    result['ping'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    return result

def login_api(server,user,password):

    result={}
    result['login']={}

    # testing connection & server version
    url=f'{server}/login'
    resp=requests.request(method='post',url= url,json={"identifier": user,"password": password},)

    # response
    response=resp.json()

    result['login'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':{"identifier": user,"password": password}, 'response':response}

    return result

class connect:
    def __init__(self,server,user,password):
        self.server=server
        self.user=user
        self.password=password

    def ping(self):
        url=f'{self.server}/ping'
        resp=requests.request(method='get',url= url)
        result={}
        result['Rafal version']=resp.json()['version']
        result['Rafal DB Version']=resp.json()['dbVersion']

        return result

def async_api(f):
    def wrapped(*args, **kwargs):
        return asyncio.get_event_loop().run_in_executor(None, f, *args, **kwargs)
    return wrapped

def query_api(server,token,module,data,query_type="query",query_name=None):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    module: module name in the GET/DELETE/POST methods
    data: dictionary containing the body query parameters
    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}

    ###################### RUNS A QUERY ######################
    if query_type=="query":
        url=f"{server}/module/{module}/query"
    elif query_type=="diff":
        url=f"{server}/module/{module}/diff"

    resp=requests.request(method='post',url=url,json=data, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text
    
    # format response
    try:
        # add comma after each json string and convert them to list
        cleaned_str = response.replace("\n", ",\n").split(",\n")

        # remove empty items from the list
        response = [s for s in cleaned_str if s]

        # convert to json object
        response=json.loads(json.dumps(response))
        response=[json.loads(element) for element in response]

        time.sleep(1)
        if query_name:
            result[f'query_{query_name}']={}
            result[f'query_{query_name}'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}
        else:
            result=response
    except:
        result=response
    return result


def details_api(server,token,module,data,details_name):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    module: module name in the GET/DELETE/POST methods
    data: dictionary containing the body query parameters
    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result[f'details_{details_name}']={}

    ###################### RUNS A QUERY ######################
    url=f"{server}/module/{module}/details"

    resp=requests.request(method='post',url=url,json=data, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text
    
    # format response

    # add comma after each json string and convert them to list
    cleaned_str = response.replace("\n", ",\n").split(",\n")

    # remove empty items from the list
    response = [s for s in cleaned_str if s]

    # convert to json object
    response=json.loads(json.dumps(response))

    time.sleep(1)

    result[f'details_{details_name}'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    return result

def merge_api(server,token,table,as_of,sourceBranch,destinationBranch,body,merge_number):

    '''

    body={"comment": "merging private into official", "conflictManagement": "KeepDestinationBranch","concurrencyManagement":"None"}

    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result[f'merge_{merge_number}']={}

    ###################### MERGE ######################
    url= f'{server}/tables/{table}/merge/{as_of}/from/{sourceBranch}/into/{destinationBranch}'
    
    resp=requests.request(method='post',url=url,json=body, headers=headers)


    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    time.sleep(1)

    result[f'merge_{merge_number}'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':body, 'response':response}

    return result

def query_validator(query_output,expected_output):
    """
    Checks if a query performed outputs the expected result
    """

    trigger=True

    if len(expected_output)==len(query_output):
        if len(expected_output):
            for record in expected_output:
                if record in query_output:
                    pass
                else:
                    trigger=False
                    break
        else:
            if query_output!=expected_output:
                trigger=False

    else:
        trigger=False
    
    return trigger