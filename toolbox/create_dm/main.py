
import sys
import os
import json
import requests
import time

# path
parent_folder = os.getcwd()
script_folder = os.path.dirname(os.path.realpath(__file__))
log_full_path = f"{script_folder}/logs.log"

sys.path.insert(1, f'{parent_folder}/toolbox/source')
import func
result = {}

########################### GLOBAL PARAMETERS ##########################

# global parameters
server = "http://dev58.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# parameters
module = "dm_CCR"
table = "dm_CCR_trade_FactTable"
pivot="dm_CCR_trade_FactTable"

# clear log file
with open(log_full_path, "w") as log_file:
    pass

########################### LOGIN TO SERVER ##########################
start = time.time()

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server, user, password))

# store token
token = result['login'][f'200 POST {server}/login']['response']['token']

headers = {'Authorization': token}

########################### FINETUNE DEFINITION ###########################
result['definition']={}
####### Get definition ######
url=f"{server}/module/{module}/definition"
resp=requests.request(method='get',url=url, headers=headers)

# response
response=resp.json()

####### Post ######
url=f"{server}/module/{module}/definition"

data=response

# Adding flatten arrayDimensions
data['arraysDimensions']=[
    {
      "dimensions": [
        {
          "label": "legalEntities_flatten",
          "array": "legalEntities"
        }
      ]
    },
    {
      "dimensions": [
        {
          "label": "legIds_flatten",
          "array": "legIds"
        },
        {
          "label": "legCurrencies_flatten",
          "array": "legCurrencies"
        }        
      ]
    }
]

data['dimensions']['legalEntities_flatten']={'enumerable': True, 'folder': 'Main', 'type': 'String', 'anonymize': False, 'description': None}
data['dimensions']['legIds_flatten']={'enumerable': True, 'folder': 'Main', 'type': 'String', 'anonymize': False, 'description': None}
data['dimensions']['legCurrencies_flatten']={'enumerable': True, 'folder': 'Main', 'type': 'String', 'anonymize': False, 'description': None}

# Modifying enumerable
for dim in data['dimensions'].keys():
    data['dimensions'][dim]['enumerable']=True

# API call
resp=requests.request(method='post',url=url,json=data, headers=headers)

try:
    response=resp.json()
except:
    response=resp.text

# Get updated definition
url=f"{server}/module/{module}/definition"
resp=requests.request(method='get',url=url, headers=headers)
response=resp.json()

result['definition'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

print("definition finetuned")

########################### FINETUNE PIVOT ###########################
result['pivot']={}
####### Get definition ######
url=f"{server}/module/{module}/pivot/{pivot}"

resp=requests.request(method='get',url=url, headers=headers)

# response
response=resp.json()

####### Post ######
url=f"{server}/module/{module}/pivot/{pivot}"
data=response

# Add flattenArraydim to dimensions list
data['dimensions'].append('legalEntities_flatten')
data['dimensions'].append('legIds_flatten')
data['dimensions'].append('legCurrencies_flatten')

# API call
resp=requests.request(method='post',url=url,json=data, headers=headers)

try:
    response=resp.json()
except:
    response=resp.text

####### Get updated pivot ######
url=f"{server}/module/{module}/pivot/{pivot}"

resp=requests.request(method='get',url=url, headers=headers)
response=resp.json()

result['pivot'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

print("pivot finetuned")

##################### PRINT LOG #####################
func.print_logs(result, log_full_path)

print(f"completed in {time.time()-start:.2f} sec")