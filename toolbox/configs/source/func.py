import sys
import os
import re
import json
import requests
import time
import random

def load_json(json_full_path):
    '''
    reads a json file as a dictionary
    '''
    with open(json_full_path, "r") as f:
        data = json.load(f)
    return data

def print_logs(obj,log_full_path):

    '''
    obj=object to print (dictionnary,list etc.)
    log_full_path= full path to log file
    '''
    with open(log_full_path, "a") as log_file:
        log_file.write(json.dumps(obj,sort_keys=False, indent=4))

def ping_api(server):

    result={}
    result['ping']={}

    # testing connection & server version
    url=f'{server}/ping'
    resp=requests.request(method='get',url= url)

    print(f"Rafal version = {resp.json()['version']}")
    print(f"Rafal DB version = {resp.json()['dbVersion']}")

    response=resp.json()

    result['ping'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    return result

def login_api(server,user,password):

    result={}
    result['login']={}

    # testing connection & server version
    url=f'{server}/login'
    resp=requests.request(method='post',url= url,json={"identifier": user,"password": password},)

    # response
    response=resp.json()

    result['login'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    return result

def datamodel_api(server,token,rdm_full_path, suffix):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    module: module name in the GET/DELETE/POST methods
    suffix: suffix to be added to module in the CREATE
    

    returns the codes and repsonses of the GET/DELETE/POST datamodel API
    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result['datamodel']={}

    ###################### POST ######################

    url=f"{server}/datamodel"

    data=load_json(rdm_full_path)

    # data_flatten=json.dumps(data)
    # # modifying facttables names
    # list_of_factables=data['definition']['tables']
    # for factable in list_of_factables:
    #     data_flatten = data_flatten.replace(factable, f"{factable}{suffix}")

    # # modifying dictionnaries names
    # list_of_dictionaries=[]
    # for decoration in data['definition']['dictionaries'].keys():
    #     list_of_dictionaries = list_of_dictionaries + [data['definition']['dictionaries'][decoration]['dict']]
    # list_of_dictionaries = set(list_of_dictionaries)

    # for dic in list_of_dictionaries:
    #     data_flatten = data_flatten.replace(dic, f"{dic}{suffix}")


    # data = json.loads(data_flatten)

    # # modifying module name
    # data['definition']['module']=f"{module}{suffix}"

    # API call
    resp=requests.request(method='post',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['datamodel'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    check_post_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### GET ######################
    module=data['definition']['module']
    url=f"{server}/datamodel/{module}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['datamodel'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    check_get_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)


    # ###################### DELETE ######################
    # url=f"{server}/datamodel/{module}"

    # resp=requests.request(method='delete',url=url, headers=headers)

    # # response
    # try:
    #     response=resp.json()
    # except:
    #     response=resp.text

    # result['datamodel'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}
    # time.sleep(1)

    # ################## CHECKS ##################
    # # checks if GET response==POST body
    # result['datamodel'][check_post_api]['check_post_body_vs_get_response']={}
    # for key in result['datamodel'][check_get_api]['response'].keys():
    #     if result['datamodel'][check_get_api]['response'][key]==result['datamodel'][check_post_api]['body'][key]:
    #         result['datamodel'][check_post_api]['check_post_body_vs_get_response'][key]='OK'
    #     else:
    #         result['datamodel'][check_post_api]['check_post_body_vs_get_response'][key]='KO'

    return result

def tablesetting_api(server,token,suffix):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    module: module name in the GET/DELETE/POST methods
    table: table name in the GET/DELETE/POST methods
    suffix: suffix to be added to table in the CREATE
    

    returns the codes and repsonses of the GET/DELETE/POST tablesetting API
    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result['tablesetting']={}

    ###################### GET A RANDOM MODULE ######################
    url=f"{server}/modules"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
        items_list=response
    except:
        response=json.loads(resp.text)
        items_list=response
    
    module=items_list[random.randint(0,len(items_list)-1)]
    time.sleep(1)

    ###################### GET LIST OF TABLES IN A RANDOM MODULE ######################
    url=f"{server}/module/{module}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
        items_list=response['tables']
    except:
        response=json.loads(resp.text)
        items_list=response
    
    table=items_list[random.randint(0,len(items_list)-1)]
    time.sleep(1)

    ###################### GET TABLESETTING FOR POSTING ######################
    
    url=f"{server}/module/{module}/tables/{table}/ingestion_settings"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['tablesetting'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}


    time.sleep(1)

    ###################### POST ######################
    url=f"{server}/module/{module}/tables/{table}{suffix}/ingestion_settings"

    data=response
    data_flatten=json.dumps(data)

    # modifying facttables names
    facttable=data['table']['table']
    data_flatten = data_flatten.replace(facttable, f"{facttable}{suffix}")

    # modifying dictionnaries names
    list_of_dictionaries=data['dictionaries']

    if len(list_of_dictionaries)>0:
        list_of_dictionaries = set(list_of_dictionaries)

        for dic in list_of_dictionaries:
            data_flatten = data_flatten.replace(dic, f"{dic}{suffix}")

    data = json.loads(data_flatten)

    # API call
    resp=requests.request(method='post',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['tablesetting'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    check_post_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### GET ######################
    url=f"{server}/module/{module}/tables/{table}{suffix}/ingestion_settings"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['tablesetting'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    check_get_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### DELETE ######################
    url=f"{server}/module/{module}/tables/{table}{suffix}/ingestion_settings"

    resp=requests.request(method='delete',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['tablesetting'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}
    time.sleep(1)

    ################## CHECKS ##################
    # checks if GET response==POST body
    result['tablesetting'][check_post_api]['check_post_body_vs_get_response']={}
    for key in result['tablesetting'][check_get_api]['response'].keys():
        if result['tablesetting'][check_get_api]['response'][key]==result['tablesetting'][check_post_api]['body'][key]:
            result['tablesetting'][check_post_api]['check_post_body_vs_get_response'][key]='OK'
        else:
            result['tablesetting'][check_post_api]['check_post_body_vs_get_response'][key]='KO'

    return result

def definition_api(server,token,suffix):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    module: module name in the GET/DELETE/POST methods
    suffix: suffix to be added to table in the CREATE
    

    returns the codes and repsonses of the GET/DELETE/POST tablesetting API
    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result['definition']={}

    ###################### GET LIST OF MODULE ######################
    url=f"{server}/modules"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
        items_list=response
    except:
        response=json.loads(resp.text)
        items_list=response

    time.sleep(1)

    ###################### GET ON EXISTING MODULE ######################

    module=items_list[random.randint(0,len(items_list)-1)]

    url=f"{server}/module/{module}/definition"
    # url=f"{server}/module/{module}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    time.sleep(1)

    temp={'status':resp.status_code, 'body':"", 'response':response}

    ###################### POST ######################
    url=f"{server}/module/{module}/definition"

    data=response
    
    # modifying enumerable
    for dim in data['dimensions'].keys():
        data['dimensions'][dim]['enumerable']=True

    # API call
    resp=requests.request(method='post',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['definition'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    check_post_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### GET ######################
    url=f"{server}/module/{module}/definition"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['definition'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    check_get_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)
    
    ###################### DELETE ######################
    url=f"{server}/module/{module}/definition"

    resp=requests.request(method='delete',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['definition'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}
    time.sleep(1)

    # reposting
    url=f"{server}/module/{module}/definition"
    data=result['definition'][check_post_api]['body']
    resp=requests.request(method='post',url=url,json=data, headers=headers)
    time.sleep(1)

    ################## CHECKS ##################
    # checks if GET response==POST body
    result['definition'][check_post_api]['check_post_body_vs_get_response']={}
    for key in result['definition'][check_get_api]['response'].keys():
        if result['definition'][check_get_api]['response'][key]==result['definition'][check_post_api]['body'][key]:
            result['definition'][check_post_api]['check_post_body_vs_get_response'][key]='OK'
        else:
            result['definition'][check_post_api]['check_post_body_vs_get_response'][key]='KO'

    return result

def dimensiongroup_api(server,token,module,suffix):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    module: module name in the GET/DELETE/POST methods
    suffix: suffix to be added to table in the CREATE
    

    returns the codes and repsonses of the GET/DELETE/POST dimensiongroup API
    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result['dimensiongroup']={}

    ###################### GET LIST OF DIMENSIONGROUP ######################
    url=f"{server}/module/{module}/dimension/group"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()

    except:
        response=json.loads(resp.text)

    time.sleep(1)

    ###################### GET DIMENSIONGROUP FOR POSTING ######################
    group=response[random.randint(0,len(response)-1)]['name']
    
    url=f"{server}/module/{module}/dimension/group/{group}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['dimensiongroup'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

    time.sleep(1)

    temp={'status':resp.status_code, 'body':"", 'response':response}

    ###################### POST ######################
    url=f"{server}/module/{module}/dimension/group/{group}{suffix}"

    data=response
    
    # modifying group name
    data['name']=f"{data['name']}{suffix}"

    # API call
    resp=requests.request(method='post',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['dimensiongroup'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    check_post_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### GET ######################
    url=f"{server}/module/{module}/dimension/group/{group}{suffix}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['dimensiongroup'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    check_get_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### DELETE ######################
    url=f"{server}/module/{module}/dimension/group/{group}{suffix}"

    resp=requests.request(method='delete',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['dimensiongroup'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}
    time.sleep(1)

    ################## CHECKS ##################
    # checks if GET response==POST body
    result['dimensiongroup'][check_post_api]['check_post_body_vs_get_response']={}
    for key in result['dimensiongroup'][check_get_api]['response'].keys():
        try:
            if result['dimensiongroup'][check_get_api]['response'][key]==result['dimensiongroup'][check_post_api]['body'][key]:
                result['dimensiongroup'][check_post_api]['check_post_body_vs_get_response'][key]='OK'
            else:
                result['dimensiongroup'][check_post_api]['check_post_body_vs_get_response'][key]='KO'
        except:
            result['dimensiongroup'][check_post_api]['check_post_body_vs_get_response'][key]='KO'
    return result

def limit_api(server,token,module,suffix):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    module: module name in the GET/DELETE/POST methods
    suffix: suffix to be added to table in the CREATE
    

    returns the codes and repsonses of the GET/DELETE/POST dimensiongroup API
    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result['limit']={}

    ###################### GET LIST OF LIMIT ######################
    url=f"{server}/module/{module}/limit"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
        items_list=list(response.keys())
    except:
        response=resp.text
        print(f'error with {url}: {response}')

    time.sleep(1)

    ###################### GET LIMIT FOR POSTING ######################
    limit=items_list[random.randint(0,len(items_list)-1)]
    
    url=f"{server}/module/{module}/limit/{limit}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['limit'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

    time.sleep(1)

    temp={'status':resp.status_code, 'body':"", 'response':response}

    ###################### POST ######################
    url=f"{server}/module/{module}/limit/{limit}"

    data=response

    # API call
    resp=requests.request(method='post',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['limit'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    check_post_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### GET ######################
    url=f"{server}/module/{module}/limit/{limit}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['limit'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    check_get_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### DELETE ######################
    url=f"{server}/module/{module}/limit/{limit}"

    resp=requests.request(method='delete',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['limit'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}
    time.sleep(1)

    # reposting
    url=f"{server}/module/{module}/limit/{limit}"
    data=result['limit'][check_post_api]['body']
    resp=requests.request(method='post',url=url,json=data, headers=headers)
    time.sleep(1)

    ################## CHECKS ##################
    # checks if GET response==POST body
    result['limit'][check_post_api]['check_post_body_vs_get_response']={}
    for key in result['limit'][check_get_api]['response'].keys():
        if result['limit'][check_get_api]['response'][key]==result['limit'][check_post_api]['body'][key]:
            result['limit'][check_post_api]['check_post_body_vs_get_response'][key]='OK'
        else:
            result['limit'][check_post_api]['check_post_body_vs_get_response'][key]='KO'

    return result

def pivot_api(server,token,module,suffix):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    module: module name in the GET/DELETE/POST methods
    suffix: suffix to be added to table in the CREATE
    

    returns the codes and repsonses of the GET/DELETE/POST dimensiongroup API
    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result['pivot']={}

    ###################### GET LIST OF PIVOT ######################
    url=f"{server}/module/{module}/pivot"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
        items_list=response
    except:
        response=resp.text
        print(f'error with {url}: {response}')

    time.sleep(1)

    ###################### GET PIVOT FOR POSTING ######################
    pivot=items_list[random.randint(0,len(items_list)-1)]['name']
    
    url=f"{server}/module/{module}/pivot/{pivot}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['pivot'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

    time.sleep(1)

    ###################### POST ######################
    url=f"{server}/module/{module}/pivot/{pivot}{suffix}"

    data=response

    # modify body for posting
    data['name']=f"{data['name']}{suffix}"

    # API call
    resp=requests.request(method='post',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['pivot'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    check_post_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### GET ######################
    url=f"{server}/module/{module}/pivot/{pivot}{suffix}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['pivot'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    check_get_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### DELETE ######################
    url=f"{server}/module/{module}/limit/{pivot}{suffix}"

    resp=requests.request(method='delete',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['pivot'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}
    time.sleep(1)


    ################## CHECKS ##################
    # checks if GET response==POST body
    result['pivot'][check_post_api]['check_post_body_vs_get_response']={}
    for key in result['pivot'][check_get_api]['response'].keys():
        if result['pivot'][check_get_api]['response'][key]==result['pivot'][check_post_api]['body'][key]:
            result['pivot'][check_post_api]['check_post_body_vs_get_response'][key]='OK'
        else:
            result['pivot'][check_post_api]['check_post_body_vs_get_response'][key]='KO'
    
    return result

def user_api(server,token,suffix):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    suffix: suffix to be added to table in the CREATE
    
    returns the codes and repsonses of the GET/DELETE/POST dimensiongroup API
    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result['user']={}

    ###################### GET LIST OF USERS ######################
    url=f"{server}/users"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text
        print(f'error with {url}: {response}')

    time.sleep(1)

    ###################### GET USERID FOR POSTING ######################
    userid=response[random.randint(0,len(response)-1)]['id']
    
    url=f"{server}/users/{userid}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['user'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

    time.sleep(1)

    ###################### POST USER ######################
    url=f"{server}/users"

    data=response

    # Modify userid and password
    userid=f"{data['id']}{suffix}"
    data['id']=userid
    data['password']="Welcome"

    # API call
    resp=requests.request(method='post',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['user'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    check_post_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### MODIFY PASSWORD ######################
    url=f"{server}/users/{userid}/resetpassword"

    data={"identifier": userid,
    "password": "Welcome?"
    }

    # API call
    resp=requests.request(method='post',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['user'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    check_post_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### GET ######################
    url=f"{server}/users/{userid}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['user'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    check_get_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### DELETE ######################
    url=f"{server}/users/{userid}"

    resp=requests.request(method='delete',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['user'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}
    time.sleep(1)


    ################## CHECKS ##################
    # checks if GET response==POST body
    result['user'][check_post_api]['check_post_body_vs_get_response']={}
    for key in result['user'][check_get_api]['response'].keys():
        if result['user'][check_get_api]['response'][key]==result['user'][check_post_api]['body'][key]:
            result['user'][check_post_api]['check_post_body_vs_get_response'][key]='OK'
        else:
            result['user'][check_post_api]['check_post_body_vs_get_response'][key]='KO'

    return result

def calculator_api(server,token,module,suffix):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    module: module name in the GET/DELETE/POST methods
    suffix: suffix to be added to table in the CREATE
    

    returns the codes and repsonses of the GET/DELETE/POST calculator API
    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result['calculator']={}

    ###################### GET LIST OF CALCULATORS ######################
    url=f"{server}/module/{module}/calculator"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
        items_list=response
    except:
        response=resp.text
        print(f'error with {url}: {response}')

    time.sleep(1)

    ###################### GET CALCULATOR FOR POSTING ######################
    calculator=items_list[random.randint(0,len(items_list)-1)]['name']
    
    url=f"{server}/module/{module}/calculator/{calculator}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['calculator'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

    time.sleep(1)

    ###################### POST A CALCULATOR ######################
    url=f"{server}/module/{module}/calculator/{calculator}{suffix}"

    data=response
    data['name']=f"{data['name']}{suffix}"

    # API call
    resp=requests.request(method='post',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['calculator'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    check_post_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### GET ######################
    url=f"{server}/module/{module}/calculator/{calculator}{suffix}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['calculator'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    check_get_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### DELETE ######################
    url=f"{server}/module/{module}/calculator/{calculator}{suffix}"

    resp=requests.request(method='delete',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['calculator'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}
    time.sleep(1)

    ###################### GET PERSONNALIZED DRILL FUNCTIONS FOR A CALCULATOR ######################
    url=f"{server}/module/{module}/calculator/{calculator}/listUserDrill"

    # API call
    resp=requests.request(method='get',url=url, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['calculator'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

    time.sleep(1)


    ################## CHECKS ##################
    # checks if GET response==POST body
    result['calculator'][check_post_api]['check_post_body_vs_get_response']={}
    for key in result['calculator'][check_get_api]['response'].keys():
        if result['calculator'][check_get_api]['response'][key]==result['calculator'][check_post_api]['body'][key]:
            result['calculator'][check_post_api]['check_post_body_vs_get_response'][key]='OK'
        else:
            result['calculator'][check_post_api]['check_post_body_vs_get_response'][key]='KO'
    
    return result


def usergroup_api(server,token,suffix):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    module: module name in the GET/DELETE/POST methods
    suffix: suffix to be added to table in the CREATE
    

    returns the codes and responses of the GET/DELETE/POST calculator API
    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result['group']={}

    ###################### GET LIST OF GROUPS ######################
    url=f"{server}/group"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
        items_list=response
    except:
        response=resp.text
        print(f'error with {url}: {response}')

    time.sleep(1)

    ###################### GET ADMIN GROUP ######################
    groups=[items_list[i]['name'] for i in range(len(items_list))]
    group='Admin'
    
    
    url=f"{server}/group/{group}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    policies=response['policies']

    result['group'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

    time.sleep(1)

    ###################### POST EMPTY GROUP ######################
    url=f"{server}/group/{group}{suffix}"

    # API call
    resp=requests.request(method='post',url=url, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['group'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':"", 'response':response}

    check_post_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"

    time.sleep(1)

    ###################### GET GROUP ######################
    url=f"{server}/group/{group}{suffix}"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['group'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    check_get_api=f"{resp.status_code} {resp.request.method} {resp.request.url}"
    time.sleep(1)

    ###################### GET LIST OF USERS IN A GROUP ######################
    url=f"{server}/group/{group}/user"

    resp=requests.request(method='get',url=url, headers=headers)

    # response
    try:
        response=resp.json()
        items_list=response
    except:
        response=resp.text
        items_list=response

    user=items_list[random.randint(0,len(items_list)-1)]

    result['group'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

    time.sleep(1)

    ###################### REMOVE USER FROM A GROUP ######################
    url=f"{server}/group/{group}/user/{user}"

    # API call
    resp=requests.request(method='delete',url=url, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['group'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

    time.sleep(1)

    ###################### ADD USER TO A GROUP ######################
    url=f"{server}/group/{group}/user/{user}"

    # API call
    resp=requests.request(method='put',url=url, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['group'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

    time.sleep(1)

    ###################### REMOVE AGAIN USER FROM A GROUP ######################
    url=f"{server}/group/{group}/user/{user}"

    # API call
    resp=requests.request(method='delete',url=url, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['group'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':'', 'response':response}

    time.sleep(1)

    ###################### ADD USER TO A SEVERAL GROUPS ######################
    url=f"{server}/group/user/{user}"

    data=[group,f"{group}{suffix}"]

    # API call
    resp=requests.request(method='put',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['group'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    time.sleep(1)

    ###################### ADD A POLICY TO A GROUP ######################
    url=f"{server}/group/{group}{suffix}/policy"

    policy=policies[random.randint(0,len(policies)-1)]
    data=policy

    # API call
    resp=requests.request(method='put',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['group'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    time.sleep(1)

    ###################### REMOVE POLICY FROM GROUP ######################
    url=f"{server}/group/{group}{suffix}/policy"
    data=policy

    # API call
    resp=requests.request(method='delete',url=url,json=data, headers=headers)

    try:
        response=resp.json()
    except:
        response=resp.text

    result['group'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    time.sleep(1)

    ###################### DELETE GROUP ######################
    url=f"{server}/group/{group}{suffix}"

    resp=requests.request(method='delete',url=url, headers=headers)

    # response
    try:
        response=resp.json()
    except:
        response=resp.text

    result['group'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}
    time.sleep(1)


    return result

