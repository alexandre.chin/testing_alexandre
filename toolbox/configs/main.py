import sys
import os
import json
import requests
import time
from source import func

result={}

########################### GLOBAL PARAMETERS ##########################

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
file_folder='data'
log_full_path=f"{script_folder}/logs.log"

# global parameters
server = "http://review_master.opensee.ninja:8080"
user = "admin"
password = "Welcome?"

##############################################################################

# clear log file
with open(log_full_path, "w") as log_file:
  pass

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
headers={'Authorization': token}

# datamodel API
print('testing datamodel API...')
suffix='_test_api_datamodel'
rdm_full_path=f"{script_folder}/data/rdm.json"

try:
  result.update(func.datamodel_api(server,token,rdm_full_path,suffix))
except:
  result[suffix]={'status':'KO: please debug for investigation'}

# tablesetting API
print('testing tablesetting API...')
suffix='_test_api_tablesetting'
try:
  result.update(func.tablesetting_api(server,token,suffix))
except:
  result[suffix]={'status':'KO: please debug for investigation'}

# definition API
print('testing definition API...')
suffix='_test_api_definition'

try:
  result.update(func.definition_api(server,token,suffix))
except:
  result[suffix]={'status':'KO: please debug for investigation'}

# dimensiongroup api
print('testing dimensiongroup API...')
module='HistoVaR'
suffix='_test_api_dimensiongroup'

try:
  result.update(func.dimensiongroup_api(server,token,module,suffix))
except:
  result[suffix]={'status':'KO: please debug for investigation'}

# limit api
print('testing limit API...')
module='HistoVaR'
suffix='_test_api_limit'

try:
  result.update(func.limit_api(server,token,module,suffix))
except:
  result[suffix]={'status':'KO: please debug for investigation'}

# pivot api
print('testing pivot API...')
module='HistoVaR'
suffix='_test_api_pivot'

try:
  result.update(func.pivot_api(server,token,module,suffix))
except:
  result[suffix]={'status':'KO: please debug for investigation'}

# user api
print('testing user API...')
suffix='_test_api_user'

try:
  result.update(func.user_api(server,token,suffix))
except:
  result[suffix]={'status':'KO: please debug for investigation'}

# calculator api
print('testing calculator API...')
module='HistoVaR'
suffix='_test_api_calculator'

try:
  result.update(func.calculator_api(server,token,module,suffix))
except:
  result[suffix]={'status':'KO: please debug for investigation'}


# usergroup api
print('testing usergroup API...')
suffix='_test_api_usergroup'

try:
  result.update(func.usergroup_api(server,token,suffix))
except:
  result[suffix]={'status':'KO: please debug for investigation'}


# print results
print('generating log file...')
func.print_logs(result,log_full_path)