import sys
import os
import json
import requests
import time
from source import func

result={}

########################### GLOBAL PARAMETERS ##########################

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
log_full_path=f"{script_folder}/logs.log"

# global parameters
server = "http://review_master.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# clear log file
with open(log_full_path, "w") as log_file:
  pass

########################### LOGIN TO SERVER ##########################

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
headers={'Authorization': token}

########################### TESTING CALCULATOR EXECUTION ##########################
print('testing calculators...')
suffix='_executecalculator_api'
module='HistoVaR'
json_full_path=f"{script_folder}/source/body.json"
data=func.load_json(json_full_path)

try:
  result.update(func.executecalculator_api(server,token,module,data))
except:
  result[suffix]={'status':'KO: please debug for investigation'}

# print results
print('generating log file...')
func.print_logs(result,log_full_path)