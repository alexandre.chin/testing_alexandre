import sys
import os
import re
import json
import requests
import time
import random

def load_json(json_full_path):
    '''
    reads a json file as a dictionary
    '''
    with open(json_full_path, "r") as f:
        data = json.load(f)
    return data

def print_logs(obj,log_full_path):

    '''
    obj=object to print (dictionnary,list etc.)
    log_full_path= full path to log file
    '''
    with open(log_full_path, "a") as log_file:
        log_file.write(json.dumps(obj,sort_keys=False, indent=4))

def ping_api(server):

    result={}
    result['ping']={}

    # testing connection & server version
    url=f'{server}/ping'
    resp=requests.request(method='get',url= url)

    print(f"Rafal version = {resp.json()['version']}")
    print(f"Rafal DB version = {resp.json()['dbVersion']}")

    response=resp.json()

    result['ping'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':"", 'response':response}

    return result

def login_api(server,user,password):

    result={}
    result['login']={}

    # testing connection & server version
    url=f'{server}/login'
    resp=requests.request(method='post',url= url,json={"identifier": user,"password": password},)

    # response
    response=resp.json()

    result['login'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code,'body':{"identifier": user,"password": password}, 'response':response}

    return result

def executecalculator_api(server,token,module,data):
    '''
    server: url to the server (ex. http://review_master.opensee.ninja:8080)
    module: module name in the GET/DELETE/POST methods
    data: dictionary containing the body query parameters

    runs a Python calculator

    '''

    headers={'Authorization': token}
    headers['Content-Type']='json'

    result={}
    result['executecalculator']={}

    ###################### RUNS A PYTHON CALCULATOR ######################
    url=f"{server}/module/{module}/calculate"

    resp=requests.request(method='post',url=url,json=data, headers=headers)

    # response
    try:
        response=resp.json()

    except:
        response=resp.text

    time.sleep(1)

    result['executecalculator'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    ###################### TESTS A CALCULATOR ON THE SERVER ######################
    url=f"{server}/module/{module}/testcalculator"

    resp=requests.request(method='post',url=url,json=data, headers=headers)

    # response
    try:
        response=resp.json()

    except:
        response=json.loads(resp.text)

    time.sleep(1)

    result['executecalculator'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    ###################### RUNS A GIVEN CALCULATOR REQUEST ######################
    url=f"{server}/module/{module}/calculatorrequest"

    resp=requests.request(method='post',url=url,json=data, headers=headers)

    # response
    try:
        response=resp.json()

    except:
        response=json.loads(resp.text)

    time.sleep(1)

    result['executecalculator'][f"{resp.status_code} {resp.request.method} {resp.request.url}"]={'status':resp.status_code, 'body':data, 'response':response}

    return result