import sys
import os
import json
import requests
import time

########################### GLOBAL PARAMETERS ##########################

server = "http://review_1329-validation-policies.opensee.ninja:8080"
# server = "http://review_master.opensee.ninja:8080"
# server = "http://review_1355-diff-api-controller.opensee.ninja:8080"
# server="http://review_feature-ingestion-by-block.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# parameters
module = "module_alexandre"
table= "Facttable"
dict ="Dictionary"
ziptable="Ziptable"

# module = "testing_Arrays"
# table= "testing_Arrays_trade_FactTable"
# dict ="testing_Arrays_csa_Dictionary"
# ziptable="testing_Arrays_riskfactors_ZipTable"

########################### STEPS ##########################
steps={
"create_dm_with_autocube":{'type':'autocube','file_name':'autocube_input.json'},

"create_userGroup":{'type':'createUserGroup','userGroup':'test_group'},

"create_user_alexandre":{'type':'createUser','userId':'alexandre_1329','groups':['test_group'],'password':"Welcome?"},

"trigger_wf":{'type':'triggerWF','body':{
    "workflowTemplate": "template1",
        "table": "Facttable",
    "source": "PRIVATE_guillaume",
    "destination": "official",
    "asOf": "2021-05-07",
    "conflictResolution": "incoming",
    "name": "MR1",
    "comment": "comment",
    "variables": {}
}},

"login_alexandre":{'type':'login','user':'alexandre_1329','password':"Welcome?"},

"add_policy":{'type':'addPolicytoGroup','userGroup':'test_group','body':{'id': '9de78f13-bc16-4bbb-abcd-8b5db16c4ffc', 'actions': ['canCreateValidationRequest'], "resource": "validationrequest:template1"}},        


"trigger_wf_again":{'type':'triggerWF','body':{
    "workflowTemplate": "template1",
        "table": "Facttable",
    "source": "PRIVATE_guillaume",
    "destination": "official",
    "asOf": "2021-05-07",
    "conflictResolution": "incoming",
    "name": "MR1",
    "comment": "comment",
    "variables": {}
}},


}


########################### PATHS ##########################
# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
log_full_path=f"{script_folder}/logs.log"

sys.path.insert(1, f'{parent_folder}/toolbox/source')
import func

# clear log file
with open(log_full_path, "w") as log_file:
  pass

checkpoints_trigger=True
result={}

########################### LOGIN TO SERVER ##########################

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...',end="\n")
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
headers={'Authorization': token}


########################### STEPS ##########################
dm_trigger=True

for step in steps:
    type=steps[step]['type']

    # autocube
    if type=="autocube":

        headers['Content-Type']='json'
        file_name=steps[step]['file_name']

        url=f"{server}/autocube/{module}"

        file_full_path=f"{script_folder}/source/{file_name}"
        autocube_input_dict=func.load_json(file_full_path)

        try:
            #Deletes module if it aleady exists
            autocube_input_dict['mode']='Delete'

            resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
            time.sleep(1)
        finally:
            #creates module
            autocube_input_dict['mode']='Create'

            resp=requests.request(method='post',url=url,json=autocube_input_dict, headers=headers)
            time.sleep(1)

            if resp.status_code!=200:
                dm_trigger=False

            # Set insertMethod to Linear
            url=f"{server}/module/{module}/tables/{table}/ingestion_settings"
            resp=requests.request(method='get',url=url, headers=headers)
            body=resp.json()
            body["insertMethod"]="Linear"

            resp=requests.request(method='post',url=url,json=body, headers=headers)

        if resp.status_code not in [200,201,204]:
            result[step]={"type":type,"status":"KO","url":url,"body":body,"response":resp.json()}
            print("datamodel error : could not create datamodel; please check logs or debug to investigate")
            func.print_logs(result,log_full_path)
            sys.exit()

    # rdm
    elif type=="rdm":
        headers['Content-Type']='json'
        file_name=steps[step]['file_name']

        url=f"{server}/datamodel"

        file_full_path=f"{script_folder}/source/{file_name}"
        body=func.load_json(file_full_path)

        resp=requests.request(method='post',url=url,json=body, headers=headers)
        time.sleep(1)

        if resp.status_code not in [200,201,204]:
            result[step]={"type":type,"status":"KO","url":url,"body":body,"response":resp.json()}
            print("datamodel error : could not create datamodel; please check logs or debug to investigate")
            func.print_logs(result,log_full_path)
            sys.exit()

    #create user
    elif type=='createUser':
        headers['Content-Type']='json'
        url=f"{server}/users"
        userId=steps[step]['userId']
        groups=steps[step]['groups']
        password=steps[step]['password']

        try:
            requests.request(method='delete',url=f'{url}/{userId}', headers=headers)

        finally:
            body={'id':userId,'groups':groups,'password':password}  
            resp=requests.request(method='post',url=url,json=body, headers=headers)

    #create user group
    elif type=='createUserGroup':
        userGroup=steps[step]['userGroup']
        url=f"{server}/group/{userGroup}"

        try:
            resp=requests.request(method='delete',url=url, headers=headers)
        finally:
            resp=requests.request(method='post',url=url, headers=headers)

    #add user to group
    elif type=='addUsertoGroup':
        user=steps[step]['user']
        userGroup=steps[step]['userGroup']
        url=f"{server}/group/{userGroup}/user/{user}"

        resp=requests.request(method='put',url=url, headers=headers)

    #add policy to group
    elif type=='addPolicytoGroup':
        userGroup=steps[step]['userGroup']
        body=steps[step]['body']
        url=f"{server}/group/{userGroup}/policy"

        resp=requests.request(method='put',url=url,json=body, headers=headers)

    #trigger WF
    elif type=='triggerWF':
        headers['Content-Type']='json'
        body=steps[step]['body']
        url=f'{server}/validation/request'

        resp=requests.request(method='put',url=url,json=body, headers=headers)
    
    #login
    elif type=='login':
        user=steps[step]['user']
        password=steps[step]['password']

        url=f'{server}/login'
        resp=requests.request(method='post',url= url,json={"identifier": user,"password": password},)

        # store token
        token=result['login'][f'200 POST {server}/login']['response']['token']
        headers={'Authorization': token}

    #queryCheckpoint
    elif type=="queryCheckpoint":
        file_name=steps[step]['file_name']
        input_file_full_path=f"{script_folder}/source/checkpoints/{file_name}"
        data=func.load_json(input_file_full_path)["body"]

        curent_output=func.query_api(server,token,module,data)
        expected_output=func.load_json(input_file_full_path)["expected_response"]

    #calculatorRequestCheckpoint
    elif type=="calculatorRequestCheckpoint":
        file_name=steps[step]['file_name']
        input_file_full_path=f"{script_folder}/source/checkpoints/{file_name}"
        data=func.load_json(input_file_full_path)["body"]

        curent_output=func.executecalculator_api(server,token,module,data)
        expected_output=func.load_json(input_file_full_path)["expected_response"]


    #queryDiffCheckpoint
    elif type=="queryDiffCheckpoint":
        file_name=steps[step]['file_name']
        input_file_full_path=f"{script_folder}/source/checkpoints/{file_name}"
        data=func.load_json(input_file_full_path)["body"]

        curent_output=func.query_api(server,token,module,data,"diff")
        expected_output=func.load_json(input_file_full_path)["expected_response"]

    #Unknown steps
    else:
        result[step]={"type":type,"status":"KO","comment":"type unknown"}
        print(f"{step} ({type}): KO. type is unknown",end="\n")

    #checkpoints checks
    if "Checkpoint" not in type:
        if resp.status_code in [200,201,204]:
            print(f"{step} ({type}): OK",end="\n")
        else:
            try:
                result[step]={"type":type,"status":"KO","url":url,"body":body,"response":resp.json()}
            except:
                result[step]={"type":type,"status":"KO","url":url,"body":body,"response":resp.text}
            print(f"{step} ({type}): KO",end="\n")

############# CHECKS ##############
    if "Checkpoint" in type :
    # Compare response with expected output
        try:
            trigger=func.query_validator(curent_output,expected_output)
            if trigger:
                print(f"{step} ({type}): OK",end="\n")
            else:
                checkpoints_trigger=False
                result[step]={"type":type,"status":"KO","module":module,"query_body":data,"query_response":curent_output,"expected_response":expected_output}
                print(f"{step} ({type}): KO",end="\n")
        except:
            trigger=False
            checkpoints_trigger=False
            print(f"{step} ({type}):KO. Please debug for more details",end="\n")

# print results
func.print_logs(result,log_full_path)

if checkpoints_trigger:
    print(f'all tests succeeded. log file available here: {log_full_path}')
else:
    print(f'Some tests failed. log file available here:{log_full_path}')