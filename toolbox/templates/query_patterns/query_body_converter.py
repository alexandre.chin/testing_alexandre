import sys
import os
import json

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
dataset_folder=f"{script_folder}/source/dataset"

# versioning
date='2021-05-07'
branch = "official"

with open(f'{script_folder}/source/output/all_queries.json', "r") as f:
    queries = json.load(f)

########################################### GENERATE QUERY BODY FOR REQUEST API FROM PYTHON QUERY BODY ##########################
for query in queries:
    # query body template
    request_api_body={
    "metrics": [],
    "postaggregaggr": {},
    "with_facet": False,
    "filter_on_query": None,
    "batch": False,
    "batch_description": None,
    "currency": None,
    "pivot": "",
    "by": [],
    "where": {
        "@and": [{"Date": {"values": [date]}}]
    },
    "branch":branch
}

    # Metrics
    request_api_body['metrics']=queries[query]['query_body']['python_query_body']['Metrics']

    # By clause
    request_api_body['by']=queries[query]['query_body']['python_query_body']['By']

    # Where clause
    for where_term in queries[query]['query_body']['python_query_body']['Where']:
        request_api_body['where']["@and"].append({where_term: {"values": queries[query]['query_body']['python_query_body']['Where'][where_term]}})

    # Store request API body
    queries[query]['query_body']['request_api_body']=request_api_body

# Print query body
with open(f'{script_folder}/source/output/all_queries.json', "w") as outfile:
    outfile.write(json.dumps(queries,sort_keys=False,indent=4))

print("request API bodies generated")