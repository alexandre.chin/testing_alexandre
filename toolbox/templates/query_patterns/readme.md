Run these 4 scripts:

1. python_querries_generator.py: generates all query patterns (python query body format) to be tested

2. query_body_converter.py: convert python query body format to request API body syntax

3. python_engine.py: performs all query patterns generated at step 1 using the Python engine

4. performs all query patterns generated at step 2 using the query engine

Test reports are located in source/output

