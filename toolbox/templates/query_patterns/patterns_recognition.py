import numpy as np
import json
 
#Put your json fil of the queries here , then run the script
file = open('failed_queries2.json')

        

# we have [x_1, x_2, ... , x_i , ... , x_m] so m set of parameters of SQL requetes 
# x_i = [a_1, a_2, ... , a_j , ... a_n] for instance [1, 2 , ... , 0, ..., 1] a set of parameter

## Creation of the group (each one defines by a class)

def classCreation(possibleValue, nbParameters):
    Classs = []
    for value in possibleValue:
        Classs.append([value])
    for i in range(nbParameters-1):
        newClasss=[]   
        for value in possibleValue :
            for class_ in Classs :
                newclass = class_.copy()
                newclass.append(value)
                newClasss.append(newclass)
        Classs=newClasss.copy()
    return Classs
 

def groupsCreation(Classs, minComplexity):
    Groups =[]
    for class_ in Classs :
        nbVariables= class_.count(-1)
        if nbVariables == minComplexity:
            Groups.append([nbVariables, class_, [] ])
    return(Groups)

#####-------------------------------------------------

## Functions to assign the values in the right group

def classAlignement(class_ ,element):
    output = element.copy()
    indexs = [i for i,e in enumerate(class_) if e == -1]
   # print(element)
    for i in indexs :
        output[i] = -1
    #print(element)
    return output

def match(class_,element): 
    return class_ == classAlignement(class_, element)

def allocation(values, group):
    for i in range(len(values)):
        result = match(group[1],values[i])
        if result:
            group[2].append(i)
    return group

def grouping(values, Groups):
    for i in range(len(Groups)) :
        Groups[i]=allocation(values, Groups[i])
    return(Groups)

#--------------------------------------------------------

## 
#Get only the group where interesting pattern can be found and replace the class type by the pattern
def meaningFullGroups(Groups,simplifiedValues,possibleValue):
    usefull=[]
    trashIndexs=[]
    for i in range(len(Groups)):
        a=foundPattern(simplifiedValues,Groups[i],possibleValue,2)
        if a:
            Groups[i][1] = a
            usefull.append(i)
            for j in Groups[i][2]:
                if j not in trashIndexs:
                    trashIndexs.append(j)
    updatedSimplifiedValues = [ j for i,j in enumerate(simplifiedValues) if i not in trashIndexs]    
    return [Groups[i] for i in usefull], updatedSimplifiedValues
    
def foundPattern(values,group, possibleValue, pollutionlevelG3=1,pollutionLevelG2=2):
    TestG3=checkGroupOf3(group,possibleValue,pollutionlevelG3)
    TestG2=checkGroupOf2(values, group,possibleValue,pollutionLevelG2)
    if TestG3:
        return TestG3
    elif TestG2:
        return TestG2
    else :
        return False
    
def checkGroupOf3(group,possibleValue, pollutionLevel=1):
    if group[0]>= pollutionLevel:

        if len(possibleValue)**group[0] == len(group[2]) and len(group[2]) != 1:
            indexs = [i for i,e in enumerate(group[1]) if e == -1]
            for i in indexs:
                group[1][i]=possibleValue
            return group[1]
        else :
            return False
    else :
        return False

def checkGroupOf2(values, group,possibleValue,pollutionLevel=2):
    if group[0] >= pollutionLevel :
        indexs = [i for i,e in enumerate(group[1]) if e == -1]
        matchIndex=[]
        matchValue=[]
        for i in indexs:
            foundValue =[]
            for element in group[2]:
                if values[element][i] not in foundValue :
                    foundValue.append(values[element][i])
            if len(foundValue)==2:
                matchIndex.append(i)
                matchValue.append(foundValue)
        if len(matchIndex) == 0:
            return False
        else :
            if len(possibleValue)**(group[0]-len(matchIndex)) * (len(possibleValue)-1)**(len(matchIndex)) == len(group[2]):
                for i in indexs:
                    if i in matchIndex:
                        index=[j for j,e in enumerate(matchIndex) if e == i]
                        group[1][matchIndex[index[0]]] = matchValue[index[0]]
                    else:
                        group[1][i]=possibleValue
                return group[1]
            else :
                return False
    else :
        return False




#------------------------------------------------------------------------------

## Function to pars the json of the queries and recreate a json looking like the entrered json

## Parsing query body
def getStructure(dict_):
    d=dict_["query_body"]["query_structure"]
    structure={}
    for key in d: 
        if len(d[key].keys()) == 2:
            structure[key]={"Where": "" , "By" : ""}
        if len(d[key].keys()) == 1:
            for i in d[key]:
                structure[key]={ i : "" }
            
    return structure

def reBuildDict(value,structure):
    output={}
    i=0
    for key in structure:
        if len(structure[key].keys()) == 2:
            output[key]={"Where": value[i], "By" :value[i+1] }
            i=i+2
        if  len(structure[key].keys()) == 1:
            for j in structure[key]:
                output[key]={ j :value[i]}
            i=i+1
    return output


def pars_queryStructure(dict_):
    output= []
    d=dict_["query_body"]["query_structure"]
    for key in d: 
        if len(d[key].keys()) == 2:
            output.append(d[key]["Where"])
            output.append(d[key]["By"])
        if len(d[key].keys()) == 1:
            output.append(d[key][str(list(d[key].keys())[0])])
            
    return output

#-------------------------------------------------------------
## Functions to evict useless indexs regarding pattern recognition in values and then to readd.


## put out useless variables in the set of values
def cleanValues(values):
    indexs=[ [] for i in range(len(values[0]))]
    usefullIndexs=[]
    for i in range(len(values)):
        for j in range(len(values[i])):
            if values[i][j] not in indexs[j] and values[i][j] != 0:
                indexs[j].append(values[i][j])
    for i in range(len(indexs)):
        if indexs[i]:
            usefullIndexs.append(i)
    output=[]
    for value in values:
        output.append([ value[i] for i in usefullIndexs]) 

    return output, usefullIndexs

def reBuildValue(indexs,usefullIndexs,Groups):
    output = []
    k=0
    for group in Groups:
        i=0
        output.append([])
        for j in indexs:
            if j in usefullIndexs :
                output[k].append(group[1][i])
                i = i + 1
            else :
                output[k].append(0)
        k = k + 1
    return output

def foundMaxSize(nbValues,nbUsefullIndexs):
    list_=[2**i for i in range(nbUsefullIndexs)]
    list_.append(nbValues)
    list_.sort()
    return min(nbUsefullIndexs, list_.index(nbValues)+1)

#---------------------------------------------------------------------

def descendingProcess(simplifiedValues,possibleValue,classValues,maxSize):

    Results=[]
    while maxSize != 0 and len(simplifiedValues) != 0 :
        print(str(len(simplifiedValues)) + " queries left")
        Groups=groupsCreation(classCreation(classValues, len(simplifiedValues[0])),maxSize)
        print("groupes created with a complexity of "+ str(maxSize) + " which give " + str(len(Groups)) + " groups.")  #with more than 4000 groups the script is a bit long
        Groups=grouping(simplifiedValues,Groups)
        print("Group filled")
        newResults, simplifiedValues= meaningFullGroups(Groups,simplifiedValues,possibleValue)
        Results = Results + newResults
        maxSize = maxSize -1
    return Results

    
##Main script


 
#DATA
data = json.load(file)
structure= getStructure([data[key] for key in data][0])
values = [pars_queryStructure(data[key]) for key in data]
possibleValue=[0,1,2]
classValues = possibleValue.copy()
classValues.append(-1) 

#Simplified data for faster grouping
simplifiedValues,usefullIndexs=cleanValues(values)
print(simplifiedValues[0:2]) #print few examples of the simplified values that will be treated

#Groups creation, play on the minCmplexity to return only the more interesting groups and have a shorter computation
#maxSize=foundMaxSize(len(values),len(usefullIndexs)) # the number minimum of variables to create class 
maxSize = foundMaxSize(len(simplifiedValues),len(usefullIndexs))
print(maxSize)
#Get only the group where interesting pattern can be found and replace the class type by the pattern
Groups = descendingProcess(simplifiedValues,possibleValue,classValues,maxSize)
valueDictFormated=reBuildValue(np.arange(len(values[0])),usefullIndexs,Groups)

# create a dict python of the pattern found, then a json
Result={}
j=0 #count for the name of the group
for i in valueDictFormated:
    Result[str("group" + str(j))]=reBuildDict(i,structure)
    j = j+1
j=0
json_Result=json.dumps(Result)
print(json_Result)

# save result
with open('failed_queries_grouping.json' ,'w') as outfile:
    outfile.write(json.dumps(Result,indent=4))

