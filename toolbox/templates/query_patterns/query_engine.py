import sys
import os
import json
import requests
import time

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))

sys.path.insert(1, f'{parent_folder}/toolbox/source')
import func

start = time.time()

validated_queries={}
failed_queries={}
unknown_queries={}

result={}

########################### GLOBAL PARAMETERS ##########################

# global parameters
server = "http://review_master.opensee.ninja:8080"
user = "admin"
password = "Welcome?"
# parameters
module = "testing_Arrays"
pivot = "testing_Arrays_trade_FactTable"
date='2021-05-07'
# versioning
branch = "official"

########################### LOGIN TO SERVER ##########################
start = time.time()
# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server,user,password))

# store token
token = result['login'][f'200 POST {server}/login']['response']['token']
headers = {'Authorization': token}
headers['Content-Type']='json'

########################### CALLING REQUEST API ##########################
print('performing queries through CKH...')

with open(f'{script_folder}/source/output/all_queries.json', "r") as f:
    queries = json.load(f)

number_of_queries=len(queries.keys())

url=f"{server}/module/{module}/query"

query_number=0
for query in queries:
  query_number=query_number+1

  queries[query]['query_body']['request_api_body']['pivot']=pivot
  
  # to be deleted later
  # queries[query]['query_body']['request_api_body']['where']['@and'].append({"Date": {"values": [date]}})
  # queries[query]['query_body']['request_api_body']['branch']=branch
  # queries[query]['query_body']['request_api_body']['versioning']=[{'commit':'9ab2957d6b8df545285cdfc3522d367e210d149a53ba0c4213e541f02434a35a','asOf':date}]

  queries[query]['query_response']['request_api_response']={}
  data=queries[query]['query_body']['request_api_body']

  resp=requests.request(method='post',url=url,json=data, headers=headers)

  # response
  try:
    response=resp.json()
  except:
    response=resp.text
  
  ### format response
  if resp.status_code==200:
    if isinstance(response,str):

      # add comma after each json string and convert them to list
      cleaned_str = response.replace("\n", ",\n").split(",\n")

      # remove empty items from the list
      response = [s for s in cleaned_str if s]

      # convert to json object
      response=[json.loads(element) for element in response]

      # convert to list for single record response
    else:
      response=[response]

  else:
    response=[response]

  queries[query]['query_response']['request_api_response']=response

  ########### PATTERN VALIDATOR ###########
  queries[query]['pattern_validated']={}
  trigger=True

  if len(queries[query]['query_response']['python_engine_response'])==len(queries[query]['query_response']['request_api_response']):
    if queries[query]['query_response']['python_engine_response']:
      for record in queries[query]['query_response']['python_engine_response']:
        if record in queries[query]['query_response']['request_api_response']:
          pass
        else:
          trigger=False
          break
      queries[query]['pattern_validated']=trigger
    else:
      queries[query]['pattern_validated']="unknown"
  else:
    queries[query]['pattern_validated']=False

  if queries[query]['pattern_validated'] is True:
    validated_queries[query]=queries[query]

  if queries[query]['pattern_validated'] is False:
    failed_queries[query]=queries[query]

  if queries[query]['pattern_validated']=="unknown":
    unknown_queries[query]=queries[query]
  
  if query_number % 10==0 or query_number==number_of_queries:
    print(f'{query_number}/{number_of_queries}')
  
# print results
######################## PRINT RESULTS ########################
with open(f'{script_folder}/source/output/all_queries.json', "w") as outfile:
  outfile.write(json.dumps(queries,sort_keys=False,indent=4))

with open(f'{script_folder}/source/output/validated_queries.json', "w") as outfile:
  outfile.write(json.dumps(validated_queries,sort_keys=False,indent=4))

number_of_failed_queries=len(failed_queries.keys())
with open(f'{script_folder}/source/output/failed_queries.json', "w") as outfile:
  outfile.write(json.dumps(failed_queries,sort_keys=False,indent=4))

number_of_unknown_queries=len(unknown_queries.keys())
with open(f'{script_folder}/source/output/unknown_queries.json', "w") as outfile:
  outfile.write(json.dumps(unknown_queries,sort_keys=False,indent=4))

print(f"{number_of_queries} CKH queries performed through Scala query engine in {time.time()-start:.2f} sec")
print(f"{number_of_failed_queries} failed queries and {number_of_unknown_queries} unknown queries")
