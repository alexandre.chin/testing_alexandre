import sys
import os
import json
import pandas as pd
import numpy as np
from ast import literal_eval
import itertools
import time
import copy

start = time.time()
print(f"Performing queries through Python query engine...")

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))

############# READ FILES ############

dataset_folder=f'{script_folder}/source/dataset'

### Datamodel
with open(f'{dataset_folder}/datamodel.json', "r") as f:
    datamodel_raw = json.load(f)

### UnictyKeys
unicitykeys=datamodel_raw["UnicityKeys"]

### FactTable

### define converter for parsing arrays from csv
converters={}
arraycolumns=[]
for list_of_elements in datamodel_raw['ArrayDimensions']:
    for element in list_of_elements:
        if element not in arraycolumns:
            arraycolumns.append(element)

for list_of_elements in datamodel_raw['ZipArrayFacts']:
    for element in list_of_elements:
        if element not in arraycolumns:
            arraycolumns.append(element)

for element in datamodel_raw['DimArrayFacts']:
    for element_key in element.keys():
        if element_key not in arraycolumns:
            arraycolumns.append(element_key)
        for element_value in element[element_key]:    
            if element_value not in arraycolumns:
                arraycolumns.append(element_value)

for element in arraycolumns:
    converters[element]=literal_eval

for element in datamodel_raw['ScalarFacts']:
    converters[element]=literal_eval

df_facttable_raw=pd.read_csv(f'{dataset_folder}/trade_FactTable.csv',converters=converters)

def replace_None_by_NaN_bis(d):
    if type (d)==list:
        M= [np.NaN if elem == None else elem for elem in d]
        return(M)
    elif type(d)!=list :
        if d is None :
            return(np.NaN)
        else :
            return(d)
for i in df_facttable_raw.columns:
    df_facttable_raw[i]= df_facttable_raw[i].apply(replace_None_by_NaN_bis)

### Dictionary
df_dictionary_raw=pd.read_csv(f'{dataset_folder}/csa_Dictionary.csv')

### ZipTable
df_ziptable_raw=pd.read_csv(f'{dataset_folder}/riskfactors_ZipTable.csv',converters={"riskfactorsLabels_": literal_eval})

### Joinkeys
dictionary_joinkeys=datamodel_raw["DictionaryJoinKeys"]["Dictionary"]
ziptable_joinkeys=datamodel_raw["ZipTableJoinKeys"]["ZipTable"]

# Group types
groupstypes=["DimensionsGroups","DecorationsGroups","ArrayDimensionsElementsGroups","ZipLabelsElementsGroups"]

### Queries
with open(f'{script_folder}/source/output/all_queries.json', "r") as f:
    queries = json.load(f)

number_of_queries=len(queries.keys())

query_number=0

for query in queries:
    query_number=query_number+1

    if query=="by_ziplabels_arrayfacts_2":
        print("xxx")
    df_facttable=df_facttable_raw.copy(deep=True)
    df_dictionary=df_dictionary_raw.copy(deep=True)
    df_ziptable=df_ziptable_raw.copy(deep=True)
    datamodel=copy.deepcopy(datamodel_raw)

    queries[query]['query_response']={}
    queries[query]['query_response']['python_engine_response']={}
    ########################################### PYTHON QUERY PARSER ######################################
    python_query_body_properties={'Metrics':{},'By':{},'Where':{}}

    ### Metrics ###
    # Facts
    python_query_body_properties["Metrics"]['ScalarFacts']=[]
    for element in queries[query]['query_body']['python_query_body']['Metrics']:
        if element in datamodel['ScalarFacts']:
            python_query_body_properties["Metrics"]['ScalarFacts'].append(element)

    # ZipArrayFacts
    python_query_body_properties["Metrics"]['ZipArrayFacts']=[]
    for element in queries[query]['query_body']['python_query_body']['Metrics']:
        if element in datamodel['ZipArrayFacts'][0]:
            python_query_body_properties["Metrics"]['ZipArrayFacts'].append(element)

    # DimArrayFacts
    python_query_body_properties["Metrics"]['DimArrayFacts']=[]
    for element in queries[query]['query_body']['python_query_body']['Metrics']:
        if element in set().union(*(d.keys() for d in datamodel['DimArrayFacts'])):
            python_query_body_properties["Metrics"]['DimArrayFacts'].append(element)

    ### Where and By ###
    # Non array type
    scalars_non_fact=['Dimensions','Decorations']
    for scalar_non_fact in scalars_non_fact:
        for clause in ['Where','By']:
            python_query_body_properties[clause][scalar_non_fact]=[]
            for element in queries[query]['query_body']['python_query_body'][clause]:
                if element in datamodel[scalar_non_fact]:
                    python_query_body_properties[clause][scalar_non_fact].append(element)

    # ArrayX type
    ArraysX=['ArrayDimensions','ArrayDimensionsElements','ZipLabels']
    for ArrayX in ArraysX:
        for clause in ['Where','By']:
            python_query_body_properties[clause][ArrayX]=[]
            for element in queries[query]['query_body']['python_query_body'][clause]:
                if element in list(itertools.chain(*datamodel[ArrayX])):
                    python_query_body_properties[clause][ArrayX].append(element)

    # ArrayDimensions linked to DimArrayFacts
    for clause in ['Where','By']:
        python_query_body_properties[clause]['ArrayDimensionsLinkedToArrayFacts']=[]
        for element in queries[query]['query_body']['python_query_body'][clause]:
            if element in list(itertools.chain(*[a_dict.values() for a_dict in datamodel['DimArrayFacts']]))[0]:
                python_query_body_properties[clause]['ArrayDimensionsLinkedToArrayFacts'].append(element)

    # Groups
    for clause in ['Where','By']:
        python_query_body_properties[clause]['Groups']={}
        for grouptype in groupstypes:
            python_query_body_properties[clause]['Groups'][grouptype]=[]
            for element in queries[query]['query_body']['python_query_body'][clause]:
                if element in datamodel[grouptype].keys():
                    python_query_body_properties[clause]['Groups'][grouptype].append(element)

    ########################################### PYTHON QUERY CONTOLLER ######################################

    # Error if Arrayfacts in metrics and both ziplabels and arraydimensions linked to arrayfacts in by clause
    errormessage="Querying ZipArrayFacts and DimArrayFacts is only supported when having no labels in the BY term"

    if python_query_body_properties["Metrics"]['ZipArrayFacts'] and python_query_body_properties["Metrics"]['DimArrayFacts'] and (python_query_body_properties["By"]['ArrayDimensionsLinkedToArrayFacts'] or python_query_body_properties["By"]['ZipLabels']):
        print(f'Unsupported query: {errormessage}')

    ########################################### PYTHON QUERY ENGINE ##########################################

    df_result=df_facttable.copy(deep=True)

    # duplicate ArrayDimensions columns type to generate ArrayDimensionsElements
    df_result[list(itertools.chain(*datamodel['ArrayDimensionsElements']))]=df_result[list(itertools.chain(*datamodel['ArrayDimensions']))]

    ###### Step 0: Join with dictionary ######

    # Parse query parameters
    decorations=[]

    for element in queries[query]['query_body']['python_query_body']['By']:
        if element in datamodel["DecorationsGroups"]:
            element=list(datamodel["DecorationsGroups"][element].keys())[0]
        if element in datamodel["Decorations"]:
            decorations.append(element)

    for element in queries[query]['query_body']['python_query_body']['Where'].keys():
        if element in datamodel["DecorationsGroups"]:
            element=list(datamodel["DecorationsGroups"][element].keys())[0]
        if element in datamodel["Decorations"]:
            decorations.append(element)

    decorations=list(set(decorations))

    if decorations:
        df_result=df_result.merge(df_dictionary, on=dictionary_joinkeys, how='left')

    ###### Step 1: Remove unrequired columns ######
    required_columns=[]
    required_columns=required_columns+unicitykeys
    required_columns=required_columns+dictionary_joinkeys
    required_columns=required_columns+ziptable_joinkeys
    required_columns=required_columns+queries[query]['query_body']['python_query_body']['Metrics']
    required_columns=required_columns+queries[query]['query_body']['python_query_body']['By']

    # Where terms
    for element in queries[query]['query_body']['python_query_body']['Where'].keys():
        required_columns.append(element)

    for clause in ['Where','By']:
        for col in queries[query]['query_body']['python_query_body'][clause]:
            # Columns required for DimensionGroups
            if col in datamodel["DimensionsGroups"]:
                for dimgroup in list(set(datamodel["DimensionsGroups"].keys())):
                    if col==dimgroup:
                        required_columns.append(list(datamodel["DimensionsGroups"][col].keys())[0])

            # Columns required for DecorationsGroups
            if col in datamodel["DecorationsGroups"]:
                for dimgroup in list(set(datamodel["DecorationsGroups"].keys())):
                    if col==dimgroup:
                        required_columns.append(list(datamodel["DecorationsGroups"][col].keys())[0])

            # Columns required for ArrayDimensionsElementsGroups
            if col in datamodel["ArrayDimensionsElementsGroups"]:
                for arraydimgroup in list(set(datamodel["ArrayDimensionsElementsGroups"].keys())):
                    if col==arraydimgroup:
                        required_columns.append(list(datamodel["ArrayDimensionsElementsGroups"][col].keys())[0])

            # Columns required for ZipLabelsElementsGroups
            if col in datamodel["ZipLabelsElementsGroups"]:
                for ziplabelgroup in list(set(datamodel["ZipLabelsElementsGroups"].keys())):
                    if col==ziplabelgroup:
                        required_columns.append(list(datamodel["ZipLabelsElementsGroups"][col].keys())[0])


    required_columns=list(set(required_columns))
    df_result=df_result[list(list(set(required_columns) & (set(df_result.columns.to_list()))))]

    #Create Groups columns
    for col in set(queries[query]['query_body']['python_query_body']['By'] + list(queries[query]['query_body']['python_query_body']['Where'].keys())):
        for grouptype in ["DimensionsGroups","DecorationsGroups"]:
            if col in datamodel[grouptype]:
                for group in list(datamodel[grouptype][col].keys()):
                    #reverse datamodel groups dictionary
                    mapping={}
                    for key, lst in datamodel[grouptype][col][group].items():
                        for val in lst:
                            mapping[val]=key
                    
                    df_result[col] = df_result[group].map(mapping)

                    # Replace NaN by 'others'
                    df_result[col]=df_result[col].fillna("others")

    ###### Step 2: Filter on dimensions ######

    # Parse query parameters
    dimensions_in_where_clause=[]
    for element in queries[query]['query_body']['python_query_body']['Where']:
        if element in datamodel["Dimensions"]:
            dimensions_in_where_clause.append(element)

    # Apply where clause (supports AND only)
    for element in dimensions_in_where_clause:
        df_result=df_result[df_result[element].isin(queries[query]['query_body']['python_query_body']['Where'][element])]

    ###### Step 3: Explode ArrayDimensions ######

    # Parse query parameters
    arraydimensionselements=[]
    required_arraydimensionselements=[]

    for element in queries[query]['query_body']['python_query_body']['By']:
        # Used for generating ArrayDimensionsElementsGroups
        if element in datamodel["ArrayDimensionsElementsGroups"]:
            element=list(datamodel["ArrayDimensionsElementsGroups"][element].keys())[0]

        for items in datamodel["ArrayDimensionsElements"]:
            if element in items and items not in arraydimensionselements:
                arraydimensionselements.append(items)
            if element in items and element not in required_arraydimensionselements:
                required_arraydimensionselements.append(element)

    for element in queries[query]['query_body']['python_query_body']['Where'].keys():
        # Used for generating ArrayDimensionsElementsGroups
        if element in datamodel["ArrayDimensionsElementsGroups"]:
            element=list(datamodel["ArrayDimensionsElementsGroups"][element].keys())[0]

        for items in datamodel["ArrayDimensionsElements"]:
            if element in items and items not in arraydimensionselements:
                arraydimensionselements.append(items)
            if element in items and element not in required_arraydimensionselements:
                required_arraydimensionselements.append(element)

    
    if arraydimensionselements:
        # Remove unqueried arraydimelements in linkedarraydimensionselements
        linkedarraydimensionselements=list(arraydimensionselements)
        for i in range(0,len(linkedarraydimensionselements)):
            linkedarraydimensionselements[i]=[j for j in linkedarraydimensionselements[i] if j in required_arraydimensionselements]
        # Explode arrays
        for linkedarraydimensionselements in linkedarraydimensionselements:
            df_result=df_result.explode(linkedarraydimensionselements)

    #Create Groups columns
    queried_ArrayDimensionsElementsGroups=[]
    for col in set(queries[query]['query_body']['python_query_body']['By'] + list(queries[query]['query_body']['python_query_body']['Where'].keys())):
        for grouptype in ["ArrayDimensionsElementsGroups"]:
            if col in datamodel[grouptype]:
                queried_ArrayDimensionsElementsGroups.append(col)
                for group in list(datamodel[grouptype][col].keys()):
                    #reverse datamodel groups dictionary
                    mapping={}
                    for key, lst in datamodel[grouptype][col][group].items():
                        for val in lst:
                            mapping[val]=key
                    
                    df_result[col] = df_result[group].map(mapping)

                    # Replace NaN by 'others'
                    df_result[col]=df_result[col].fillna("others")

    queried_ArrayDimensionsElementsGroups=list(set(queried_ArrayDimensionsElementsGroups))

    # Get rid off unqueried ArrayDimensionsElements columns
    cols=df_result.columns.to_list()

    for element in cols:
        if element not in (queries[query]['query_body']['python_query_body']['By']+list(set(queries[query]['query_body']['python_query_body']['Where'].keys()))) and element in datamodel["ArrayDimensionsElementsGroups"]:
            cols.remove(element)
    
    # Remove duplicates
    queried_elements=list(set(list(queries[query]['query_body']['python_query_body']['Where'].keys())+queries[query]['query_body']['python_query_body']['By']))
    
    queried_arraydimensionselements=[]
    for element in required_arraydimensionselements:
        if element in queried_elements and element not in queried_arraydimensionselements:
            queried_arraydimensionselements.append(element)

    subset=unicitykeys+queried_arraydimensionselements+queried_ArrayDimensionsElementsGroups
    df_result=df_result.drop_duplicates(subset=subset)

    ###### Step 3bis: Explode DimArrayFacts ######
    # Parse query parameters
    queried_dimarrays_linked_to_dimarrayfacts=[]
    queried_dimarrayfacts=[]

    for element in queries[query]['query_body']['python_query_body']['By']:
        for items in datamodel["DimArrayFacts"]:
            if element in list(itertools.chain(*items.values())) and items not in queried_dimarrays_linked_to_dimarrayfacts:
                queried_dimarrays_linked_to_dimarrayfacts.append(element)

    for element in queries[query]['query_body']['python_query_body']['Metrics']:
        for items in datamodel["DimArrayFacts"]:
            if element in items.keys() and element not in queried_dimarrayfacts:
                queried_dimarrayfacts.append(element)

    if queried_dimarrayfacts+queried_dimarrays_linked_to_dimarrayfacts:
        df_result=df_result.explode(queried_dimarrayfacts+queried_dimarrays_linked_to_dimarrayfacts)

    if queried_dimarrayfacts:
    # Convert dimarrayfacts to float
        df_result[queried_dimarrayfacts] = df_result[queried_dimarrayfacts].astype(float)

    ###### Step 4: Unfold Ziplabels ######

    # Parse query parameters
    ziplabelselements=[]
    required_ziplabelselements=[]

    required_ziptablecolumns=list(set(required_columns) & set(df_ziptable.columns.to_list()))
    for element in ziptable_joinkeys:
        if element in required_ziptablecolumns:
            required_ziptablecolumns.remove(element)

    for element in list(set(queries[query]['query_body']['python_query_body']['By'])):
        # Used for generating ZipLabelsElementsGroups
        if element in datamodel["ZipLabelsElementsGroups"]:
            element=list(datamodel["ZipLabelsElementsGroups"][element].keys())[0]
        for items in datamodel["ZipLabelsElements"]:
            if element in items and items not in ziplabelselements:
                ziplabelselements.append(items)
            if element in items and element not in required_ziplabelselements:
                required_ziplabelselements=required_ziplabelselements+items

    for element in list(set(queries[query]['query_body']['python_query_body']['Where'].keys())):
        # Used for generating ZipLabelsElementsGroups
        if element in datamodel["ZipLabelsElementsGroups"]:
            element=list(datamodel["ZipLabelsElementsGroups"][element].keys())[0]
        for items in datamodel["ZipLabelsElements"]:
            if element in items and items not in ziplabelselements:
                ziplabelselements.append(items)
            if element in items and element not in required_ziplabelselements:
                required_ziplabelselements=required_ziplabelselements+items

    if ziplabelselements:
        # duplicate Ziplabel columns type to generate Ziplabelelements
        df_ziptable[list(itertools.chain(*datamodel['ZipLabelsElements']))]=df_ziptable[list(itertools.chain(*datamodel['ZipLabels']))]

        # Remove unqueried ziplabels in linkedziplabels
        for linkedziplabelselements in ziplabelselements:
            for element in linkedziplabelselements:
                if element not in required_ziplabelselements+required_ziptablecolumns:
                    linkedziplabelselements.remove(element)
    
        # Store index position of ziplabel elements
        if ziplabelselements[0]:
            df_ziptable['ZipTableIndex']=df_ziptable[ziplabelselements[0][0]].apply(lambda x:[i for i in range(0, len(x))])
            ziplabelselements[0].append('ZipTableIndex')

        # Explode ziplabel
        for linkedziplabelselements in ziplabelselements:
            df_ziptable=df_ziptable.explode(linkedziplabelselements)

    ###### Step 5: Explode ziparrayfacts ######

    # Parse query parameters
    ziparrayfacts=[]
    queried_ziparrayfacts=[]

    for element in queries[query]['query_body']['python_query_body']['Metrics']:
        for items in datamodel["ZipArrayFacts"]:
            if element in items and items not in ziparrayfacts:
                ziparrayfacts.append(items)
            if element in items and element not in queried_ziparrayfacts:
                queried_ziparrayfacts.append(element)

    # Remove unqueried ziparrayfacts in linkedziparrayfacts
    for linkedziparrayfacts in ziparrayfacts:
        for element in linkedziparrayfacts:
            if element not in queried_ziparrayfacts:
                linkedziparrayfacts.remove(element)

    # Store index position of ziparrayfacts elements
    if ziparrayfacts:
        if ziparrayfacts[0]:
            df_result['ZipTableIndex']=df_result[ziparrayfacts[0][0]].apply(lambda x:[i for i in range(0, len(x))])

        # # Explode ziparrayfacts and Index
        for linkedziparrayfacts in ziparrayfacts:
            df_result=df_result.explode(linkedziparrayfacts+['ZipTableIndex'])

        # Convert ziparrayfacts to float
        df_result[ziparrayfacts[0]] = df_result[ziparrayfacts[0]].astype(float)

    ###### Step 7: Join with ziptable ######
    if ziplabelselements:
        if ziparrayfacts:
            df_result=df_result.merge(df_ziptable, on=ziptable_joinkeys+['ZipTableIndex'], how='left')
        else:
            df_result=df_result.merge(df_ziptable, on=ziptable_joinkeys, how='left')

    #Create Groups columns
    queried_ZipLabelsElementsGroups=[]
    for col in set(queries[query]['query_body']['python_query_body']['By'] + list(queries[query]['query_body']['python_query_body']['Where'].keys())):
        for grouptype in ["ZipLabelsElementsGroups"]:
            if col in datamodel[grouptype]:
                queried_ZipLabelsElementsGroups.append(col)
                for group in list(datamodel[grouptype][col].keys()):
                    #reverse datamodel groups dictionary
                    mapping={}
                    for key, lst in datamodel[grouptype][col][group].items():
                        for val in lst:
                            mapping[val]=key
                    
                    df_result[col] = df_result[group].map(mapping)
    
    queried_ZipLabelsElementsGroups=list(set(queried_ZipLabelsElementsGroups))

    # Remove duplicates
    queried_elements=list(set(list(queries[query]['query_body']['python_query_body']['Where'].keys())+queries[query]['query_body']['python_query_body']['By']))
    
    queried_ziplabelselements=[]
    for element in required_ziplabelselements:
        if element in queried_elements and element not in queried_ziplabelselements:
            queried_ziplabelselements.append(element)

    subset=unicitykeys+queried_arraydimensionselements+queried_ArrayDimensionsElementsGroups+queried_arraydimensionselements+queried_ziplabelselements+queried_ZipLabelsElementsGroups
    df_result=df_result.drop_duplicates(subset=subset)

    ###### Step 9: filter on scalar elements (decorations, ArrayDim elements, ZipLabel elements, groups etc.) ######

    # Parse query parameters
    elements_in_where_clause=[]
    elements_in_by_clause=[]

    for element in queries[query]['query_body']['python_query_body']['Where']:
        elements_in_where_clause.append(element)

    for element in queries[query]['query_body']['python_query_body']['By']:
        elements_in_by_clause.append(element)

    # Apply where clause (supports AND only)
    for element in elements_in_where_clause:
        values=queries[query]['query_body']['python_query_body']['Where'][element]
        df_result=df_result[df_result[element].isin(values)]

    ###### Step 10: Convert list-type columns to string (required for grouping) ######
    arrays_cols=[]
    for clause in python_query_body_properties:
        for coltype in python_query_body_properties[clause]:
            if coltype in ["ArrayDimensions","ZipLabels"]:
               arrays_cols=list(set(arrays_cols + python_query_body_properties[clause][coltype]))

    df_result[arrays_cols]=df_result[arrays_cols].astype(str)

    ###### Keep Unicity keys, By terms and Metrics ######
    metrics=queries[query]['query_body']['python_query_body']['Metrics']
    cols_to_keep=unicitykeys+queries[query]['query_body']['python_query_body']['By']+metrics
    cols_to_keep=list(set(cols_to_keep))
    df_result=df_result[cols_to_keep]

    ###### Step 11: Aggregate Facts and ziparrayfacts ######

    # Parse query parameters
    scalarfacts=[]
    for element in queries[query]['query_body']['python_query_body']['Metrics']:
        if element in datamodel['ScalarFacts']:
            scalarfacts.append(element)

    # Drop unrequired columns
    required_columns=unicitykeys+queries[query]['query_body']['python_query_body']['By']+queries[query]['query_body']['python_query_body']['Metrics']
    required_columns=list(set(required_columns))
    df_result=df_result[required_columns]

    # Aggregate ArrayFacts by (unicitykeys+by terms+scalar facts)
    aggregation_columns=list(set(unicitykeys+queries[query]['query_body']['python_query_body']['By']+scalarfacts))
    df_result=df_result.groupby(aggregation_columns,as_index=False,dropna=False).sum()

    # Aggregate Facts by (By terms)
    if queries[query]['query_body']['python_query_body']['By']:
        df_result=df_result.groupby(queries[query]['query_body']['python_query_body']['By'],as_index=False).sum()
    else:
        df_result['dummy_col']="agg"
        df_result=df_result.groupby(['dummy_col'],as_index=False).sum()
        df_result=df_result.drop(['dummy_col'], axis=1)
    
    if query_number % 100==0 or query_number==number_of_queries:
        print(f'{query_number}/{number_of_queries}')


    ######################## Store result ########################
    # Reorder columns
    sorted_columns=queries[query]['query_body']['python_query_body']["By"]+queries[query]['query_body']['python_query_body']["Metrics"]

    df_result=df_result[sorted_columns]
    dict_result=df_result.to_dict('records')

    # Store result
    queries[query]['query_response']['python_engine_response']=dict_result

######################## PRINT RESULTS ########################
with open(f'{script_folder}/source/output/all_queries.json', "w") as outfile:
    outfile.write(json.dumps(queries,sort_keys=False,indent=4))

print(f"{number_of_queries} queries performed through Python query engine in {time.time()-start:.2f} sec")