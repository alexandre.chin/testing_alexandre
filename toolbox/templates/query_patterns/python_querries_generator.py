import sys
import os
import json
import itertools
import copy
import random


print(f'Generating Python querries bodies based on provided patterns...')
##################### PARAMETERS #####################
randomize_by_clauses=False
match_where_and_by_clauses=True
seed=12

by_elements={
            "DimensionsGroups":["counterparty_Group"],
            "Dimensions":["mtfScenario","counterparty"],
            "DecorationsGroups":["collateral_Group"],
            "Decorations":["elligibleCollateral","frequency"],
            "ArrayDimensions":["legIds","legCurrencies"],
            "ArrayDimensionsElementsGroups":["legalEntity_Group"],
            "ArrayDimensionsElements":["legId","legCurrency"],
            "ZipLabels":["riskfactorsLabel"],
            "ZipLabelsElementsGroups":["riskfactor_Group"],
            "ZipLabelsElements":["riskfactorsLabels"]      
            }

##################### LOAD DATA #####################
# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))

# load datamodel
with open(f'{script_folder}/source/dataset/datamodel.json', "r") as f:
    datamodel = json.load(f)

# load query patterns
with open(f'{script_folder}/source/query_patterns/query_patterns.json', "r") as f:
    query_patterns = json.load(f)

# load where terms
with open(f'{script_folder}/source/dataset/datamodel.json', "r") as f:
    datamodel = json.load(f)

# query_patterns={"test":{"Dimensions":{"Where":[1,2,3],"By":[3,4]},"ZipLabels":{"Where":[5,6],"By":[3,4]}}}
result={}

##################### EXPLODE QUERRY PATTERN #####################
# Creates blank queries
for lvl1 in query_patterns.keys():
    i=1
    combinations=[]
    labels=[]
    lvl2_labels=query_patterns[lvl1].keys()
    
    for lvl2 in query_patterns[lvl1].keys():
        lvl3_labels=query_patterns[lvl1][lvl2].keys()
        for lvl3 in query_patterns[lvl1][lvl2].keys():
            labels.append(lvl3)
            combinations.append(query_patterns[lvl1][lvl2][lvl3])

    combinations=list(itertools.product(*combinations))

    for i in range(len(combinations)):
        j=0
        result[f"{lvl1}_{i}"]={}
        result[f"{lvl1}_{i}"]['query_body']={"query_structure":copy.deepcopy(query_patterns[lvl1])}

        for lvl2_label in lvl2_labels:
            lvl3_labels=result[f"{lvl1}_{i}"]['query_body']["query_structure"][lvl2_label].keys()
            for lvl3_label in lvl3_labels:
                result[f"{lvl1}_{i}"]['query_body']["query_structure"][lvl2_label][lvl3_label]=combinations[i][j]
                j=j+1

##################### GENERATE CLAUSES #####################

# By
if randomize_by_clauses is False:
    pass
else:
    by_elements={
                "DimensionsGroups":[],
                "Dimensions":[],
                
                "DecorationsGroups":[],
                "Decorations":[],
                
                "ArrayDimensions":[],
                "ArrayDimensionsElementsGroups":[],
                "ArrayDimensionsElements":[],

                "ZipLabels":[],
                "ZipLabelsElementsGroups":[],
                "ZipLabelsElements":[] 
                }
    
    random.seed(seed)
    for element in by_elements:
        if datamodel[element]:
            if isinstance(datamodel[element],dict):
                 by_elements[element]=list(set(random.sample(list(datamodel[element].keys()), k=min(len(list(datamodel[element].keys())),2))))
            else:
                if isinstance(datamodel[element][0],list):
                    flatten_list=list(itertools.chain(*datamodel[element]))
                    by_elements[element]=list(set(random.sample(flatten_list, k=min(len(flatten_list),2))))
                else:
                    by_elements[element]=list(set(random.sample(datamodel[element], k=min(len(datamodel[element]),2))))

# Where
where_elements={"Dimensions":{},
"Decorations":{},
"ArrayDimensions":{},
"ArrayDimensionsElements":{},
# "ZipLabels":{},
"ZipLabelsElements":{},

"DimensionsGroups":{},
"DecorationsGroups":{},
"ArrayDimensionsElementsGroups":{},
"ZipLabelsElementsGroups":{}
}

if match_where_and_by_clauses:
    for col_type in by_elements:
        for col_name in by_elements[col_type]:
            try:
                where_elements[col_type][col_name]=datamodel['Where'][col_name]
            except:
                pass
else:
    for element in where_elements:
        if datamodel[element]:
            if isinstance(datamodel[element][0],list):
                flatten_list=list(itertools.chain(*datamodel[element]))
                temp_elements=list(set(random.sample(flatten_list, k=min(len(flatten_list),2))))
            else:
                temp_elements=list(set(random.sample(datamodel[element], k=min(len(datamodel[element]),2))))
        if isinstance(temp_elements[0],str):
            pass
        else:
            temp_elements=list(set(itertools.chain(*temp_elements)))

        for temp_element in temp_elements:
            where_elements[element][temp_element]=datamodel['Where'][temp_element]


# Metrics
metrics_elements={
"ScalarFacts":[],
"ZipArrayFacts":[],
"DimArrayFacts":[]
}

random.seed(seed)
for element in metrics_elements:
    if isinstance(datamodel[element][0],str):
        metrics_elements[element]=list(set(random.sample(datamodel[element], k=min(len(datamodel[element]),2))))     
    else:
        flatten_list=list(itertools.chain(*datamodel[element]))
        metrics_elements[element]=list(set(random.sample(flatten_list, k=min(len(flatten_list),2))))


##################### GENERATE QUERIES #####################

python_query_template={
    "Pivot":"",
    "AsOf":"",
    "Branch":"",

    "Metrics":[],
    "By": [],
    "Where": {}
}

for query in result:
    result[query]['query_body']["python_query_body"]=copy.deepcopy(python_query_template) 
    for element in result[query]['query_body']["query_structure"]:
        for clause in result[query]['query_body']["query_structure"][element]:
            number_of_elements=result[query]['query_body']["query_structure"][element][clause]

            # Where clause
            if clause=="Where":
                temp_list=list(where_elements[element].keys())[0:number_of_elements]
                for temp in temp_list:
                    result[query]['query_body']["python_query_body"][clause].update({temp:where_elements[element][temp]})
            
            # By clause
            if clause=="By":
                result[query]['query_body']["python_query_body"][clause]=result[query]['query_body']["python_query_body"][clause]+by_elements[element][:number_of_elements]

            # Metrics
            if clause=="Metrics":
                result[query]['query_body']["python_query_body"][clause]=result[query]['query_body']["python_query_body"][clause]+metrics_elements[element][:number_of_elements]

number_of_query_patterns=len(result.keys())

with open(f'{script_folder}/source/output/all_queries.json', "w") as outfile:
    outfile.write(json.dumps(result,sort_keys=False,indent=4))

print(f'{number_of_query_patterns} Python querries bodies generated based on provided patterns')