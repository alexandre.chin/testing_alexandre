import sys
import os
import json
import pandas as pd
import numpy as np
import requests
import time

number_of_records=100000

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))

df = pd.DataFrame({
    "asOf":"2020-06-01",
    "cpty":['cpty_'+str(i) for i in range(0,number_of_records)],
    "portfolio":['portfolio_'+str(i) for i in range(0,number_of_records)],
    "trade":['trade_'+str(i) for i in range(0,number_of_records)],
    "fact"  : np.random.normal(1000.0, 1000.0, size=number_of_records)
                     })

df.to_csv(f"{script_folder}/data.csv",index=False,header=False)