import os
import time
import func
import pandas as pd
import json

########################### GLOBAL PARAMETERS ##########################

server = "http://capgemini.opensee.ninja:8080"
# server = "http://devtest-rhel.opensee.ninja:8080"
# server = "http://devtest.opensee.ninja:8080"

user = "admin"
password = "Welcome?"

module="testing_Arrays"
table="testing_Arrays_trade_FactTable"
dict="testing_Arrays_csa_Dictionary"
ziptable="testing_Arrays_riskfactors_ZipTable"

script_folder=os.path.dirname(os.path.realpath(__file__))

result={}
token=None

steps={
############# PING AND LOGIN #############
#ping
'ping':{'type':'ping','method':'get','route_parameters':{'server':server},'body':{}},
#login
'login_admin':{'type':'login','method':'post','route_parameters':{'server':server},'body':{"identifier": user,"password": password}},

############# DM #############
"delete_dm":{'type':'deleteDM','method':'delete','route_parameters':{'server':server,'module':module},'body':{"hardDelete": True}},
"create_dm":{'type':'createDM','method':'post','route_parameters':{'server':server},'body':f'{script_folder}/source/dm/rdm_testing_arrays.json'},
"get_dm":{'type':'getDM','method':'get','route_parameters':{'server':server,'module':module},'body':None},

#ingestion settings
"get_ingestionSettings_0":{'type':'ingestionSettings','method':'get','route_parameters':{'server':server,'module':module,'table':table},'body':{}},
"post_ingestionSettings":{'type':'ingestionSettings','method':'post','route_parameters':{'server':server,'module':module,'table':table},'body':None,'overload':[{'replace_body':'get_ingestionSettings_0'},{'body_items':{'dictionaries':[dict]}}]},
"get_ingestionSettings":{'type':'ingestionSettings','method':'get','route_parameters':{'server':server,'module':module,'table':table},'body':{}},

"addPolicy":{'type':'addPolicy','method':'put','route_parameters':{'server':server,'userGroup':'capgemini'},'body':{'actions': ['editPivot','runPivot'],'resource': 'pivot:testing_Arrays*'}},
"addMandatoryFilter":{'type':'addMandatoryFilter','method':'post','route_parameters':{'server':server,'userGroup':'capgemini'},'body':{"testing_Arrays": {"@and": []}}},

"yamina.preston":{'type':'addUser','method':'put','route_parameters':{'server':server,'userGroup':'capgemini','user':"yamina.preston"},'body':None},
"nikhil.kaluskar":{'type':'addUser','method':'put','route_parameters':{'server':server,'userGroup':'capgemini','user':"nikhil.kaluskar"},'body':None},
"vitlesh.pandita":{'type':'addUser','method':'put','route_parameters':{'server':server,'userGroup':'capgemini','user':"vitlesh.pandita"},'body':None},
"raghuveeran.thangamani":{'type':'addUser','method':'put','route_parameters':{'server':server,'userGroup':'capgemini','user':"raghuveeran.thangamani"},'body':None},
"bhavyarani.mallenahalli":{'type':'addUser','method':'put','route_parameters':{'server':server,'userGroup':'capgemini','user':"bhavyarani.mallenahalli"},'body':None},
"rahul.panchal":{'type':'addUser','method':'put','route_parameters':{'server':server,'userGroup':'capgemini','user':"rahul.panchal"},'body':None},
"srividya.sethuraman":{'type':'addUser','method':'put','route_parameters':{'server':server,'userGroup':'capgemini','user':"srividya.sethuraman"},'body':None},
"paidisetti.pradeepkumar":{'type':'addUser','method':'put','route_parameters':{'server':server,'userGroup':'capgemini','user':"paidisetti.pradeepkumar"},'body':None},
"basavaprabhu":{'type':'addUser','method':'put','route_parameters':{'server':server,'userGroup':'capgemini','user':"basavaprabhu"},'body':None},
"arno.oelofse":{'type':'addUser','method':'put','route_parameters':{'server':server,'userGroup':'capgemini','user':"arno.oelofse"},'body':None},

############# INGESTION #############
# Ingest zip
"zip_ingest":{'type':'zipIngest','method':'post','route_parameters':{'server':server,'table':ziptable},'body':f'{script_folder}/source/data/riskfactors_ZipTable.csv'},

# ingest dict
"dict_ingest":{'type':'dictIngest','method':'post','route_parameters':{'server':server,'dict':dict,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/csa_Dictionary.csv'},

# ingest initial records in facttable
"open_initial_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'initial_1'},'body':None},
"ingest_initial_1":{'type':'ingest','transaction':'open_initial_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/trade_FactTable_initial_1.csv'},
"close_initial_1":{'type':'closeTransaction','method':'post','transaction':'open_initial_1','route_parameters':{'server':server,'table':table,'destinationBranch':'initial'},'body':{"closingMode": {"Merge": {"force":True}}}},

"open_initial_2":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'initial_2'},'body':None},
"ingest_initial_2":{'type':'ingest','transaction':'open_initial_2','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/trade_FactTable_initial_2.csv'},
"close_initial_2":{'type':'closeTransaction','method':'post','transaction':'open_initial_2','route_parameters':{'server':server,'table':table,'destinationBranch':'initial'},'body':{"closingMode": {"Merge": {"force":True}}}},

"open_initial_3":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'initial_3'},'body':None},
"ingest_initial_3":{'type':'ingest','transaction':'open_initial_3','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07'},'body':f'{script_folder}/source/data/trade_FactTable_initial_3.csv'},
"close_initial_3":{'type':'closeTransaction','method':'post','transaction':'open_initial_3','route_parameters':{'server':server,'table':table,'destinationBranch':'initial'},'body':{"closingMode": {"Merge": {"force":True}}}},

# getBranch
"get_branch_initial":{'type':'getBranch','method':'get','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','branch':'initial'},'body':None},

# ingest adjustments in facttable
"open_adj_1":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'adj_1'},'body':None},
"ingest_adj_1":{'type':'ingest','transaction':'open_adj_1','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07',"blocks":"book/tradeId"},'body':f'{script_folder}/source/data/trade_FactTable_adj_1.csv'},
"close_adj_1":{'type':'closeTransaction','method':'post','transaction':'open_adj_1','route_parameters':{'server':server,'table':table,'destinationBranch':'user_1_branch'},'body':{"closingMode": {"Merge": {"force":True}}}},

"get_branch_userl":{'type':'getBranch','method':'get','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','branch':'user_1_branch'},'body':None},
"query_user_1_branch":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_user_1_branch.json'},


"open_adj_2":{'type':'openTransaction','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'initial','comment':'adj_2'},'body':None},
"ingest_adj_2":{'type':'ingest','transaction':'open_adj_2','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07',"blocks":"book/tradeId"},'body':f'{script_folder}/source/data/trade_FactTable_adj_2.csv'},
"close_adj_2":{'type':'closeTransaction','method':'post','transaction':'open_adj_2','route_parameters':{'server':server,'table':table,'destinationBranch':'user_2_branch'},'body':{"closingMode": {"Merge": {"force":True}}}},

"get_branch_user2":{'type':'getBranch','method':'get','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','branch':'user_2_branch'},'body':None},
"query_user_2_branch":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_user_2_branch.json'},

# merge branches user1 with user2
"merge_user2_in_user1":{'type':'merge','method':'post','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','sourceBranch':'user_1_branch','destinationBranch':'user_2_branch'},'body':{"comment": "merging user_1_branch in user_2_branch", "conflictManagement": "KeepDestinationBranch", "concurrencyManagement":"None","moveBranch": "Destination"}},
"get_branch_user2_after_merge":{'type':'getBranch','method':'get','route_parameters':{'server':server,'table':table,'asof':'2021-05-07','branch':'user_2_branch'},'body':None},

# query branch_user2_after_merge
"query_user_2_branch_after_merge":{'type':'queryCheckpoint','method':'post','route_parameters':{'server':server,'module':module},'body':f'{script_folder}/source/checkpoints/query_user_2_branch_after_merge.json'},

}

start = time.time()
print(f"Tests on going...")

all_tests_ok=True
script_folder=os.path.dirname(os.path.realpath(__file__))

full_log_path=f"{script_folder}/full_logs.log"
error_log_path=f"{script_folder}/error_logs.log"

# clear log file
with open(full_log_path, "w") as log_file:
    pass
with open(error_log_path, "w") as log_file:
    pass

for step in steps:
    if steps[step]['type'] in ['ingest','softDelete','closeTransaction']:
        #add asof and transaction_id to route parameters
        steps[step]['route_parameters']['transaction_id']=result[steps[step]['transaction']]['response']['response']
        steps[step]['route_parameters']['asof']=result[steps[step]['transaction']]['route_parameters']['asof']
        time.sleep(1)

    # overload
    result=func.overload(steps[step],result)

    # api call
    temp=func.api_call(token,**steps[step])

    result[step]=func.store_api_call(step,temp)

    if steps[step]['type']=='login':
        token=result[step]['response']['response']['token']

###### Print logs #######
func.print_logs(result,full_log_path)

result_errors={}

for step in result:
    if result[step]['response']['status_code'] in [200,201,203,204]:
        if result[step]['type'] in ['ping','login']:
            result_errors[step]=result[step]
        elif 'Checkpoint' in result[step]['type']:
            if result[step]['checkpoint_status']=='KO':
                print(step)
                print("current response", result[step]['response']['response'])
                print("expected response", result[step]['expected_response'])
                result_errors[step]=result[step]
                all_tests_ok=False
    else:
        result_errors[step]=result[step]
        all_tests_ok=False

print(f"Tests completed in {time.time()-start:.2f} sec. /n")

if all_tests_ok:
    print(f'all tests succeeded. full logs file available here: {full_log_path}')
else:
    func.print_logs(result_errors,error_log_path)
    print(f'Some tests failed. full logs and error logs file available here:{script_folder}')
