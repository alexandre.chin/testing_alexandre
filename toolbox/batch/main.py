import sys
import os
import json
import requests
import time
from source import func

result={}

########################### GLOBAL PARAMETERS ##########################

# path
parent_folder=os.getcwd()
script_folder=os.path.dirname(os.path.realpath(__file__))
log_full_path=f"{script_folder}/logs.log"

# global parameters
server = "http://review_766-arraydimension-error.opensee.ninja:8080"
user = "guillaume"
password = "Welcome?"

# clear log file
with open(log_full_path, "w") as log_file:
  pass

########################### LOGIN TO SERVER ##########################

# ping server
result.update(func.ping_api(server))

# login to server
print('login to server...')
result.update(func.login_api(server,user,password))

# store token
token=result['login'][f'200 POST {server}/login']['response']['token']
kwcnx = dict(headers={'Authorization': token})

########################### TESTING ##########################
print('testing batch api...')
suffix='_batch_api'

try:
  result.update(func.batch_api(server,token))
except:
  result[suffix]={'status':'KO: please debug for investigation'}

# print results
print('generating log file...')
func.print_logs(result,log_full_path)